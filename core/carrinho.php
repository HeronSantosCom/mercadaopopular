﻿<?php

Class Carrinho extends Core {

    static function Id() {
        $id = parent::Session("idCarrinho");
        if ($id) {
            return $id;
        }
        return parent::Session("idCarrinho", hash('crc32', date("r") . session_id()));
    }

    static function Itens() {
        $carrinho = parent::Session(self::Id() . "itensCarrinho");
        if ($carrinho) {
            return $carrinho;
        }
        return false;
    }

    static function Frete($peso = false, $total = false) {
        $entrega = parent::Session(self::Id() . "freteCarrinho");
        if ($entrega) {
            if ($peso and $total) {
                list($cep, $envio, $endereco, $frete_local, $frete_valor) = $entrega;
                list($frete_local, $frete_valor) = PagSeguro::Frete($cep, $envio, $peso, $total);
                return parent::Session(self::Id() . "freteCarrinho", array($cep, $envio, $endereco, $frete_local, $frete_valor));
            }
            return parent::Session(self::Id() . "freteCarrinho");
        }
        return array(false, false, false, false, false);
    }

    static function FreteSimulacao($peso = false, $total = false) {
        $entrega = parent::Session(self::Id() . "simuladorCarrinho");
        if ($entrega) {
            if ($peso and $total) {
                list($cep, $envio, $frete_local, $frete_valor) = $entrega;
                list($frete_local, $frete_valor) = PagSeguro::Frete($cep, $envio, $peso, $total);
                return parent::Session(self::Id() . "simuladorCarrinho", array($cep, $envio, $frete_local, $frete_valor));
            }
            return parent::Session(self::Id() . "simuladorCarrinho");
        }
        return array(false, false, false, false);
    }

    static function EnderecosEntrega() {
        $enderecos = Cliente::EnderecosEntrega();
        if ($enderecos) {
            foreach ($enderecos as $key => $endereco) {
                $array[$key] = $endereco['nome'];
            }
            return $array;
        }
        return false;
    }

    static function Limpar() {
        parent::UnSession(self::Id() . "itensCarrinho");
        parent::UnSession(self::Id() . "freteCarrinho");
        parent::UnSession(self::Id() . "simuladorCarrinho");
        parent::UnSession("idCarrinho");
        return true;
    }

    static function Controle() {
        $MYSQL = parent::MySQL();
        $carrinho = self::Itens();
        $submit = parent::Post("submit");
        switch ($submit) {

            case "frete":
                $enderecos = Cliente::EnderecosEntrega();
                $entrega = parent::Post("entrega");
                $envio = parent::Post("envio");
                if (isset($enderecos[$entrega])) {
                    return parent::Session(self::Id() . "freteCarrinho", array($enderecos[$entrega]['cep'], $envio, $entrega, false, false));
                }
                return false;
                break;

            case "frete-simular":
                $cep = parent::Post("cep");
                $envio = parent::Post("envio");
                return parent::Session(self::Id() . "simuladorCarrinho", array($cep, $envio, false, false));
                break;

            case "limpar":
                return Carrinho::Limpar();
                break;

            case "voltar":
                header("Location: checkout.php");
                exit();
                break;

            case "checkout":
                header("Location: checkout.php");
                exit();
                break;

            case "finalizar":
                $itens = Carrinho::Itens();
                if ($itens) {
                    sleep(3);
                    list($cep, $envio, $endereco, $frete_local, $frete_valor) = Carrinho::Frete();
                    $enderecos = Cliente::EnderecosEntrega();
                    if (isset($enderecos[$endereco])) {
                        $pedidos['TABLE'] = 'pedidos';
                        $pedidos['FIELDS'][] = array(
                            //'pagamento' => "pagseguro",
                            'referencia' => Carrinho::Id(),
                            'frete_tipo' => $envio,
                            'frete_local' => $frete_local,
                            'frete_valor' => $frete_valor,
                            'entrega_nome' => $enderecos[$endereco]['nome'],
                            'entrega_logradouro' => $enderecos[$endereco]['logradouro'],
                            'entrega_numero' => $enderecos[$endereco]['numero'],
                            'entrega_complemento' => $enderecos[$endereco]['complemento'],
                            'entrega_bairro' => $enderecos[$endereco]['bairro'],
                            'entrega_cidade' => $enderecos[$endereco]['cidade'],
                            'entrega_uf' => $enderecos[$endereco]['uf'],
                            'entrega_cep' => $cep,
                            'idclientes' => Cliente::Id(),
                            'insert' => date("Y-m-d H:i:s")
                        );
                        $pedidos = $MYSQL->save($pedidos);
                        if (is_array($pedidos)) {
                            foreach ($itens as $item) {
                                $pedidos_produtos['TABLE'] = 'pedidos_produtos';
                                $pedidos_produtos['FIELDS'][] = array(
                                    'peso' => $item['peso'],
                                    'valor' => $item['valor'],
                                    'desconto' => $item['desconto'],
                                    'selecionado' => $item['selecionado'],
                                    'idprodutos' => $item["id"],
                                    'idpedidos' => $pedidos[0]
                                );
                                $pedidos_produtos = $MYSQL->save($pedidos_produtos);
                                if (!is_array($pedidos_produtos)) {
                                    $remove['TABLE'] = 'pedidos';
                                    $remove['WHERE'] = '`id` = "' . $pedidos[0] . '"';
                                    $MYSQL->remove($remove);
                                    return parent::Alert("Ocorreu alguns problemas ao efetuar seu pedido... Por favor, entre em contato com nosso atendimento!");
                                } else {
                                    $produto['TABLE'] = 'produtos';
                                    $produto['FIELDS'][] = array(
                                        'WHERE' => '`id` = "' . $item["id"] . '"',
                                        'quantidade' => "(`quantidade` - {$item['selecionado']})");
                                    $MYSQL->save($produto);
                                }
                                unset($pedidos_produtos);
                            }
                            Carrinho::Limpar();
                            header("Location: pedido.php?id={$pedidos[0]}");
                            exit();
                        }
                    }
                    header("Location: checkout.php");
                    exit();
                }
                break;

            default:
                $adiciona = parent::Post("adicionar");
                if ($adiciona) {
                    if (!isset($carrinho[$adiciona])) {
                        $find = $MYSQL->find(array('FIELDS' => "produtos.*, departamentos.nome as departamento", 'FROM' => 'produtos', 'JOIN' => 'LEFT JOIN departamentos on (departamentos.id = produtos.iddepartamentos)', 'WHERE' => 'departamentos.idlojas = "' . Loja::Id() . '" AND produtos.id = "' . $adiciona . '"', 'LIMIT' => '1'));
                        if (isset($find[0]['nome'])) {
                            $carrinho[$adiciona] = $find[0];
                            $carrinho[$adiciona]['selecionado'] = 1;
                        }
                    } else {
                        if ($carrinho[$adiciona]['selecionado'] < $carrinho[$adiciona]['quantidade_max']) {
                            $carrinho[$adiciona]['selecionado'] = $carrinho[$adiciona]['selecionado'] + 1;
                        } else {
                            parent::Alert("O máximo de quantidade permitida para este produto é {$carrinho[$adiciona]['quantidade_max']}!");
                        }
                    }
                }
                $remove = parent::Post("remover");
                if ($remove) {
                    if (isset($carrinho[$remove])) {
                        $carrinho[$remove]['selecionado'] = $carrinho[$remove]['selecionado'] - 1;
                        if ($carrinho[$remove]['selecionado'] < 1) {
                            unset($carrinho[$remove]);
                        }
                    }
                }
                $avisar = parent::Post("avisar");
                if ($avisar) {
                    $find = $MYSQL->find(array('FIELDS' => "produtos.*, departamentos.nome as departamento", 'FROM' => 'produtos', 'JOIN' => 'LEFT JOIN departamentos on (departamentos.id = produtos.iddepartamentos)', 'WHERE' => 'departamentos.idlojas = "' . Loja::Id() . '" AND produtos.id = "' . $avisar . '"', 'LIMIT' => '1'));
                    if (isset($find[0]['nome'])) {
                        $nome = parent::Post("nome");
                        $email = parent::Post("email");
                        if (strlen($nome) > 0 and parent::ValidaEmail($email)) {
                            $produto['TABLE'] = 'avisos';
                            $produto['FIELDS'][] = array(
                                'nome' => $nome,
                                'email' => $email,
                                'idprodutos' => $avisar
                            );
                            $MYSQL->save($produto);
                            parent::Alert("Informaremos através do email {$email} quando o produto {$find[0]['nome']} estiver disponível!");
                        } else {
                            parent::Alert("Nome ou e-mail inválido!");
                        }
                        header("Location: produto.php?id={$avisar}");
                        exit();
                    }
                    header("Location: index.php");
                    exit();
                }
                if (count($carrinho) >= 1) {
                    return parent::Session(self::Id() . "itensCarrinho", $carrinho);
                }
                return parent::UnSession(self::Id() . "itensCarrinho");
                break;
        }
    }

}
?>