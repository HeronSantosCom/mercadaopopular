﻿<?php

Class Cliente extends Core {

    static function Get() {
        $cliente = parent::Session("infoCliente");
        if (isset($cliente)) {
            return $cliente;
        }
        return false;
    }

    static function Id() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["id"];
        }
        return false;
    }

    static function Nome() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["nome"];
        }
        return false;
    }

    static function Documento() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["documento"];
        }
        return false;
    }

    static function Nascimento() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return parent::toData($cliente["nascimento"], "YYYY-MM-DD", "DD/MM/YYYY");
        }
        return false;
    }

    static function Email() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["email"];
        }
        return false;
    }

    static function Senha() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["senha"];
        }
        return false;
    }

    static function TelefoneFixo() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["telefone_fixo"];
        }
        return false;
    }

    static function TelefoneComercial() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["telefone_comercial"];
        }
        return false;
    }

    static function TelefoneCelular() {
        $cliente = self::Get();
        if (isset($cliente)) {
            return $cliente["telefone_celular"];
        }
        return false;
    }

    static function EnderecosEntrega() {
        if (self::Id()) {
            $MYSQL = parent::MySQL();
            $find = $MYSQL->find(array('FROM' => 'clientes_enderecos', 'WHERE' => '`idclientes` = "' . self::Id() . '"'));
            if (is_array($find)) {
                foreach ($find as $endereco) {
                    $array[$endereco["id"]] = $endereco;
                }
                return $array;
            }
        }
        return false;
    }

    static function Pedidos() {
        if (self::Id()) {
            $MYSQL = parent::MySQL();
            $find = $MYSQL->find(array('FROM' => 'pedidos', 'WHERE' => 'idclientes = "' . self::Id() . '"'));
            if (is_array($find)) {
                foreach ($find as $pedidos) {
                    $array[$pedidos["id"]] = $pedidos;
                }
                return $array;
            }
        }
        return false;
    }

    static function Bloqueio() {
        if (!self::Id()) {
            parent::Session("returnCliente", $_SERVER['REQUEST_URI']);
            header("Location: login.php");
            exit();
        }
        return false;
    }

    static function Login() {
        if (!self::Id()) {
            if (parent::Post("submit") == "login") {
                $email = parent::Post("email");
                $senha = parent::Post("senha");
                if (strlen($email) > 0) {
                    $MYSQL = parent::MySQL();
                    $find = $MYSQL->find(array('FROM' => 'clientes', 'WHERE' => '`email` = "' . $email . '" AND `senha` = "' . md5($senha) . '" AND `idlojas` = "' . Loja::Id() . '" AND `status` = "1"', 'LIMIT' => '1'));
                    if (isset($find[0]['nome'])) {
                        $return = parent::Session("returnCliente");
                        parent::Session("infoCliente", $find[0]);
                        parent::UnSession("returnCliente");
                        header("Location: " . ($return ? $return : "index.php"));
                        exit();
                    }
                }
                parent::Alert("E-mail ou senha informado inválido!");
            }
            return false;
        }
        header("Location: index.php");
        exit();
    }

    static function Cadastro() {
        if (!self::Id()) {
            $MYSQL = parent::MySQL();
            if (parent::Post("submit") == "cadastro") {
                $obrigatorio = array("nome", "documento", "nascimento", "cep", "telefone_fixo", "email", "confirmaemail", "senha", "confirmasenha");
                $nome = parent::Post("nome");
                $documento = parent::Post("documento");
                $nascimento = parent::Post("nascimento");
                $cep = parent::Post("cep");
                $telefone_fixo = parent::Post("telefone_fixo");
                $telefone_comercial = parent::Post("telefone_comercial");
                $telefone_celular = parent::Post("telefone_celular");
                $email = parent::Post("email");
                $confirmaemail = parent::Post("confirmaemail");
                $senha = parent::Post("senha");
                $confirmasenha = parent::Post("confirmasenha");
                foreach ($obrigatorio as $campo) {
                    if (strlen($$campo) == 0) {
                        parent::Alert("Os campos com * são obrigatórios." . $campo);
                        return false;
                    }
                }
                if (!parent::ValidaDocumento($documento)) {
                    parent::Alert("O CPF informado não é válido.");
                    return false;
                }
                if (!parent::ValidaEmail($email)) {
                    parent::Alert("O e-mail informado não é válido.");
                    return false;
                }
                if (!parent::ValidaData($nascimento)) {
                    parent::Alert("A data de nascimento informado não é válido.");
                    return false;
                }
                if ($email != $confirmaemail) {
                    parent::Alert("O endereço informado não confere com o endereço de confirmação informado...");
                } else {
                    if ($senha != $confirmasenha) {
                        parent::Alert("A senha informada difere da senha de confirmação...");
                    } else {
                        $find = $MYSQL->find(array('FROM' => 'clientes', 'WHERE' => '`email` = "' . $email . '" AND `idlojas` = "' . Loja::Id() . '"', 'LIMIT' => '1'));
                        if (isset($find[0]['nome'])) {
                            parent::Alert("Este e-mail já está cadastrado, caso não lembre de sua senha utilize o serviço de 'Esqueci minha senha'.");
                            return false;
                        }
                        $clientes['TABLE'] = 'clientes';
                        $clientes['FIELDS'][] = array(
                            'nome' => "{$nome}",
                            'documento' => "{$documento}",
                            'nascimento' => parent::toData($nascimento, "DD/MM/YYYY", "YYYY-MM-DD"),
                            'email' => "{$email}",
                            'senha' => md5($senha),
                            'telefone_fixo' => "{$telefone_fixo}",
                            'telefone_comercial' => "{$telefone_comercial}",
                            'telefone_celular' => "{$telefone_celular}",
                            'idlojas' => Loja::Id(),
                            'insert' => date("Y-m-d H:i:s")
                        );
                        $clientes = $MYSQL->save($clientes);
                        if (is_array($clientes)) {
                            $cep = parent::GetCep($cep);
                            if ($cep) {
                                $enderecos['TABLE'] = 'clientes_enderecos';
                                $enderecos['FIELDS'][] = array(
                                    'nome' => "Principal",
                                    'logradouro' => "{$cep["logradouro"]}",
                                    'bairro' => "{$cep["bairro"]}",
                                    'cidade' => "{$cep["cidade"]}",
                                    'uf' => "{$cep["uf"]}",
                                    'cep' => "{$cep["cep"]}",
                                    'idclientes' => "{$clientes[0]}",
                                );
                                $enderecos = $MYSQL->save($enderecos);
                            }
                            parent::Alert("Cadastro efetuado com sucesso!\\nAgora você já pode fazer login com sua conta...");
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        header("Location: index.php");
        exit();
    }

    static function Controle() {
        if (self::Id()) {
            $MYSQL = parent::MySQL();
            switch (parent::Post("submit")) {
                case "atualiza":
                    $obrigatorio = array("nome", "documento", "nascimento", "telefone_fixo", "email");
                    $nome = parent::Post("nome");
                    $documento = parent::Post("documento");
                    $nascimento = parent::Post("nascimento");
                    $telefone_fixo = parent::Post("telefone_fixo");
                    $telefone_comercial = parent::Post("telefone_comercial");
                    $telefone_celular = parent::Post("telefone_celular");
                    $email = parent::Post("email");
                    $senha = parent::Post("senha");
                    $confirmasenha = parent::Post("confirmasenha");
                    foreach ($obrigatorio as $campo) {
                        if (strlen($$campo) == 0) {
                            parent::Alert("Os campos com * são obrigatórios." . $campo);
                            return false;
                        }
                    }
                    if (!parent::ValidaDocumento($documento)) {
                        parent::Alert("O CPF informado não é válido.");
                        return false;
                    }
                    if (!parent::ValidaEmail($email)) {
                        parent::Alert("O e-mail informado não é válido.");
                        return false;
                    }
                    if (!parent::ValidaData($nascimento)) {
                        parent::Alert("A data de nascimento informado não é válido.");
                        return false;
                    }
                    if (strlen($senha) > 0) {
                        if ($senha != $confirmasenha) {
                            parent::Alert("A senha informada difere da senha de confirmação...");
                            return false;
                        }
                        $senha = md5($senha);
                    } else {
                        $senha = self::Senha();
                    }
                    if ($email != self::Email()) {
                        $find = $MYSQL->find(array('FROM' => 'clientes', 'WHERE' => '`email` = "' . $email . '" AND `idlojas` = "' . Loja::Id() . '"', 'LIMIT' => '1'));
                        if (isset($find[0]['nome'])) {
                            parent::Alert("Este e-mail já está cadastrado!");
                            return false;
                        }
                    }
                    $array = array(
                        'WHERE' => '`id` = "' . self::Id() . '"',
                        'nome' => "{$nome}",
                        'documento' => "{$documento}",
                        'nascimento' => parent::toData($nascimento, "DD/MM/YYYY", "YYYY-MM-DD"),
                        //'email' => "{$email}",
                        'senha' => "{$senha}",
                        'telefone_fixo' => "{$telefone_fixo}",
                        'telefone_comercial' => "{$telefone_comercial}",
                        'telefone_celular' => "{$telefone_celular}");
                    $clientes['TABLE'] = 'clientes';
                    $clientes['FIELDS'][] = $array;
                    $clientes = $MYSQL->save($clientes);
                    if (is_array($clientes)) {
                        $array['id'] = self::Id();
                        parent::Session("infoCliente", $array);
                        parent::Alert("Cadastro alterado com sucesso!");
                        return true;
                    }
                    parent::Alert("Problemas ao alterar seu cadastro...");
                    break;
                case "novo-endereco":
                    $obrigatorio = array("nome", "logradouro", "numero", "bairro", "cidade", "uf", "cep");
                    $nome = parent::Post("nome");
                    $logradouro = parent::Post("logradouro");
                    $numero = parent::Post("numero");
                    $complemento = parent::Post("complemento");
                    $bairro = parent::Post("bairro");
                    $cidade = parent::Post("cidade");
                    $uf = parent::Post("uf");
                    $cep = parent::Post("cep");
                    $passed = false;
                    $old_enderecos = self::EnderecosEntrega();
                    if ($old_enderecos) {
                        foreach ($old_enderecos as $key => $endereco) {
                            if (parent::Get("id") == $key) {
                                $passed = true;
                            } else {
                                if (strtolower($endereco["nome"]) == strtolower($nome)) {
                                    parent::Alert("Este endereço já está cadastrado!");
                                    return false;
                                }
                            }
                        }
                    }
                    $enderecos['TABLE'] = 'clientes_enderecos';
                    if (parent::Get("id")) {
                        if ($passed) {
                            $enderecos['FIELDS'][] = array(
                                'WHERE' => '`id` = "' . parent::Get("id") . '"',
                                'nome' => "{$nome}",
                                'logradouro' => "{$logradouro}",
                                'numero' => "{$numero}",
                                'complemento' => "{$complemento}",
                                'bairro' => "{$bairro}",
                                'cidade' => "{$cidade}",
                                'uf' => "{$uf}",
                                'cep' => "{$cep}"
                            );
                            $enderecos = $MYSQL->save($enderecos);
                            if (is_array($enderecos)) {
                                parent::Alert("Endereço alterado com sucesso!");
                                header("Location: cliente.php");
                                exit();
                            }
                        }
                        parent::Alert("Problemas ao alterar seu endereço...");
                    }
                    $enderecos['FIELDS'][] = array(
                        'nome' => "{$nome}",
                        'logradouro' => "{$logradouro}",
                        'numero' => "{$numero}",
                        'complemento' => "{$complemento}",
                        'bairro' => "{$bairro}",
                        'cidade' => "{$cidade}",
                        'uf' => "{$uf}",
                        'cep' => "{$cep}",
                        'idclientes' => self::Id()
                    );
                    $enderecos = $MYSQL->save($enderecos);
                    if (is_array($enderecos)) {
                        parent::Alert("Endereço cadastrado com sucesso!");
                        header("Location: cliente.php");
                        exit();
                    }
                    parent::Alert("Problemas ao cadastrar seu endereço...");
                    break;
                case "remove-endereco":
                    if (parent::Get("id")) {
                        $enderecos = self::EnderecosEntrega();
                        if ($enderecos) {
                            if (in_array(parent::Get("id"), array_keys($enderecos))) {
                                $enderecos['TABLE'] = 'clientes_enderecos';
                                $enderecos['WHERE'] = '`id` = "' . parent::Get("id") . '"';
                                if ($MYSQL->remove($enderecos)) {
                                    parent::Alert("Endereço removido com sucesso!");
                                    header("Location: cliente.php");
                                    exit();
                                }
                            }
                        }
                    }
                    parent::Alert("Problemas ao remover seu endereço...");
                    break;
            }
            return false;
        }
        header("Location: index.php");
        exit();
    }

    static function Logoff() {
        if (self::Id()) {
            parent::UnSession("infoCliente");
            header("Location: login.php");
            exit();
        }
        header("Location: index.php");
        exit();
    }

    static function RenovaSenha() {
        if (!self::Id()) {
            $MYSQL = parent::MySQL();
            $email = parent::Post("email");
            if (strlen($email) > 0) {
                $confirmaemail = parent::Post("confirmaemail");
                if ($email == $confirmaemail) {
                    $find = $MYSQL->find(array('FROM' => 'clientes', "FIELDS" => "clientes.*, md5(concat(date(now()), clientes.email)) as 'token'", 'WHERE' => '`email` = "' . $email . '" AND `idlojas` = "' . Loja::Id() . '" AND status = "1"', 'LIMIT' => '1'));
                    if (isset($find[0]['token'])) {
                        $array[] = "<h1>" . Loja::Titulo() . "</h1>";
                        $array[] = "<p>Esta é uma mensagem automática. Por favor, não a responda.</p>";
                        $array[] = "<p>";
                        $array[] = "Prezado(a) Heron Santos,<br />";
                        $array[] = "Conforme sua solicitação, clique no link para definir uma nova senha:";
                        $array[] = "</p>";
                        $array[] = "<p>";
                        $array[] = "<ul>";
                        $array[] = "<li><a href='{$_SERVER["SCRIPT_URI"]}?token=" . $find[0]['token'] . "'>Clique aqui!</li>";
                        $array[] = "</ul>";
                        $array[] = "</p>";
                        $array[] = "<p>";
                        $array[] = "Atenciosamente,<br />";
                        $array[] = "Equipe " . Loja::Titulo() . "<br />";
                        $array[] = Loja::Site();
                        $array[] = "</p>";
                        if (parent::Email($find[0]['nome'], $find[0]['email'], "Lembrete de senha de {$find[0]['nome']}...", join("\n", $array))) {
                            parent::Alert("Em instantes, você receberá um e-mail com sua senha de identificação.");
                            header("Location: login.php");
                            exit();
                        }
                        parent::Alert("Problemas ao enviar sua senha de identificação, tente novamente mais tarde...");
                        return false;
                    }
                    parent::Alert("Não foi possível enviar sua senha. Por favor, verifique o e-mail informado...");
                    return false;
                }
                parent::Alert("O endereço informado não confere com o endereço de confirmação informado...");
            } else {
                $token = parent::Get("token");
                $senha = parent::Post("senha");
                if (strlen($token) > 0) {
                    $find = $MYSQL->find(array('FROM' => 'clientes', 'WHERE' => 'md5(concat(date(now()), clientes.email)) = "' . $token . '"', 'LIMIT' => '1'));
                    if (isset($find[0]['nome'])) {
                        if (strlen($senha) > 0) {
                            $confirmasenha = parent::Post("confirmasenha");
                            if ($senha == $confirmasenha) {
                                $clientes['TABLE'] = 'clientes';
                                $clientes['FIELDS'][] = array(
                                    'WHERE' => '`id` = "' . $find[0]['id'] . '"',
                                    'senha' => md5($senha)
                                );
                                $clientes = $MYSQL->save($clientes);
                                if (is_array($clientes)) {
                                    parent::Alert("Sua senha foi alterada com sucesso...");
                                    header("Location: login.php");
                                    exit();
                                }
                                parent::Alert("Problemas ao alterar sua senha de identificação, tente novamente mais tarde...");
                                return false;
                            }
                            parent::Alert("A senha informada difere da senha de confirmação...");
                            return false;
                        }
                    } else {
                        header("Location: index.php");
                        exit();
                    }
                }
            }
        }
        return false;
    }

}
?>