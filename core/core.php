﻿<?php

Class Core {
    const db_server = "mysql";
    const db_user = "";
    const db_password = "";
    const db_name = "";
    
    const smtp_server = "smtp.gmail.com";
    const smtp_auth = true;
    const smtp_ssl = true;
    const smtp_port = 465;
    const smtp_user = "";
    const smtp_pass = "";
    
    const redirect = "http://www.mercadaopopular.com.br";

    static function Iniciar() {
        header("Content-type: text/html; charset=utf8");
        setlocale(LC_ALL, "pt_BR");
        date_default_timezone_set("America/Sao_Paulo");
        error_reporting(1);
        ini_set('error_reporting', E_ALL);
        ini_set("display_errors", 1);
        session_start();
        include '../painel/core/class/mysql.php';
        include '../painel/core/class/smtp.php';
        include '../painel/core/class/pagseguro.php';
        include '../core/loja.php';
        include '../core/pagseguro.php';
        include '../core/theme.php';
        include '../core/cliente.php';
        include '../core/departamento.php';
        include '../core/produto.php';
        include '../core/carrinho.php';
        include '../core/pedido.php';
        if (!loja::Get()) {
            header("Location: " . self::redirect);
            exit();
        }
    }

    static function Site() {
        return $_SERVER["HTTP_HOST"];
    }

    static function CertID() {
        return md5(session_id() . self::Site());
    }

    static function Session($name, $value = false) {
        $name = strtolower($name);
        if ($value) {
            return $_SESSION[self::CertID()][$name] = $value;
        } else {
            if (isset($_SESSION[self::CertID()][$name])) {
                return $_SESSION[self::CertID()][$name];
            }
        }
        return false;
    }

    static function UnSession($name, $return = false) {
        $name = strtolower($name);
        if (isset($_SESSION[self::CertID()][$name])) {
            unset($_SESSION[self::CertID()][$name]);
        }
        return $return;
    }

    static function Get($name) {
        if (isset($_GET[$name])) {
            return trim(mysql_escape_string($_GET[$name]));
        }
        return false;
    }

    static function Post($name) {
        if (isset($_POST[$name])) {
            return trim(mysql_escape_string($_POST[$name]));
        }
        return false;
    }

    static function Email($name, $email, $subject, $body) {
        $smtp = new Smtp(self::smtp_server, self::smtp_ssl, self::smtp_port);
        $smtp->user = self::smtp_user;
        $smtp->pass = self::smtp_pass;
        $smtp->auth = self::smtp_auth;
        $smtp->charset = "utf-8";
        $smtp->debug = false;
        if ($smtp->Send(Loja::Titulo(), Loja::Email(), $name, $email, $subject, $body, false, false, "text/html")) {
            $smtp->Close();
            return true;
        }
        return false;
    }

    static function GetCep($cep) {
        $cep = trim(str_replace(array("-", " "), "", $cep));
        if (strlen($cep) > 0) {
            $cep = utf8_encode(urldecode(self::External("http://www.lanstore.com.br/consCep.asp?cep={$cep}")));
            $cep = explode("|", $cep);
            if (isset($cep[4])) {
                $array["logradouro"] = $cep[0];
                $array["bairro"] = $cep[1];
                $array["cidade"] = $cep[2];
                $array["uf"] = $cep[3];
                $array["cep"] = $cep[4];
                return $array;
            }
        }
        return false;
    }

    static function ValidaDocumento($documento) {
        $documento = trim(str_replace(array("-", ".", " "), "", $documento));
        $status = false;
        if (!is_numeric($documento)) {
            $status = false;
        } else {
            if (($documento == '11111111111') || ($documento == '22222222222') || ($documento == '33333333333') || ($documento == '44444444444') || ($documento == '55555555555') || ($documento == '66666666666') || ($documento == '77777777777') || ($documento == '88888888888') || ($documento == '99999999999') || ($documento == '00000000000')) {
                $status = false;
            } else {
                $dv_informado = substr($documento, 9, 2);
                for ($i = 0; $i <= 8; $i++) {
                    $digito[$i] = substr($documento, $i, 1);
                }
                $posicao = 10;
                $soma = 0;
                for ($i = 0; $i <= 8; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }
                $digito[9] = $soma % 11;
                if ($digito[9] < 2) {
                    $digito[9] = 0;
                } else {
                    $digito[9] = 11 - $digito[9];
                }
                $posicao = 11;
                $soma = 0;
                for ($i = 0; $i <= 9; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }
                $digito[10] = $soma % 11;
                if ($digito[10] < 2) {
                    $digito[10] = 0;
                } else {
                    $digito[10] = 11 - $digito[10];
                }
                $dv = $digito[9] * 10 + $digito[10];
                if ($dv != $dv_informado) {
                    $status = false;
                } else {
                    $status = true;
                }
            }
        }
        return $status;
    }

    static function ValidaData($data, $delimitador = '/') {
        if (!isset($data) or strlen($data) != 10) {
            return false;
        } else {
            list($dd, $mm, $yy) = explode($delimitador, $data);
            if (!is_null($dd) or !is_null($mm) or !is_null($yy))
                if (checkdate($mm, $dd, $yy))
                    return true;
        }
        return false;
    }

    static function ValidaEmail($email, $checkdomain = false) {
        if ($email != "default@localhost") {
            $email = explode("@", $email);
            if (count($email) != 2) {
                return false;
            }
            list($user, $domain) = $email;
            if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*$/xi", $user)) {
                return false;
            }
            if ($checkdomain) {
                if (!checkdnsrr($domain, "MX")) {
                    return false;
                }
            }
        }
        return true;
    }

    static function toData($data, $formato_atual, $formato_novo) {
        $formato_atual = strtoupper($formato_atual);
        $formato_novo = strtoupper($formato_novo);
        $dia = substr($data, strpos($formato_atual, "DD"), 2);
        $data_nova = str_replace("DD", $dia, $formato_novo);
        $mes = substr($data, strpos($formato_atual, "MM"), 2);
        $data_nova = str_replace("MM", $mes, $data_nova);
        $ano = substr($data, strpos($formato_atual, "YYYY"), 4);
        $data_nova = str_replace("YYYY", $ano, $data_nova);
        if (strlen($formato_atual) == 8) {
            $ano = substr($data, strpos($formato_atual, "YY"), 2);
            $data_nova = str_replace("YY", $ano, $data_nova);
        }
        return $data_nova;
    }

    static function MySQL() {
        if (isset($_ENV["MYSQL"])) {
            return $_ENV["MYSQL"];
        }
        $_ENV["MYSQL"] = new Sql(self::db_server, self::db_user, self::db_password, self::db_name);
        return $_ENV["MYSQL"];
    }

    static function External($uri, $method = "curl") {
        $contents = false;
        switch (strtolower($method)) {
            case "fopen":
                ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                $pointer = @fopen($uri, "r");
                if ($pointer) {
                    $contents = null;
                    while (!feof($pointer)) {
                        $contents .= @ fgets($pointer, 4096);
                    }
                    fclose($pointer);
                }
                break;
            case "curl":
            default:
                $pointer = curl_init($uri);
                curl_setopt($pointer, CURLOPT_TIMEOUT, 15);
                curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
                ob_start();
                curl_exec($pointer);
                curl_close($pointer);
                $contents = ob_get_contents();
                ob_end_clean();
                break;
        }
        if (eregi('301 Moved Permanently', $contents)) {
            preg_match("/HREF=\"(.*)\"/si", $contents, $pointer);
            return self::External($pointer[1], $method);
        }
        return $contents;
    }

    static function Populate($array, $selected = false, $first_null = true) {
        if (is_array($array)) {
            if ($first_null) {
                print "<option value=''>Selecione...</option>";
            }
            foreach ($array as $key => $value) {
                $option = ($selected == $key ? " selected" : null);
                print "<option value='{$key}'{$option}>{$value}</option>";
            }
        }
    }

    static function IsPage($page) {
        if (preg_match("#{$page}#", $_SERVER["SCRIPT_FILENAME"])) {
            return true;
        }
        return false;
    }

    static function Alert($message = false) {
        if ($message) {
            return self::Session("alertSystem", $message);
        }
        $message = self::Session("alertSystem");
        if ($message) {
            echo "<script type='text/javascript'>";
            echo "alert('" . addslashes(self::Session("alertSystem")) . "')";
            echo "</script>";
            return self::UnSession("alertSystem");
        }
        return false;
    }

    static function Log($message, $filename = "system.log") {
        if (!is_array($message)) {
            $message = str_replace("\r\n", "\n", $message);
            $message = str_replace("\n\r", "\n", $message);
            $message = explode("\n", $message);
        }
        $date = date('Y-m-d H:i:s');
        if (isset($_SERVER["REMOTE_ADDR"])) {
            $host = $_SERVER["REMOTE_ADDR"];
            $pid = uniqid();
        } else {
            $host = exec('whoami');
            $pid = getmypid();
        }
        foreach ($message as $line) {
            if (strlen(trim($line)) > 0) {
                $print[] = "[{$date}] [{$host}] [{$pid}] {$line}";
            }
        }
        if (isset($print)) {
            $string = str_replace("´", "\´", join("\n", $print));
            $string = str_replace("`", "\`", $string);
            $string = str_replace('"', '\"', $string);
            exec('echo "' . $string . '" >> ' . $filename);
        }
    }

}

Core::Iniciar();
?>