﻿<?php

Class Departamento extends Core {

    static function Sub($id = false) {
        $MYSQL = parent::MySQL();
        $find = $MYSQL->find(array('FIELDS' => "*", 'FROM' => 'departamentos', 'WHERE' => 'idlojas = "' . Loja::Id() . '" AND iddepartamentos ' . ($id ? " = '{$id}'" : "is null"), "ORDER" => "nome"));
        if (isset($find[0]['id'])) {
            return $find;
        }
        return false;
    }

    static function Get() {
        $id = parent::Get("id");
        if ($id) {
            $MYSQL = parent::MySQL();
            $find = $MYSQL->find(array('FROM' => 'departamentos', 'WHERE' => 'idlojas = "' . Loja::Id() . '" AND id = "' . $id . '"', 'LIMIT' => '1'));
            if (isset($find[0]['nome'])) {
                return $find[0];
            }
        }
        return false;
    }

    static function Id() {
        $departamento = self::Get();
        if (isset($departamento)) {
            return $departamento["id"];
        }
        return false;
    }

    static function Nome() {
        $departamento = self::Get();
        if (isset($departamento)) {
            return $departamento["nome"];
        }
        return false;
    }

    static function Products() {
        $departamento = self::Id();
        
    }

}
?>