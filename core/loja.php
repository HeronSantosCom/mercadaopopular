﻿<?php

Class Loja extends Core {
    const description = "";
    const keywords = "";

    static function Get() {
        $loja = parent::Session("infoLoja");
        if ($loja) {
            if (preg_match("#{$loja["url"]}#", parent::Site())) {
                return $loja;
            }
        }
        $MYSQL = parent::MySQL();
        $find = $MYSQL->find(array('FIELDS' => "*", 'FROM' => 'lojas', 'WHERE' => 'url like "%://' . parent::Site() . '%" AND status = "1"', 'LIMIT' => '1'));
        if (isset($find[0]['id'])) {
            return parent::Session("infoLoja", $find[0]);
        }
        return false;
    }

    static function Id() {
        $loja = self::Get();
        return $loja["id"];
    }

    static function Email() {
        $loja = self::Get();
        return $loja["email"];
    }

    static function Endereco() {
        $loja = self::Get();
        $array["logradouro"] = $loja["endereco_logradouro"];
        $array["numero"] = $loja["endereco_numero"];
        $array["complemento"] = $loja["endereco_complemento"];
        $array["bairro"] = $loja["endereco_bairro"];
        $array["cidade"] = $loja["endereco_cidade"];
        $array["uf"] = $loja["endereco_uf"];
        $array["cep"] = $loja["endereco_cep"];
        return $array;
    }

    static function Telefones() {
        $loja = self::Get();
        $array["fixo"] = array(false, false);
        $array["comercial"] = array(false, false);
        $array["celular"] = array(false, false);
        if (strlen($loja["telefone_fixo"]) > 0) {
            $array["fixo"] = array(substr($loja["telefone_fixo"], 0, strpos("-", $loja["telefone_fixo"])), substr($loja["telefone_fixo"], strpos("-", $loja["telefone_fixo"]) + 1));
        }
        if (strlen($loja["telefone_comercial"]) > 0) {
            $array["comercial"] = array(substr($loja["telefone_comercial"], 0, strpos("-", $loja["telefone_comercial"])), substr($loja["telefone_comercial"], strpos("-", $loja["telefone_comercial"]) + 1));
        }
        if (strlen($loja["telefone_celular"]) > 0) {
            $array["celular"] = array(substr($loja["telefone_celular"], 0, strpos("-", $loja["telefone_celular"])), substr($loja["telefone_celular"], strpos("-", $loja["telefone_celular"]) + 1));
        }
        return $array;
    }

    static function Titulo() {
        $loja = self::Get();
        return $loja["titulo"];
    }

    static function Site() {
        $loja = self::Get();
        if (substr($loja["url"], strlen($loja["url"] - 1), strlen($loja["url"])) == "/") {
            $loja["url"] = substr($loja["url"], 0, strlen($loja["url"] - 1));
        }
        return $loja["url"];
    }

    static function Tema() {
        $loja = self::Get();
        return $loja["tema"];
    }

}
?>