<?php

Class PagSeguro extends Core {

    static function Id() {
        $loja = Loja::Get();
        if (isset($loja["pagseguro"])) {
            return $loja["pagseguro"];
        }
        return false;
    }

    static function Token() {
        $loja = Loja::Get();
        if (isset($loja["pagseguro_token"])) {
            return $loja["pagseguro_token"];
        }
        return false;
    }

    static function Frete($cep, $envio, $peso, $valor) {
        $peso = str_replace('.', ',', $peso);
        $valor = str_replace('.', '', number_format((float) $valor, 2, ",", "."));
        $return = @file_get_contents("https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete_calcular.jhtml?postalCodeFrom=26256100&weight={$peso}&value={$valor}&postalCodeTo={$cep}");
        if (strlen($return) > 0) {
            $result = explode('|', $return);
            if ($result[0] == 'ok') {
                return array($result[1] . ($result[2] == "true" ? " (capital)" : " (interior)"), ($envio == "SD" ? $result[3] : $result[4]));
            }
        }
        return array(false, false);
    }

}

?>