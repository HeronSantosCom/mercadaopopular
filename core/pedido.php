﻿<?php

Class Pedido extends Core {

    static function Get() {
        $id = parent::Get("id");
        if ($id) {
            $MYSQL = parent::MySQL();
            $pedidos = $MYSQL->find(array('FROM' => 'pedidos', 'WHERE' => 'idclientes = "' . Cliente::Id() . '" AND id = "' . $id . '"'));
            if (isset($pedidos[0]['id'])) {
                return parent::Session("ultimoPedido", $pedidos[0]);
            }
        }
        return false;
    }

    static function Id() {
        $pedido = self::Get();
        if (isset($pedido["id"])) {
            return $pedido["id"];
        }
        return false;
    }

    static function Referencia() {
        $pedido = self::Get();
        if (isset($pedido["referencia"])) {
            return $pedido["referencia"];
        }
        return false;
    }

    static function Itens() {
        $idpedidos = self::Id();
        if ($idpedidos) {
            $MYSQL = parent::MySQL();
            $produtos = $MYSQL->find(array('FIELDS' => '`produtos`.`id` AS `id`, `produtos`.`nome` AS `nome`, `pedidos_produtos`.`peso` AS `peso`, `pedidos_produtos`.`valor` AS `valor`, `pedidos_produtos`.`desconto` AS `desconto`, `pedidos_produtos`.`selecionado` AS `selecionado`', 'FROM' => 'pedidos_produtos', 'JOIN' => 'left join `produtos` on (`produtos`.`id` = `pedidos_produtos`.`idprodutos`)', 'WHERE' => '`pedidos_produtos`.`idpedidos` = "' . $idpedidos . '"'));
            if (isset($produtos[0]['id'])) {
                return $produtos;
            }
        }
        return false;
    }

    static function Transacoes() {
        $referencia = self::Referencia();
        if ($referencia) {
            $MYSQL = parent::MySQL();
            $pagseguro = $MYSQL->find(array('FROM' => 'pagseguro', 'WHERE' => 'Referencia = "' . $referencia . '"', 'ORDER' => 'DataTransacao asc'));
            if (isset($pagseguro[0]['id'])) {
                return $pagseguro;
            }
        }
        return false;
    }

}
?>