﻿<?php

Class Produto extends Core {
    const path_image = "../userfiles/novaloja/big/";
    const path_image_thumb = "../userfiles/novaloja/small/";
    const total_rows = 20;

    static function Catalogo($departamento = false) {
        $MYSQL = parent::MySQL();
        $page = parent::Get("pagina");
        $list = array('FIELDS' => "produtos.*, departamentos.nome as departamento", 'FROM' => 'produtos', 'JOIN' => 'LEFT JOIN departamentos on (departamentos.id = produtos.iddepartamentos)', 'WHERE' => ($departamento ? "produtos.iddepartamentos = '{$departamento}'" : "departamentos.idlojas = '" . Loja::Id() . "'"), "ORDER" => "produtos.nome", 'OTHERS' => "LIMIT " . ($page > 0 ? ($page - 1 ) * self::total_rows : 0) . ',' . self::total_rows);
        $find = $MYSQL->find($list);
        if (isset($find[0]['id'])) {
            return array($find, ceil($MYSQL->total_rows / self::total_rows));
        }
        return array(false, false);
    }

    static function Fotos($idprodutos = false) {
        if (!$idprodutos) {
            $idprodutos = self::Id();
        }
        if ($idprodutos) {
            $MYSQL = parent::MySQL();
            $find = $MYSQL->find(array('FROM' => 'produtos_fotos', 'WHERE' => 'idprodutos = "' . $idprodutos . '"'));
            if (isset($find[0]['arquivo'])) {
                foreach ($find as $foto) {
                    $thumbnail = self::path_image_thumb . $foto["thumbnail"];
                    if (file_exists($thumbnail)) {
                        $arquivo = self::path_image . $foto["arquivo"];
                        if (!file_exists($arquivo)) {
                            $arquivo = false;
                        }
                        $foto["arquivo"] = $arquivo;
                        $foto["thumbnail"] = $thumbnail;
                        $array[] = $foto;
                    }
                }
                return (isset($array) ? $array : false);
            }
        }
        return false;
    }

    static function Get() {
        $id = parent::Get("id");
        if ($id) {
            $MYSQL = parent::MySQL();
            $find = $MYSQL->find(array('FIELDS' => "produtos.*, departamentos.nome as departamento", 'FROM' => 'produtos', 'JOIN' => 'LEFT JOIN departamentos on (departamentos.id = produtos.iddepartamentos)', 'WHERE' => 'departamentos.idlojas = "' . Loja::Id() . '" AND produtos.id = "' . $id . '"', 'LIMIT' => '1'));
            if (isset($find[0]['nome'])) {
                return $find[0];
            }
        }
        return false;
    }

    static function Id() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["id"];
        }
        return false;
    }

    static function Nome() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["nome"];
        }
        return false;
    }

    static function Departamento() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["departamento"];
        }
        return false;
    }

    static function idDepartamento() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["iddepartamentos"];
        }
        return false;
    }

    static function Descricao() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["descricao"];
        }
        return false;
    }

    static function Especificacao() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["especificacao"];
        }
        return false;
    }

    static function Garantia() {
        $produto = self::Get();
        if (isset($produto)) {
            return $produto["garantia"];
        }
        return false;
    }

    static function Preco() {
        $produto = self::Get();
        if (isset($produto)) {
            return (float) $produto["valor"];
        }
        return false;
    }

    static function Quantidade() {
        $produto = self::Get();
        if (isset($produto)) {
            return (int) $produto["quantidade"] - ($produto["quantidade_min"] + 1);
        }
        return false;
    }

    static function Desconto() {
        $produto = self::Get();
        if (isset($produto)) {
            if ($produto["valor"] > $produto["desconto"]) {
                return (float) $produto["desconto"];
            }
        }
        return false;
    }

}
?>