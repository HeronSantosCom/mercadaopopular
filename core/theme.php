<?php

Class Theme extends Core {

    static function Images($file) {
        return "themes/" . Loja::Tema() . "/images/{$file}";
    }

    static function Style($file = "style.css") {
        return "themes/" . Loja::Tema() . "/{$file}";
    }

    static function Script($file = "script.js") {
        return "themes/" . Loja::Tema() . "/{$file}";
    }

}

?>