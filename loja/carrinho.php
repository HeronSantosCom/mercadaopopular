﻿<?php
include_once "../core/core.php";
Carrinho::Controle();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <?php
            $carrinho = Carrinho::Itens();
            if (is_array($carrinho)) {
            ?>
                <form action="" method="POST">
                    <fieldset>
                        <legend>Produtos selecionados</legend>
                    <?php
                    $peso = 0;
                    $total = 0;
                    $desconto = 0;
                    $frete = 0;
                    echo "<table>";
                    echo "<tr>";
                    echo "<td>Nome</td>";
                    echo "<td width='20px' align='center'></td>";
                    echo "<td width='50px' align='center'>Qtd.</td>";
                    echo "<td width='20px' align='center'></td>";
                    echo "<td width='100px' align='right'>Preço</td>";
                    echo "<td width='100px' align='right'>Subtotal</td>";
                    echo "</tr>";
                    foreach ($carrinho as $produto) {
                        $valor = (float) $produto["valor"];
                        $subtotal = $valor * $produto["selecionado"];
                        $subtotal_desconto = ($valor - (float) $produto["desconto"]) * $produto["selecionado"];
                        $desconto += (float) $produto["desconto"];
                        $total += $subtotal_desconto;
                        if ($produto["peso"] == 0) {
                            $produto["nome"] .= " <b>(frete grátis)</b>";
                        } else {
                            $peso += $produto["peso"];
                        }
                        echo "<tr>";
                        echo "<td>{$produto["nome"]}</td>";
                        echo "<td align='center'><button type='submit' name='remover' value='{$produto["id"]}'>-</button></td>";
                        echo "<td align='center'>{$produto["selecionado"]}</td>";
                        echo "<td align='center'><button type='submit' name='adicionar' value='{$produto["id"]}'>+</button></td>";
                        echo "<td align='right'>R$ " . number_format($valor, 2, ',', '') . "</td>";
                        echo "<td align='right'>R$ " . number_format($subtotal, 2, ',', '') . "</td>";
                        echo "</tr>";
                    }
                    list($cep, $envio, $frete_local, $frete_valor) = Carrinho::FreteSimulacao($peso, $total);
                    if ($frete_local) {
                        echo "<tr>";
                        echo "<td colspan='5'>Frete - {$cep} ({$frete_local})</td>";
                        echo "<td align='right'>R$ " . number_format($frete_valor, 2, ',', '') . "</td>";
                        echo "</tr>";
                        $total += $frete_valor;
                    }
                    if ($desconto) {
                        echo "<tr>";
                        echo "<td colspan='5'>Desconto</td>";
                        echo "<td align='right'>- R$ " . number_format($desconto, 2, ',', '') . "</td>";
                        echo "</tr>";
                    }
                    echo "<tr>";
                    echo "<td colspan='5'>Total</td>";
                    echo "<td align='right'>R$ " . number_format($total, 2, ',', '') . "</td>";
                    echo "</tr>";
                    echo "</table>";
                    ?>
                </fieldset>
                <fieldset>
                    <legend>Simular Frete</legend>
                    <ul>
                        <li>
                            <label>CEP: *</label><input name="cep" id="cep" type="text" value="<?php echo $cep; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Envio: </label>
                            <select name="envio" id="envio">
                                <?php Core::Populate(array("EN" => "Encomenda Normal (PAC)", "SD" => "Sedex"), $envio, false); ?>
                            </select>
                        </li>
                        <li>
                            <button type="submit" name="submit" value="frete-simular">simular frete para este cep</button>
                        </li>
                    </ul>
                </fieldset>
                <ul>
                    <li>
                        <button type="submit" name="submit" value="limpar">limpar carrinho</button>
                    </li>
                    <li>
                        <button type="submit" name="submit" value="checkout">efetuar compra</button>
                    </li>
                </ul>
            </form>
            <?php
                            }
            ?>
                        </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>