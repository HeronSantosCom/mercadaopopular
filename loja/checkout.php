﻿<?php
include_once "../core/core.php";
Carrinho::Controle();
Cliente::Bloqueio();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <?php
            //$transacao = Carrinho::Id();
            $carrinho = Carrinho::Itens();
            if (is_array($carrinho)) {
            ?>
                <form action="" method="POST">
                    <fieldset>
                        <legend>Produtos selecionados</legend>
                    <?php
                    $peso = 0;
                    $total = 0;
                    $desconto = 0;
                    $frete = 0;
                    echo "<table>";
                    echo "<tr>";
                    echo "<td>Nome</td>";
                    echo "<td width='50px' align='center'>Qtd.</td>";
                    echo "<td width='100px' align='right'>Preço</td>";
                    echo "<td width='100px' align='right'>Subtotal</td>";
                    echo "</tr>";
                    foreach ($carrinho as $produto) {
                        $valor = (float) $produto["valor"];
                        $subtotal = $valor * $produto["selecionado"];
                        $subtotal_desconto = ($valor - (float) $produto["desconto"]) * $produto["selecionado"];
                        $desconto += (float) $produto["desconto"];
                        $total += $subtotal_desconto;
                        if ($produto["peso"] == 0) {
                            $produto["nome"] .= " <b>(frete grátis)</b>";
                        } else {
                            $peso += $produto["peso"];
                        }
                        echo "<tr>";
                        echo "<td>{$produto["nome"]}</td>";
                        echo "<td align='center'>{$produto["selecionado"]}</td>";
                        echo "<td align='right'>R$ " . number_format($valor, 2, ',', '') . "</td>";
                        echo "<td align='right'>R$ " . number_format($subtotal, 2, ',', '') . "</td>";
                        echo "</tr>";
                    }
                    list($cep, $envio, $endereco, $frete_local, $frete_valor) = Carrinho::Frete($peso, $total);
                    if ($frete_local) {
                        echo "<tr>";
                        echo "<td colspan='3'>Frete - {$cep} ({$frete_local})</td>";
                        echo "<td align='right'>R$ " . number_format($frete_valor, 2, ',', '') . "</td>";
                        echo "</tr>";
                        $total += $frete_valor;
                    }
                    if ($desconto) {
                        echo "<tr>";
                        echo "<td colspan='3'>Desconto</td>";
                        echo "<td align='right'>- R$ " . number_format($desconto, 2, ',', '') . "</td>";
                        echo "</tr>";
                    }
                    echo "<tr>";
                    echo "<td colspan='3'>Total</td>";
                    echo "<td align='right'>R$ " . number_format($total, 2, ',', '') . "</td>";
                    echo "</tr>";
                    echo "</table>";
                    ?>
                </fieldset>
                <fieldset>
                    <legend>Endereço e Forma de Entrega</legend>
                    <ul>
                        <li>
                            <label>Endereço: </label>
                            <select name="entrega" id="entrega">
                                <?php Core::Populate(Carrinho::EnderecosEntrega(), $endereco, true); ?>
                            </select>
                        </li>
                        <li>
                            <label>Envio: </label>
                            <select name="envio" id="envio">
                                <?php Core::Populate(array("EN" => "Encomenda Normal (PAC)", "SD" => "Sedex"), $envio, false); ?>
                            </select>
                        </li>
                        <li>
                            <button type="submit" name="submit" value="frete">selecionar endereço e forma de envio</button>
                        </li>
                    </ul>
                </fieldset>
                <button type="submit" name="submit" value="voltar">alterar pedido</button>
                <?php
                                $enderecos = Cliente::EnderecosEntrega();
                                if (isset($enderecos[$endereco])) {
                ?>
                                    <button type="submit" name="submit" value="finalizar">fechar pedido</button>
                <?php
                                }
                ?>
                            </form>
            <?php
                            }
            ?>
                        </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>