<?php
include_once "../core/core.php";
Cliente::Bloqueio();
Cliente::Controle();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <form action="" method="POST">
                <p>Os campos com * são obrigatórios.</p>
                <fieldset>
                    <legend>Dados de acesso</legend>
                    <ul>
                        <li>
                            <label>E-mail:</label><input name="email" id="email" type="text" value="<?php echo Cliente::Email(); ?>" readonly />
                        </li>
                        <li>
                            <label>Senha:</label><input name="senha" id="senha" type="password" />
                        </li>
                        <li>
                            <label>Confirme a senha:</label><input name="confirmasenha" id="confirmasenha" type="password" class="" />
                        </li>
                    </ul>
                </fieldset>
                <fieldset>
                    <legend>Seus dados pessoais</legend>
                    <ul>
                        <li>
                            <label>Nome: *</label><input name="nome" id="nome" type="text" value="<?php echo Cliente::Nome(); ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>CPF: *</label><input name="documento" id="documento" type="text" value="<?php echo Cliente::Documento(); ?>" class="obrigatorio cpf" />
                        </li>
                        <li>
                            <label>Data de nascimento: *</label><input name="nascimento" id="nascimento" type="text" value="<?php echo Cliente::Nascimento(); ?>" class="obrigatorio data" />
                        </li>
                        <li>
                            <label>Telefone fixo: *</label><input name="telefone_fixo" id="telefone_fixo" type="text" value="<?php echo Cliente::TelefoneFixo(); ?>" class="obrigatorio telefone" />
                        </li>
                        <li>
                            <label>Telefone comercial:</label><input name="telefone_comercial" id="telefone_comercial" type="text" value="<?php echo Cliente::TelefoneComercial(); ?>" class="telefone" />
                        </li>
                        <li>
                            <label>Celular:</label><input name="telefone_celular" id="telefone_celular" type="text" value="<?php echo Cliente::TelefoneCelular(); ?>" class="telefone" />
                        </li>
                    </ul>
                </fieldset>
                <button type="submit" name="submit" value="atualiza">Salvar</button>
            </form>
            <form action="endereco.php" method="POST">
                <fieldset>
                    <legend>Endereços de Entrega</legend>
                    <ul>
                        <?php
                        $enderecos = Cliente::EnderecosEntrega();
                        if (is_array($enderecos)) {
                            foreach ($enderecos as $endereco) {
                                echo "<li><a href='endereco.php?id={$endereco["id"]}'>{$endereco["nome"]}</a></li>";
                            }
                        }
                        ?>
                        <li>
                            <p><b>Novo endereço:</b></p>
                            <label>CEP: *</label><input name="cep" id="cep" type="text" class="obrigatorio cep" /><button type="submit" name="submit" value="endereco">Inserir um novo endereço...</button>
                        </li>
                    </ul>
                </fieldset>
            </form>
            <fieldset>
                <legend>Pedidos</legend>
                <ul>
                    <?php
                        $pedidos = Cliente::Pedidos();
                        if (is_array($pedidos)) {
                            foreach ($pedidos as $key => $pedido) {
                                echo "<li><a href='pedido.php?id={$key}'>" . date("d/m/Y H:i", strtotime($pedido["insert"])) . " (" . strtoupper($pedido["referencia"]) . ")</a></li>";
                            }
                        }
                    ?>
                    </ul>
                </fieldset>
            </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>