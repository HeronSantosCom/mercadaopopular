﻿<?php
include_once "../core/core.php";
Cliente::Bloqueio();
Cliente::Controle();
$endereco["nome"] = null;
$endereco["logradouro"] = null;
$endereco["numero"] = null;
$endereco["complemento"] = null;
$endereco["bairro"] = null;
$endereco["cidade"] = null;
$endereco["uf"] = null;
$endereco["cep"] = null;
if (Core::Get("id")) {
    $enderecos = Cliente::EnderecosEntrega();
    if (!isset($enderecos[Core::Get("id")])) {
        Core::Alert("Endereço não cadastrado!");
        header("Location: cliente.php");
        exit();
    }
    $endereco = $enderecos[Core::Get("id")];
} else {
    if (Core::Post("cep")) {
        $enderecos = Core::GetCep(Core::Post("cep"));
        if (!$enderecos) {
            Core::Alert("CEP inexistente!");
            header("Location: cliente.php");
            exit();
        }
    }
    $endereco["logradouro"] = $enderecos["logradouro"];
    $endereco["bairro"] = $enderecos["bairro"];
    $endereco["cidade"] = $enderecos["cidade"];
    $endereco["uf"] = $enderecos["uf"];
    $endereco["cep"] = $enderecos["cep"];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <form action="" method="POST">
                <p>Os campos com * são obrigatórios.</p>
                <fieldset>
                    <legend>Seus dados pessoais</legend>
                    <ul>
                        <li>
                            <label>Nome: *</label><input name="nome" id="nome" type="text" value="<?php echo $endereco["nome"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Logradouro: *</label><input name="logradouro" id="logradouro" type="text" value="<?php echo $endereco["logradouro"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Número: *</label><input name="numero" id="numero" type="text" value="<?php echo $endereco["numero"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Complemento:</label><input name="complemento" id="complemento" type="text" value="<?php echo $endereco["complemento"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Bairro: *</label><input name="bairro" id="bairro" type="text" value="<?php echo $endereco["bairro"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Cidade: *</label><input name="cidade" id="cidade" type="text" value="<?php echo $endereco["cidade"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>Estado (UF): *</label><input name="uf" id="uf" type="text" value="<?php echo $endereco["uf"]; ?>" class="obrigatorio" />
                        </li>
                        <li>
                            <label>CEP: *</label><input name="cep" id="cep" type="text" value="<?php echo $endereco["cep"]; ?>" readonly class="obrigatorio" />
                        </li>
                    </ul>
                </fieldset>
                <button type="submit" name="submit" value="novo-endereco">Salvar</button>
                <?php if (Core::Get("id")) {
 ?>
                    <button type="submit" name="submit" value="remove-endereco">Remover</button>
<?php } ?>
            </form>
        </div>
<?php include "includes/html_body_footer.php"; ?>
    </body>
</html>