﻿<div>
    <h1><?php echo Loja::Titulo(); ?></h1>
    <?php
    echo "<p><a href='index.php'>Início</a></p>";
    if (Cliente::Id()) {
        echo "<p>Olá " . Cliente::Nome() . "! Não é você? <a href='logoff.php'>Sair</a>.</p>";
        echo "<p><a href='cliente.php'>Meu " . Loja::Titulo() . "</a></p>";
    } else {
        echo "<p>Seja bem-vindo!</p>";
        echo "<p><a href='login.php'>Faça seu login ou cadastre-se</a></p>";
    }
        $carrinho = Carrinho::Itens();
    if ($carrinho) {
        echo "<p><a href='carrinho.php'>" . count($carrinho) . " produto selecionado</a></p>";
    } else {
        echo "<p>Seu carrinho está vazio!</p>";
    }
    if (Core::IsPage("index\.php")) {
        if (Departamento::Id()) {
            echo "<h3>" . Departamento::Nome() . "</h3>";
        }
    }
    ?>
</div>