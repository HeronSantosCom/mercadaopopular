﻿<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="cs" />
	<meta name="robots" content="all,follow" />
	<meta name="author" content="Softpixel" />
	<meta name="copyright" content="www.softpixel.com.br" />
	<title><?php echo Loja::Titulo(); ?></title>
	<meta name="description" content="<?php echo Loja::description; ?>" />
	<meta name="keywords" content="<?php echo Loja::keywords; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo Theme::Style(); ?>" />
	<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>-->
	<script type="text/javascript" src="<?php echo Theme::Script(); ?>"></script>
	<?php Core::Alert(); ?>
</head>