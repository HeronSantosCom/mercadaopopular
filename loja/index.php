﻿<?php
include_once "../core/core.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <?php
            $departamentos = Departamento::Sub();
            if (is_array($departamentos)) {
                echo "<ul>";
                foreach ($departamentos as $departamento) {
                    echo "<li><a href='departamento.php?id={$departamento["id"]}'>{$departamento["nome"]}</a></li>";
                }
                echo "</ul>";
            }
            ?>
        </div>
        <div id="produtos">
            <?php
            list($produtos, $paginacao) = Produto::Catalogo();
            if (is_array($produtos)) {
                echo "<ul>";
                foreach ($produtos as $produto) {
                    $thumbnails = Produto::Fotos($produto["id"]);
                    echo "<li>";
                    echo "<a href='produto.php?id={$produto["id"]}'>";
                    echo "<ul>";
                    echo "<li>";
                    echo ($thumbnails ? "<img src='{$thumbnails[0]['thumbnail']}' alt='{$produto["nome"]}' title='{$produto["nome"]}' />" : "<img src='themes/" . Loja::Tema() . "/images/nothumb.png' alt='{$produto["nome"]}' title='{$produto["nome"]}' />");
                    echo "</li>";
                    echo "<li>";
                    echo $produto["nome"];
                    echo "</li>";
                    $quantidade = (int) $produto["quantidade"] - $produto["quantidade_min"];
                    $preco = (float) $produto["valor"];
                    $desconto = (float) $produto["desconto"];
                    if ($desconto) {
                        $valor = $preco - $desconto;
                        echo "<li>De: R$ {$preco}</li>";
                        echo "<li>Por: R$ {$valor}</li>";
                    } else {
                        echo "<li>R$ {$preco}</li>";
                    }
                    if ($quantidade == 0) {
                        echo "<li>Produto indisponível</li>";
                    }
                    echo "</ul>";
                    echo "</a>";
                    echo "</li>";
                }
                echo "</ul>";
            }
            ?>
        </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>