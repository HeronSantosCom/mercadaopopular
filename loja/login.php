﻿<?php
include_once "../core/core.php";
Cliente::Login();
Cliente::Cadastro();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <div>
                <p>JÁ SOU CLIENTE DO <?php echo strtoupper(Loja::Titulo()); ?></p>
                <form action="" method="POST">
                    <fieldset>
                        <legend>Efetuar login</legend>
                        <ul>
                            <li>
                                <label>E-mail: </label><input name="email" id="email" type="text" />
                            </li>
                            <li>
                                <label>Senha: </label><input name="senha" id="senha" type="password" /><br />
                                <a href="senha.php">Esqueci minha senha</a>
                            </li>
                        </ul>
                    </fieldset>
                    <button type="submit" name="submit" value="login">Continuar</button>
                </form>
            </div>
            <div>
                <p>QUERO SER CLIENTE DO <?php echo strtoupper(Loja::Titulo()); ?></p>
                <form action="" method="POST">
                    <p>Por favor, preencha os dados para criar sua conta no <?php echo Loja::Titulo(); ?>. Os campos com * são obrigatórios.</p>
                    <fieldset>
                        <legend>Seus dados pessoais</legend>
                        <ul>
                            <li>
                                <label>Nome: *</label><input name="nome" id="nome" type="text" class="obrigatorio" />
                            </li>
                            <li>
                                <label>CPF: *</label><input name="documento" id="documento" type="text" class="obrigatorio cpf" />
                            </li>
                            <li>
                                <label>Data de nascimento: *</label><input name="nascimento" id="nascimento" type="text" class="obrigatorio data" />
                            </li>
                            <li>
                                <label>CEP: *</label><input name="cep" id="cep" type="text" class="obrigatorio cep" />
                            </li>
                            <li>
                                <label>Telefone fixo: *</label><input name="telefone_fixo" id="telefone_fixo" type="text" class="obrigatorio telefone" />
                            </li>
                            <li>
                                <label>Telefone comercial:</label><input name="telefone_comercial" id="telefone_comercial" type="text" class="telefone" />
                            </li>
                            <li>
                                <label>Celular:</label><input name="telefone_celular" id="telefone_celular" type="text" class="telefone" />
                            </li>
                        </ul>
                    </fieldset>
                    <fieldset>
                        <legend>Dados de acesso</legend>
                        <ul>
                            <li>
                                <label>E-mail: *</label><input name="email" id="email" type="text" class="obrigatorio" />
                            </li>
                            <li>
                                <label>Confirme o e-mail: *</label><input name="confirmaemail" id="confirmaemail" type="text" class="obrigatorio" />
                            </li>
                            <li>
                                <label>Senha: *</label><input name="senha" id="senha" type="password" class="obrigatorio" />
                            </li>
                            <li>
                                <label>Confirme a senha: *</label><input name="confirmasenha" id="confirmasenha" type="password" class="obrigatorio" />
                            </li>
                        </ul>
                    </fieldset>
                    <button type="submit" name="submit" value="cadastro">Continuar</button>
                </form>
            </div>
        </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>