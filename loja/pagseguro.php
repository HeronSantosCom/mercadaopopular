<?php

include_once "../core/core.php";
$MYSQL = Core::MySQL();
$VendedorEmail = Core::Post("VendedorEmail");
$TransacaoID = Core::Post("TransacaoID");
$Referencia = Core::Post("Referencia");
if ($VendedorEmail and $TransacaoID and $Referencia) {
    ob_start();
    print str_pad(" PROCESSO ", 80, "=", STR_PAD_BOTH) . "\r\n";
    $npi = new PagSeguroNpi();
    $result = $npi->notificationPost(PagSeguro::Token());
    if ($result == "VERIFICADO") {
        $DataTransacao = date("Y-m-d H:i:s");
        if (strlen(Core::Post("DataTransacao")) > 0) {
            $DataTransacao = explode(" ", Core::Post("DataTransacao"));
            $DiaTransacao = Core::toData($DataTransacao[0], "DD/MM/YYYY", "YYYY-MM-DD");
            $HoraTransacao = date("H:i:s");
            if (isset($DataTransacao[1])) {
                $HoraTransacao = $DataTransacao[1];
            }
        }
        $pagamento['TABLE'] = 'pagseguro';
        $pagamento['FIELDS'][] = array(
            'VendedorEmail' => $VendedorEmail,
            'TransacaoID' => $TransacaoID,
            'Referencia' => $Referencia,
            'CliNome' => Core::Post("CliNome"),
            'CliEmail' => Core::Post("CliEmail"),
            'CliEndereco' => Core::Post("CliEndereco"),
            'CliNumero' => Core::Post("CliNumero"),
            'CliComplemento' => Core::Post("CliComplemento"),
            'CliBairro' => Core::Post("CliBairro"),
            'CliCidade' => Core::Post("CliCidade"),
            'CliEstado' => Core::Post("CliEstado"),
            'CliCEP' => Core::Post("CliCEP"),
            'CliTelefone' => Core::Post("CliTelefone"),
            'Anotacao' => Core::Post("Anotacao"),
            'DataTransacao' => "{$DiaTransacao} {$HoraTransacao}",
            'TipoPagamento' => Core::Post("TipoPagamento"),
            'StatusTransacao' => Core::Post("StatusTransacao"),
            'Parcelas' => Core::Post("Parcelas"),
        );
        $save = $MYSQL->save($pagamento);
        if (is_array($save)) {
            var_dump($save);
        }
    }
    print str_pad(" POST ", 80, "=", STR_PAD_BOTH) . "\r\n";
    var_dump($_POST);
    print str_pad(" GET ", 80, "=", STR_PAD_BOTH) . "\r\n";
    var_dump($_GET);
    print str_pad(" SESSION ", 80, "=", STR_PAD_BOTH) . "\r\n";
    var_dump($_SESSION);
    $pagseguro = ob_get_clean();
    Core::Log($pagseguro, "pagseguro.log");
    exit();
} else {
    ob_start();
    print str_pad(" RETORNO ", 80, "=", STR_PAD_BOTH) . "\r\n";
    print str_pad(" POST ", 80, "=", STR_PAD_BOTH) . "\r\n";
    var_dump($_POST);
    print str_pad(" GET ", 80, "=", STR_PAD_BOTH) . "\r\n";
    var_dump($_GET);
    print str_pad(" SESSION ", 80, "=", STR_PAD_BOTH) . "\r\n";
    var_dump($_SESSION);
    Core::Log(ob_get_clean(), "pagseguro.log");
    $pedido = Core::Session("ultimoPedido");
    if ($pedido) {
        $Count = Core::Get("c") + 1;
        if ($Count <= 3) {
            $pagseguro = $MYSQL->find(array('FROM' => 'pagseguro', 'WHERE' => 'Referencia = "' . $pedido["referencia"] . '"'));
            if (isset($pagseguro[0]['id'])) {
                Core::UnSession("ultimoPedido");
                header("Location: pedido.php?id={$pedido["id"]}");
                exit();
            }
            sleep(3);
            header("Location: pagseguro.php?c={$Count}");
            exit();
        }
    }
    header("Location: cliente.php");
    exit();
}
?>