﻿<?php
include_once "../core/core.php";
Cliente::Bloqueio();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <?php
            $pedido = Pedido::Get();
            if (is_array($pedido)) {
                $itens = Pedido::Itens();
                $transacoes = Pedido::Transacoes();
            ?>
                <form action="" method="POST">
                    <fieldset>
                        <legend>Produtos selecionados</legend>
                    <?php
                    $total = 0;
                    $desconto = 0;
                    $peso = 0;
                    $frete = 0;
                    echo "<table>";
                    echo "<tr>";
                    echo "<td>Nome</td>";
                    echo "<td width='50px' align='center'>Qtd.</td>";
                    echo "<td width='100px' align='right'>Preço</td>";
                    echo "<td width='100px' align='right'>Subtotal</td>";
                    echo "</tr>";
                    foreach ($itens as $produto) {
                        $valor = (float) $produto["valor"];
                        $subtotal = $valor * $produto["selecionado"];
                        $subtotal_desconto = ($valor - (float) $produto["desconto"]) * $produto["selecionado"];
                        $desconto += (float) $produto["desconto"];
                        $total += $subtotal_desconto;
                        if ($produto["peso"] == 0) {
                            $produto["nome"] .= " <b>(frete grátis)</b>";
                        } else {
                            $peso += $produto["peso"];
                        }
                        echo "<tr>";
                        echo "<td>{$produto["nome"]}</td>";
                        echo "<td align='center'>{$produto["selecionado"]}</td>";
                        echo "<td align='right'>R$ " . number_format($valor, 2, ',', '') . "</td>";
                        echo "<td align='right'>R$ " . number_format($subtotal, 2, ',', '') . "</td>";
                        echo "</tr>";
                    }
                    if ($pedido['frete_valor']) {
                        echo "<tr>";
                        echo "<td colspan='3'>Frete - {$pedido['entrega_cep']} ({$pedido['frete_local']})</td>";
                        echo "<td align='right'>R$ " . number_format($pedido['frete_valor'], 2, ',', '') . "</td>";
                        echo "</tr>";
                        $total += $pedido['frete_valor'];
                    }
                    if ($desconto) {
                        echo "<tr>";
                        echo "<td colspan='3'>Desconto</td>";
                        echo "<td align='right'>- R$ " . number_format($desconto, 2, ',', '') . "</td>";
                        echo "</tr>";
                    }
                    echo "<tr>";
                    echo "<td colspan='3'>Total</td>";
                    echo "<td align='right'>R$ " . number_format($total, 2, ',', '') . "</td>";
                    echo "</tr>";
                    echo "</table>";
                    ?>
                </fieldset>
                <fieldset>
                    <legend>Endereços</legend>
                    <ul>
                        <li>
                            Entrega: <?php echo $pedido['entrega_nome']; ?>
                            <ul>
                                <li><b>Logradouro:</b> <?php echo $pedido['entrega_logradouro']; ?></li>
                                <li><b>Número:</b> <?php echo $pedido['entrega_numero']; ?></li>
                                <li><b>Complemento:</b> <?php echo $pedido['entrega_complemento']; ?></li>
                                <li><b>Bairro:</b> <?php echo $pedido['entrega_bairro']; ?></li>
                                <li><b>Cidade:</b> <?php echo $pedido['entrega_cidade']; ?></li>
                                <li><b>UF:</b> <?php echo $pedido['entrega_uf']; ?></li>
                                <li><b>CEP:</b> <?php echo $pedido['entrega_cep']; ?></li>
                            </ul>
                        </li>
                    </ul>
                </fieldset>
            </form>
            <?php
                    if (is_array($transacoes)) {
            ?>
                        <fieldset>
                            <legend>Estado do pedido</legend>
                            <ul>
                                <li>Forma de pagamento:  <?php echo utf8_encode($transacoes[0]['TipoPagamento']); ?></li>
                                <li>Parcelas:  <?php echo $transacoes[0]['Parcelas']; ?>x</li>
                                <li>Atualizações: <br />
                        <?php
                        foreach ($transacoes as $transacao) {
                            $DataTransacao = explode(" ", $transacao["DataTransacao"]);
                            $DiaTransacao = Core::toData($DataTransacao[0], "YYYY-MM-DD", "DD/MM/YYYY");
                            $HoraTransacao = $DataTransacao[1];
                            echo "<ul>";
                            echo "<li>{$DiaTransacao} às {$HoraTransacao}</li>";
                            echo "<li>" . utf8_encode($transacao['StatusTransacao']) . "</li>";
                            if (strlen($transacao['Anotacao']) > 0) {
                                echo "<li>Observação: " . utf8_encode($transacao['Anotacao']) . "</li>";
                            }
                            echo "</ul>";
                        }
                        ?>
                    </li>
                </ul>
            </fieldset>
            <?php
                    } else {
                        $telefone = str_replace(array("-", "(", ")", "'", '"', "/", "\\", "_", "."), '', Cliente::TelefoneFixo());
                        $ddd = substr($telefone, 0, 2);
                        $telefone = substr($telefone, 2);
                        print "<form method='post' action='https://pagseguro.uol.com.br/checkout/checkout.jhtml'>";
                        print "<fieldset>";
                        print "<legend>Pagamento</legend>";
                        print "<input type='hidden' name='email_cobranca' value='" . PagSeguro::Id() . "' />";
                        print "<input type='hidden' name='tipo' value='CP' />";
                        print "<input type='hidden' name='moeda' value='BRL' />";
                        $key = 1;
                        foreach ($itens as $item) {
                            $item_peso = '0';
                            $item_frete = '0';
                            if ($pedido['frete_valor']) {
                                if ($item["peso"] > 0) {
                                    $porcentagem = ((($item["peso"] / $item["selecionado"]) * 100) / $peso);
                                    if ($porcentagem > 0) {
                                        $item_peso = ceil($item["peso"] / $item["selecionado"]);
                                        $item_frete = $pedido['frete_valor'] * ($porcentagem / 100);
                                    }
                                }
                            }
                            print "<input type='hidden' name='item_id_{$key}' value='{$item["id"]}' />";
                            print "<input type='hidden' name='item_descr_{$key}' value='{$item["nome"]}' />";
                            print "<input type='hidden' name='item_quant_{$key}' value='{$item["selecionado"]}' />";
                            print "<input type='hidden' name='item_valor_{$key}' value='" . number_format(((float) $item["valor"] - (float) $item["desconto"]), 2, ',', '') . "' />";
                            print "<input type='hidden' name='item_peso_{$key}' value='{$item_peso}' />";
                            print "<input type='hidden' name='item_frete_{$key}' value='" . number_format(((float) $item_frete), 2, ',', '') . "' />";
                            $key++;
                        }
                        print "<input type='hidden' name='cliente_nome' value='" . Cliente::Nome() . "' />";
                        print "<input type='hidden' name='cliente_email' value='" . Cliente::Email() . "' />";
                        print "<input type='hidden' name='cliente_end' value='{$pedido['entrega_logradouro']}' />";
                        print "<input type='hidden' name='cliente_num' value='{$pedido['entrega_numero']}' />";
                        print "<input type='hidden' name='cliente_compl' value='{$pedido['entrega_complemento']}' />";
                        print "<input type='hidden' name='cliente_bairro' value='{$pedido['entrega_bairro']}' />";
                        print "<input type='hidden' name='cliente_cidade' value='{$pedido['entrega_cidade']}' />";
                        print "<input type='hidden' name='cliente_uf' value='{$pedido['entrega_uf']}' />";
                        print "<input type='hidden' name='cliente_cep' value='{$pedido['entrega_cep']}' />";
                        print "<input type='hidden' name='cliente_ddd' value='{$ddd}' />";
                        print "<input type='hidden' name='cliente_tel' value='{$telefone}' />";
                        print "<input type='hidden' name='tipo_frete' value='{$pedido['frete_tipo']}' />";
                        print "<input type='hidden' name='ref_transacao' value='{$pedido['referencia']}' />";
                        print "<ul>";
                        print "<li>";
                        print "<input name='submit' type='submit' id='buy_pagseguro' value='pagar via PagSeguro' />";
                        print "</li>";
                        print "</ul>";
                        print "</fieldset>";
                        print "</form>";
                    }
                }
            ?>
            </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>