﻿<?php
include_once "../core/core.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <h3><?php echo Produto::Nome(); ?></h3>
            <h4><a href="departamento.php?id=<?php echo Produto::idDepartamento(); ?>"><?php echo Produto::Departamento(); ?></a></h4>
            <div id="fotos">
                <?php
                $fotos = Produto::Fotos();
                if ($fotos) {
                    echo "<ul>";
                    foreach ($fotos as $foto) {
                        if ($foto["thumbnail"]) {
                            echo "<li><a href='{$foto["arquivo"]}'><img src='{$foto["thumbnail"]}' alt='clique para ampliar' title='clique para ampliar' /></a></li>";
                        }
                    }
                    echo "</ul>";
                }
                ?>
            </div>
            <form action="carrinho.php" method="POST">
                <div id="comprar">
                    <ul>
                        <?php
                        $id = Produto::Id();
                        $quantidade = Produto::Quantidade();
                        $preco = Produto::Preco();
                        $desconto = Produto::Desconto();
                        if ($desconto) {
                            $valor = $preco - $desconto;
                            echo "<li>De: R$ {$preco}</li>";
                            echo "<li>Por: R$ {$valor}</li>";
                            echo "<li>Economia de: R$ {$desconto}</li>";
                        } else {
                            echo "<li>Preço: R$ {$preco}</li>";
                        }
                        if ($quantidade > 0) {
                            echo "<li><button type='submit' name='adicionar' value='{$id}'>Comprar</button></li>";
                        } else {
                            echo "<li>Produto indisponível</li>";
                            echo "<li><label>Nome: *</label><input name='nome' id='nome' type='text' class='obrigatorio' /></li>";
                            echo "<li><label>Email: *</label><input name='email' id='email' type='text' class='obrigatorio' /></li>";
                            echo "<li><button type='submit' name='avisar' value='{$id}'>avisar quando estiver disponível</button></li>";
                        }
                        ?>
                    </ul>
                </div>
            </form>
            <div id="descricao">
                <?php echo Produto::Descricao() ?>
                    </div>
                    <div id="especificacao">
                <?php echo Produto::Especificacao() ?>
                    </div>
                    <div id="garantia">
                <?php echo Produto::Garantia() ?>
                    </div>
                </div>
        <?php include "includes/html_body_footer.php"; ?>
    </body>
</html>