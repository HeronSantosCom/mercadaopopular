﻿<?php
include_once "../core/core.php";
Cliente::RenovaSenha();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <?php include "includes/html_head.php"; ?>
    <body>
        <?php include "includes/html_body_header.php"; ?>
        <div id="body">
            <?php if (!Core::Get("token")) {
            ?>
                <p>Por favor, preencha os campos abaixo e enviaremos a senha para seu e-mail (todos os campos são obrigatórios):</p>
                <form action="" method="POST">
                    <fieldset>
                        <legend>Solicitar senhade identificação</legend>
                        <ul>
                            <li>
                                <label>E-mail:</label><input name="email" id="email" type="text" />
                            </li>
                            <li>
                                <label>Confirme o e-mail:</label><input name="confirmaemail" id="confirmaemail" type="text" />
                            </li>
                        </ul>
                    </fieldset>
                    <button type="submit">Continuar</button>
                </form>
            <?php } else {
 ?>
                <p>Por favor, preencha os campos abaixo para alterar sua senha de acesso ao <?php echo Loja::Titulo(); ?> (todos os campos são obrigatórios):</p>
                <form action="" method="POST">
                    <fieldset>
                        <legend>Criar nova senha</legend>
                        <ul>
                            <li>
                                <label>Digite a nova senha:</label><input name="senha" id="senha" type="password" />
                            </li>
                            <li>
                                <label>Confirme a nova senha:</label><input name="confirmasenha" id="confirmasenha" type="password" />
                            </li>
                        </ul>
                    </fieldset>
                    <button type="submit">Continuar</button>
                </form>
<?php } ?>
        </div>
<?php include "includes/html_body_footer.php"; ?>
    </body>
</html>