/*
 Navicat MySQL Data Transfer

 Source Server         : mysql.mercadaopopular.com.br
 Source Server Version : 50153
 Source Host           : mysql.mercadaopopular.com.br
 Source Database       : mercadaopopular

 Target Server Version : 50153
 File Encoding         : utf-8

 Date: 07/05/2011 22:29:51 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `_catalog_categories`
-- ----------------------------
DROP TABLE IF EXISTS `_catalog_categories`;
CREATE TABLE `_catalog_categories` (
  `idcatalog_categories` int(11) NOT NULL AUTO_INCREMENT,
  `idlogin` int(11) DEFAULT NULL,
  `idcatalog_categories_main` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idcatalog_categories`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `_catalog_manufacturers`
-- ----------------------------
DROP TABLE IF EXISTS `_catalog_manufacturers`;
CREATE TABLE `_catalog_manufacturers` (
  `idcatalog_manufacturers` int(11) NOT NULL AUTO_INCREMENT,
  `idlogin` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idcatalog_manufacturers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `_catalog_products`
-- ----------------------------
DROP TABLE IF EXISTS `_catalog_products`;
CREATE TABLE `_catalog_products` (
  `idcatalog_products` int(11) NOT NULL AUTO_INCREMENT,
  `idlogin` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `idcatalog_categories` int(11) DEFAULT NULL,
  `idcatalog_manufacturers` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `last_value` varchar(50) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `guarantee` varchar(250) DEFAULT NULL,
  `description` text,
  `variables` text,
  PRIMARY KEY (`idcatalog_products`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `_catalog_products_photos`
-- ----------------------------
DROP TABLE IF EXISTS `_catalog_products_photos`;
CREATE TABLE `_catalog_products_photos` (
  `idcatalog_products_photos` int(11) NOT NULL AUTO_INCREMENT,
  `idcatalog_products` int(11) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idcatalog_products_photos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `_groups`
-- ----------------------------
DROP TABLE IF EXISTS `_groups`;
CREATE TABLE `_groups` (
  `idgroups` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idgroups`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `_groups`
-- ----------------------------
BEGIN;
INSERT INTO `_groups` VALUES ('1', 'admin');
COMMIT;

-- ----------------------------
--  Table structure for `_itens_pedidos`
-- ----------------------------
DROP TABLE IF EXISTS `_itens_pedidos`;
CREATE TABLE `_itens_pedidos` (
  `iditens_pedidos` int(11) NOT NULL AUTO_INCREMENT,
  `idpedidos` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `produto_nome` varchar(250) DEFAULT NULL,
  `produto_valor` varchar(250) DEFAULT NULL,
  `produto_qnt` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`iditens_pedidos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `_login`
-- ----------------------------
DROP TABLE IF EXISTS `_login`;
CREATE TABLE `_login` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) DEFAULT NULL,
  `senha` varchar(40) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `documento` varchar(50) DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `uf` varchar(5) DEFAULT NULL,
  `loja` varchar(5) DEFAULT NULL,
  `allowedmodules` text,
  `idgroups` int(11) DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `_login`
-- ----------------------------
BEGIN;

COMMIT;

-- ----------------------------
--  Table structure for `_pedidos_status`
-- ----------------------------
DROP TABLE IF EXISTS `_pedidos_status`;
CREATE TABLE `_pedidos_status` (
  `idpedidos_status` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idpedidos_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `avisos`
-- ----------------------------
DROP TABLE IF EXISTS `avisos`;
CREATE TABLE `avisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idprodutos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `avisos_fk_produtos` (`idprodutos`),
  CONSTRAINT `avisos_fk_produtos` FOREIGN KEY (`idprodutos`) REFERENCES `produtos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `clientes`
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `documento` varchar(20) NOT NULL,
  `nascimento` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `telefone_fixo` varchar(12) NOT NULL,
  `telefone_comercial` varchar(12) DEFAULT NULL,
  `telefone_celular` varchar(12) DEFAULT NULL,
  `idlojas` int(11) NOT NULL,
  `insert` datetime NOT NULL,
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idlojas` (`idlojas`),
  CONSTRAINT `clientes_fk_lojas` FOREIGN KEY (`idlojas`) REFERENCES `lojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `clientes_enderecos`
-- ----------------------------
DROP TABLE IF EXISTS `clientes_enderecos`;
CREATE TABLE `clientes_enderecos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `logradouro` varchar(255) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` char(2) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `idclientes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idclientes` (`idclientes`),
  CONSTRAINT `clientes_enderecos_fk_clientes` FOREIGN KEY (`idclientes`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `conteudos`
-- ----------------------------
DROP TABLE IF EXISTS `conteudos`;
CREATE TABLE `conteudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `conteudo` mediumtext NOT NULL,
  `idlojas` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idlojas` (`idlojas`),
  CONSTRAINT `conteudos_fk_lojas` FOREIGN KEY (`idlojas`) REFERENCES `lojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `creditos`
-- ----------------------------
DROP TABLE IF EXISTS `creditos`;
CREATE TABLE `creditos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` double(10,5) NOT NULL,
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(50) NOT NULL,
  `idclientes` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `creditos_fk_clientes` (`idclientes`),
  CONSTRAINT `creditos_fk_clientes` FOREIGN KEY (`idclientes`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `departamentos`
-- ----------------------------
DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `iddepartamentos` int(11) DEFAULT NULL,
  `idlojas` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idlojas` (`idlojas`),
  KEY `iddepartamentos` (`iddepartamentos`),
  CONSTRAINT `departamentos_fk_departamentos` FOREIGN KEY (`iddepartamentos`) REFERENCES `departamentos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `departamentos_fk_lojas` FOREIGN KEY (`idlojas`) REFERENCES `lojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `especificacoes`
-- ----------------------------
DROP TABLE IF EXISTS `especificacoes`;
CREATE TABLE `especificacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `array` mediumtext,
  `padrao` varchar(255) DEFAULT NULL,
  `iddepartamentos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `especificacoes_fk_departamentos` (`iddepartamentos`),
  CONSTRAINT `especificacoes_fk_departamentos` FOREIGN KEY (`iddepartamentos`) REFERENCES `departamentos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `globals`
-- ----------------------------
DROP TABLE IF EXISTS `globals`;
CREATE TABLE `globals` (
  `var` varchar(20) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `globals`
-- ----------------------------
BEGIN;
INSERT INTO `globals` VALUES ('SYSTEMNAME', 'AdministraÃ§Ã£o'), ('MAX_LINES', '5'), ('BIG_WIDTH_SIZE', '800'), ('BIG_HEIGHT_SIZE', '600'), ('SMALL_WIDTH_SIZE', '120'), ('SMALL_HEIGHT_SIZE', '100');
COMMIT;

-- ----------------------------
--  Table structure for `lojas`
-- ----------------------------
DROP TABLE IF EXISTS `lojas`;
CREATE TABLE `lojas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `documento` varchar(20) NOT NULL,
  `nascimento` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `endereco_logradouro` varchar(255) NOT NULL,
  `endereco_numero` int(11) NOT NULL,
  `endereco_complemento` varchar(255) DEFAULT NULL,
  `endereco_bairro` varchar(255) NOT NULL,
  `endereco_cidade` varchar(255) NOT NULL,
  `endereco_uf` char(2) NOT NULL,
  `endereco_cep` char(10) NOT NULL,
  `telefone_fixo` varchar(12) NOT NULL,
  `telefone_comercial` varchar(12) DEFAULT NULL,
  `telefone_celular` varchar(12) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `tema` varchar(255) NOT NULL DEFAULT 'default',
  `pagseguro` varchar(255) NOT NULL,
  `pagseguro_token` varchar(255) NOT NULL,
  `insert` datetime NOT NULL,
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `lojas`
-- ----------------------------
BEGIN;
INSERT INTO `lojas` VALUES ('2', 'Felipe Silva', '', '0000-00-00', 'felipe@softpixel.com.br', '123', 'R Barcelos Domingos', '89', null, 'Campo Grande', 'Rio de Janeiro', 'RJ', '23080-020', '21-30225560', null, '2178777601', 'Nintendo World', 'http://nintendoworld.mercadaopopular.com.br', 'default', '', '', '2011-03-12 12:49:49', '2011-03-24 14:35:50', '1');
COMMIT;

-- ----------------------------
--  Table structure for `pagseguro`
-- ----------------------------
DROP TABLE IF EXISTS `pagseguro`;
CREATE TABLE `pagseguro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `VendedorEmail` varchar(255) NOT NULL,
  `TransacaoID` varchar(32) NOT NULL,
  `Referencia` varchar(255) NOT NULL,
  `CliNome` varchar(100) DEFAULT NULL,
  `CliEmail` varchar(255) DEFAULT NULL,
  `CliEndereco` varchar(200) DEFAULT NULL,
  `CliNumero` varchar(10) DEFAULT NULL,
  `CliComplemento` varchar(100) DEFAULT NULL,
  `CliBairro` varchar(100) DEFAULT NULL,
  `CliCidade` varchar(100) DEFAULT NULL,
  `CliEstado` varchar(2) DEFAULT NULL,
  `CliCEP` varchar(10) DEFAULT NULL,
  `CliTelefone` varchar(16) DEFAULT NULL,
  `DataTransacao` datetime DEFAULT NULL,
  `StatusTransacao` varchar(30) NOT NULL,
  `Anotacao` varchar(250) DEFAULT NULL,
  `TipoPagamento` varchar(30) NOT NULL,
  `Parcelas` int(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pagseguro`
-- ----------------------------
BEGIN;

COMMIT;

-- ----------------------------
--  Table structure for `pedidos`
-- ----------------------------
DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagamento` varchar(45) NOT NULL DEFAULT 'pagseguro',
  `referencia` varchar(255) NOT NULL,
  `frete_tipo` char(2) DEFAULT NULL,
  `frete_local` varchar(255) DEFAULT NULL,
  `frete_valor` double(10,5) DEFAULT NULL,
  `entrega_nome` varchar(255) DEFAULT NULL,
  `entrega_logradouro` varchar(255) DEFAULT NULL,
  `entrega_numero` int(11) DEFAULT NULL,
  `entrega_complemento` varchar(255) DEFAULT NULL,
  `entrega_bairro` varchar(255) DEFAULT NULL,
  `entrega_cidade` varchar(255) DEFAULT NULL,
  `entrega_uf` char(2) DEFAULT NULL,
  `entrega_cep` varchar(10) DEFAULT NULL,
  `idclientes` int(11) NOT NULL,
  `insert` datetime NOT NULL,
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `pedidos_fk_clientes` (`idclientes`),
  CONSTRAINT `pedidos_fk_clientes` FOREIGN KEY (`idclientes`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pedidos_produtos`
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_produtos`;
CREATE TABLE `pedidos_produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `peso` int(11) DEFAULT NULL,
  `valor` double(10,5) NOT NULL,
  `desconto` double(10,5) DEFAULT NULL,
  `selecionado` int(11) NOT NULL DEFAULT '1',
  `idprodutos` int(11) NOT NULL,
  `idpedidos` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idprodutos` (`idprodutos`),
  KEY `idpedidos` (`idpedidos`),
  KEY `pedidos_produtos_fk_pedidos` (`idpedidos`),
  CONSTRAINT `pedidos_produtos_fk_pedidos` FOREIGN KEY (`idpedidos`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pedidos_produtos_fk_produtos` FOREIGN KEY (`idprodutos`) REFERENCES `produtos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `produtos`
-- ----------------------------
DROP TABLE IF EXISTS `produtos`;
CREATE TABLE `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `peso` int(11) NOT NULL,
  `valor` double(10,5) NOT NULL,
  `desconto` double(10,5) DEFAULT '0.00000',
  `frete` double(10,5) DEFAULT '0.00000',
  `descricao` mediumtext NOT NULL,
  `especificacao` mediumtext NOT NULL,
  `garantia` varchar(250) DEFAULT NULL,
  `quantidade` int(11) NOT NULL DEFAULT '0',
  `quantidade_min` int(11) NOT NULL DEFAULT '1',
  `quantidade_max` int(11) NOT NULL DEFAULT '10',
  `iddepartamentos` int(11) NOT NULL,
  `insert` datetime NOT NULL,
  `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `iddepartamentos` (`iddepartamentos`),
  CONSTRAINT `produtos_fk_departamentos` FOREIGN KEY (`iddepartamentos`) REFERENCES `departamentos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `produtos_especificacoes`
-- ----------------------------
DROP TABLE IF EXISTS `produtos_especificacoes`;
CREATE TABLE `produtos_especificacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` varchar(255) DEFAULT NULL,
  `idprodutos` int(11) DEFAULT NULL,
  `idespecificacoes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produtos_especificacoes_fk_produtos` (`idprodutos`),
  KEY `produtos_especificacoes_fk_especificacoes` (`idespecificacoes`),
  CONSTRAINT `produtos_especificacoes_fk_especificacoes` FOREIGN KEY (`idespecificacoes`) REFERENCES `especificacoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `produtos_especificacoes_fk_produtos` FOREIGN KEY (`idprodutos`) REFERENCES `produtos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `produtos_fotos`
-- ----------------------------
DROP TABLE IF EXISTS `produtos_fotos`;
CREATE TABLE `produtos_fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arquivo` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `idprodutos` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idprodutos` (`idprodutos`),
  KEY `idprodutos_2` (`idprodutos`),
  CONSTRAINT `produtos_fotos_fk_produtos` FOREIGN KEY (`idprodutos`) REFERENCES `produtos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
