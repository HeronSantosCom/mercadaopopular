<?php

class Sql {

    private $conn;
    private $user;
    private $pass;
    private $db;
    private $host;
    private $error;
    private $insert_id;
    private $show_errors = true;
    public $total_rows;
    private $show_sql = false;

    public function __construct($host=null, $user=null, $pass=null, $db=null) {
        if (isset($host[0])) {
            $this->host = $host;
        }
        if (isset($user[0])) {
            $this->user = $user;
        }
        if (isset($pass[0])) {
            $this->pass = $pass;
        }
        if (isset($db[0])) {
            $this->db = $db;
        }
        $this->conn = @mysql_connect($host, $user, $pass);
        $this->error = mysql_error($this->conn);
        echo $this->error;
        mysql_select_db($db);
        if (mysql_client_encoding() != "utf8") {
            //mysql_query("SET NAMES utf8", $this->conn);
        }
    }

    public function findall($table) {
        $_SQL = 'SELECT * FROM ' . $table;
        $local_execute = mysql_query($_SQL);
        $this->error = mysql_error($this->conn);
        if (isset($this->error[0])) {
            $this->error .= '<br/>Codigo SQL: "' . $_SQL . '"';
        }
        $retorno = false;
        if (@mysql_num_rows($local_execute) > 0) {
            while ($line = mysql_fetch_array($local_execute, MYSQL_ASSOC)) {
                $retorno[] = $line;
            }
        }
        return $retorno;
    }

    public function getValue($data=null) {
        $retorno = null;
        if (is_array($data)) {
            if (isset($data['FIELD'])) {
                $FIELD = $data['FIELD'];
            }
            if (isset($data['FROM'])) {
                $FROM = $data['FROM'];
            }
            if (isset($data['WHERE'])) {
                $WHERE = $data['WHERE'];
            }
            $_SQL = 'SELECT ' . $FIELD . ' FROM ' . $FROM . ' WHERE ' . $WHERE;
            $local_execute = mysql_query($_SQL);
            $this->error = mysql_error($this->conn);
            if (isset($this->error[0])) {
                $this->error .= '<br/>Codigo SQL: "' . $_SQL . '"';
            }
            if (@mysql_num_rows($local_execute) > 0) {
                while ($line = mysql_fetch_array($local_execute, MYSQL_ASSOC)) {
                    $retorno = $line[$FIELD];
                }
            }
        }
        return $retorno;
    }

    public function getPrimaryKey($table) {
        $_SQL = 'SHOW /*!32332 FULL */ COLUMNS FROM `' . $table . '` WHERE `KEY` = "PRI" ;';
        $local_execute = mysql_query($_SQL);
        $this->error = mysql_error($this->conn);
        if (isset($this->error[0])) {
            $this->error .= '<br/>Codigo SQL: "' . $_SQL . '"';
        }
        $retorno = false;
        if (@mysql_num_rows($local_execute) > 0) {
            $retorno = mysql_result($local_execute, 0, 'Field');
        }
        return $retorno;
    }

    public function getColumns($table) {
        $_SQL = 'SHOW /*!32332 FULL */ COLUMNS FROM `' . $table . '` ;';
        $local_execute = mysql_query($_SQL);
        $this->error = mysql_error($this->conn);
        if (isset($this->error[0])) {
            $this->error .= '<br/>Codigo SQL: "' . $_SQL . '"';
        }
        $retorno = false;
        if (@mysql_num_rows($local_execute) > 0) {
            while ($line = mysql_fetch_array($local_execute, MYSQL_ASSOC)) {
                $retorno[] = $line['Field'];
            }
        }
        return $retorno;
    }

    public function find($data=null, $type='') {
        global $pg;
        global $GLOBAL;
        if ($type == 'grid') {
            $max_lines = (int) $GLOBAL['MAX_LINES'];
            $init = ($pg - 1) * $max_lines;
        }
        $retorno = null;
        if (is_array($data)) {
            $FIELDS = '*';
            if (isset($data['FIELDS'])) {
                $FIELDS = $data['FIELDS'];
            }
            $primary_key = $this->getPrimaryKey($data['FROM']);
            if ($type == 'grid') {
                if (isset($primary_key[0])) {
                    $FIELDS .= ', `' . $primary_key . '` as `grid_id`';
                }
            }
            $WHERE = null;
            if (isset($data['WHERE'])) {
                $WHERE[] = "(" . $data['WHERE'] . ")";
            }
            if ($type == 'grid' and isset($_GET['grid_filter']) and strlen($_GET['grid_filter']) > 0) {
                $col = $this->getColumns($data['FROM']);
                if (is_array($col)) {
                    foreach ($col as $key) {
                        $aux[] = $data['FROM'] . '.`' . $key . '` LIKE "%' . $_GET['grid_filter'] . '%"';
                    }
                    $fields_select = explode(", ", $data['FIELDS']);
                    if (is_array($fields_select)) {
                        foreach ($fields_select as $campo) {
                            if (!preg_match("#\.\*#", $campo)) {
                                //if ($campo != $data['FROM'].'.*') {
                                $field_this = explode(" as ", $campo);
                                if (isset($field_this[0]) and strlen($field_this[0]) > 0) {
                                    $aux[] = '' . $field_this[0] . ' LIKE "%' . $_GET['grid_filter'] . '%"';
                                }
                            }
                        }
                    }
                }
                if (isset($aux) and is_array($aux)) {
                    if (is_array($WHERE)) {
                        $WHERE[] = "(" . join(' or ', $aux) . ")";
                    } else {
                        $WHERE = "(" . join(' or ', $aux) . ")";
                    }
                }
            }
            if (is_array($WHERE)) {
                $WHERE = join(' AND ', $WHERE);
            }
            $ORDER = '';
            if (isset($data['ORDER'])) {
                $ORDER = $data['ORDER'];
            }
            $FROM = '';
            if (isset($data['FROM'])) {
                $FROM = $data['FROM'];
            }
            if (isset($data['JOIN'])) {
                $FROM .= ' ' . $data['JOIN'];
            }
            $OTHERS = '';
            if (isset($data['OTHERS'])) {
                $OTHERS = $data['OTHERS'];
            }
            if (!preg_match("#LIMIT#", $OTHERS) and ($type == 'grid')) {
                $OTHERS .= ' LIMIT ' . $init . ',' . $max_lines;
            }
            if ($type == 'grid') {
                if (strlen($ORDER) == 0) {
                    $ORDER = $_GET['grid_order'];
                } else {
                    if (isset($_GET['grid_order']) and strlen($_GET['grid_order']) > 0) {
                        $ORDER .= ',' . $_GET['grid_order'];
                    }
                }
            }


            $_SQL = 'SELECT ' . $FIELDS . ' FROM ' . $FROM;
            $_COUNT = 'SELECT count(*) AS `rows` FROM ' . $FROM;
            if (isset($WHERE[0])) {
                $_SQL .= ' WHERE ' . $WHERE;
                $_COUNT .= ' WHERE ' . $WHERE;
            }


            $test_qnt = mysql_query($_COUNT);
            if (mysql_num_rows($test_qnt) > 0) {
                $this->total_rows = mysql_result($test_qnt, 0, 'rows');
            }

            unset($test_qnt);

            if (isset($ORDER[0])) {
                $_SQL .= ' ORDER BY ' . $ORDER;
            } else {
                if (isset($primary_key[0])) {
                    $_SQL .= ' ORDER BY ' . $primary_key . ' DESC';
                }
            }
            if (isset($OTHERS[0])) {
                $_SQL .= ' ' . $OTHERS;
            }
            //echo $_SQL;
            $local_execute = mysql_query($_SQL);
            $this->sql = $_SQL;
            $this->error = mysql_error($this->conn);
            if (isset($this->error[0])) {
                $this->error .= '<br/>Codigo SQL: "' . $_SQL . '"';
            }
            $retorno = false;
            if (@mysql_num_rows($local_execute) > 0) {

                while ($line = mysql_fetch_array($local_execute, MYSQL_ASSOC)) {
                    $retorno[] = $line;
                }
            }
            $this->debug_error();
        }
        return $retorno;
    }

    public function save($data=null) {
        $retorno = true;
        $this->insert_id = null;
        if (is_array($data) and $this->conn) {
            if (isset($data['TABLE'])) {
                $TABLE = $data['TABLE'];
                if (isset($data['FIELDS'])) {
                    if (is_array($data['FIELDS'])) {
                        foreach ($data['FIELDS'] as $line) {
                            if (is_array($line)) {
                                $query_fields = null;
                                $WHERE = null;
                                foreach ($line as $col => $value) {
                                    if ($col == 'WHERE') {
                                        $WHERE = $value;
                                    } else {
                                        $query_fields[] = "`" . $col . "`=" . (strlen($value) > 0 ? "'" . (string) $value . "'" : "NULL") . "";
                                    }
                                }
                                if (is_array($query_fields)) {
                                    $_SQL = "INSERT INTO `" . $TABLE . "` SET " . join(", ", $query_fields);
                                    $execute = true;

                                    if (isset($WHERE)) {
                                        $verify = mysql_query('SELECT * FROM ' . $TABLE . ' WHERE ' . $WHERE);
                                        $_SQL = "UPDATE `" . $TABLE . "` SET " . join(", ", $query_fields) . " WHERE " . $WHERE;
                                        if (mysql_errno() != 0) {
                                            $execute = false;
                                        }
                                    } else if (isset($line['id' . $TABLE])) {
                                        $WHERE = '`id' . $TABLE . '` = "' . $line['id' . $TABLE] . '"';
                                        $verify = mysql_query('SELECT * FROM ' . $TABLE . ' WHERE ' . $WHERE);
                                        $_SQL = "UPDATE `" . $TABLE . "` SET " . join(", ", $query_fields) . " WHERE " . $WHERE;

                                        if (mysql_errno() != 0) {
                                            $execute = false;
                                        }
                                    }
                                    if (isset($_SQL[0]) and $execute != false) {
                                        mysql_query($_SQL);

                                        if ($this->show_sql) {
                                            echo $_SQL;
                                        }

                                        if (isset($line['id' . $TABLE])) {
                                            $this->insert_id[] = $line['id' . $TABLE];
                                        } else {
                                            $this->insert_id[] = mysql_insert_id();
                                        }
                                        $this->error = mysql_error($this->conn);
                                    } else {
                                        $this->error = 'Erro de AtualizaÃ§Ã£o do registro.';
                                    }
                                    if (isset($this->error[0])) {
                                        $retorno = false;
                                        $this->error .= '<br/>Codigo SQL: "' . $_SQL . '"';
                                    }
                                    $this->debug_error();
                                }
                            }
                        }
                        return $this->insert_id;
                    }
                } else {
                    $this->debug_error();
                    return false;
                }
            } else {
                $this->debug_error();
                return false;
            }
        }
    }

    public function remove($data=null) {
        if (is_array($data) and $this->conn) {
            if (isset($data['TABLE'])) {
                $TABLE = $data['TABLE'];
                if (isset($data['WHERE'])) {
                    $WHERE = $data['WHERE'];
                    $_SQL = "DELETE FROM `" . $TABLE . "` WHERE " . $WHERE;
                    mysql_query($_SQL);
                    if (mysql_error($this->conn)) {
                        $this->error = 'Falha ao excluir o registro.' . mysql_error($this->conn);
                        return false;
                    } else {
                        return mysql_affected_rows();
                    }
                } else {
                    $this->error = 'Falha(3) ao excluir o registro.';
                    $this->debug_error();
                }
            } else {
                $this->error = 'Falha(2) ao excluir o registro.';
                $this->debug_error();
            }
        } else {
            $this->error = 'Falha(1) ao excluir o registro.';
            $this->debug_error();
        }
    }

    public function tableExists($table) {
        if (strlen($table) > 0) {
            $exe = mysql_query("show tables where Tables_in_" . $this->db . " = '" . $table . "';") or die('oi' . mysql_error());
            if (mysql_num_rows($exe) > 0) {
                return true;
            }
        }

        return false;
    }

    public function execSql($sql) {
        return mysql_query($sql) or die(mysql_error());
    }

    private function set_debug($state = true) {
        $this->show_errors = $state;
    }

    public function setShowSQL($status = false) {
        $this->show_sql = $status;
    }

    public function debug_error() {
        if ($this->show_errors == true) {
            if (strlen($this->error) > 0) {
                echo '<div class="debug">' . $this->error . '</div>';
            }
        }
    }

}

?>