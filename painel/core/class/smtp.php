<?php

//bloqueador de acesso interno
$url_check = $_SERVER["PHP_SELF"];
if (eregi("smtp.php", "$url_check")) {
    header("Location: /index.php");
}

/**
 * @class Envio de Mensagem via SMTP
 */
class Smtp {

    // config de conexao
    var $conn;
    var $host;
    var $localhost;
    var $debug;
    var $xResource = 0;
    //config da autenticacao
    var $auth = false;
    //config da mensagem
    var $charset = "iso-8589-1";
    var $user;
    var $pass;
    var $header;
    var $message;
    var $onconnected = false;
    var $on_send_email = false;

    /**
     * Instanciador do SMTP
     * @param string $host
     * @param boolean $ssl
     * @param integer $port
     * @param integer $timeout
     */
    function Smtp($host, $ssl = false, $port = 25, $timeout = 45, $now = false) {
        $this->on_send_email = true;

        if ($this->on_send_email) {
            if ($ssl == "1") {
                $host = 'ssl://' . $host . ':' . $port;
                $port = 25;
            }

            $this->conn = @fsockopen($host, $port, $errno, $errstr, $timeout);

            $this->host = $host;
            if (isset($_SERVER["HTTP_HOST"])) {
                $this->localhost = $_SERVER["REMOTE_ADDR"];
            }
        }
    }

    /**
     *
     * @param <type> $extensions
     * @return <type>
     */
    function toMimeType($extensions) {
        switch ($extensions) {
            case "xls":
                $content_type = "application/excel";
                break;
            case "hqx":
                $content_type = "application/macbinhex40";
                break;
            case "doc":
            case "dot":
            case "wrd":
                $content_type = "application/msword";
                break;
            case "pdf":
                $content_type = "application/pdf";
                break;
            case "pgp":
                $content_type = "application/pgp";
                break;
            case "ps":
            case "eps":
            case "ai":
                $content_type = "application/postscript";
                break;
            case "ppt":
                $content_type = "application/powerpoint";
                break;
            case "rtf":
                $content_type = "application/rtf";
                break;
            case "tgz":
            case "gtar":
                $content_type = "application/x-gtar";
                break;
            case "gz":
                $content_type = "application/x-gzip";
                break;
            case "php":
            case "php3":
                $content_type = "application/x-httpd-php";
                break;
            case "js":
                $content_type = "application/x-javascript";
                break;
            case "ppd":
            case "psd":
                $content_type = "application/x-photoshop";
                break;
            case "swf":
            case "swc":
            case "rf":
                $content_type = "application/x-shockwave-flash";
                break;
            case "tar":
                $content_type = "application/x-tar";
                break;
            case "zip":
                $content_type = "application/zip";
                break;
            case "mid":
            case "midi":
            case "kar":
                $content_type = "audio/midi";
                break;
            case "mp2":
            case "mp3":
            case "mpga":
                $content_type = "audio/mpeg";
                break;
            case "ra":
                $content_type = "audio/x-realaudio";
                break;
            case "wav":
                $content_type = "audio/wav";
                break;
            case "bmp":
                $content_type = "image/bitmap";
                break;
            case "gif":
                $content_type = "image/gif";
                break;
            case "iff":
                $content_type = "image/iff";
                break;
            case "jb2":
                $content_type = "image/jb2";
                break;
            case "jpg":
            case "jpe":
            case "jpeg":
                $content_type = "image/jpeg";
                break;
            case "jpx":
                $content_type = "image/jpx";
                break;
            case "png":
                $content_type = "image/png";
                break;
            case "tif":
            case "tiff":
                $content_type = "image/tiff";
                break;
            case "wbmp":
                $content_type = "image/vnd.wap.wbmp";
                break;
            case "xbm":
                $content_type = "image/xbm";
                break;
            case "css":
                $content_type = "text/css";
                break;
            case "txt":
                $content_type = "text/plain";
                break;
            case "htm":
            case "html":
                $content_type = "text/html";
                break;
            case "xml":
                $content_type = "text/xml";
                break;
            case "mpg":
            case "mpe":
            case "mpeg":
                $content_type = "video/mpeg";
                break;
            case "qt":
            case "mov":
                $content_type = "video/quicktime";
                break;
            case "avi":
                $content_type = "video/x-ms-video";
                break;
            case "eml":
                $content_type = "message/rfc822";
                break;
            default:
                $content_type = "application/octet-stream";
                break;
        }
        return $content_type;
    }

    /**
     * Envia mensagem
     * @param string $from_name
     * @param string $from_email
     * @param string $to_name
     * @param string $to_email
     * @param string $subject
     * @param string $container
     * @param string $file [path]
     * @return boolean
     */
    function Send($from_name, $from_email, $to_name, $to_email, $subject, $container = null, $file = null, $description = null, $type = 'text/plain', $in_spool = false) {

        $stop = false;

        $this->on_send_email = true;

        if ($this->on_send_email) {
            if (preg_match("#(seu@email.com.br|exemplo@email.com.br|@localhost|@localdomain)#", $from_email)) {
                $this->getDebug('SendMail(): Envio de email (remetente) nao autorizado!');
                return false;
            }
            if (preg_match("#(seu@email.com.br|exemplo@email.com.br|@localhost|@localdomain)#", $to_email)) {
                $this->getDebug('SendMail(): Envio de email (destinatario) nao autorizado!');
                return false;
            }
            if ($this->Open()) {
                if (!$stop and !($this->getCode($this->Put('MAIL FROM: <' . $from_email . '>', true)) == '250')) {
                    $stop = true;
                }
                if (!$stop and !($this->getCode($this->Put('RCPT TO: <' . $to_email . '>', true)) == '250')) {
                    $stop = true;
                }
                if (!$stop) {
                    $this->getDebug('SendAttachment(): Start Message');
                    $boundary = md5(uniqid(time()));
                    if (!$stop and !($this->getCode($this->Put('DATA', true)) == '354')) {
                        $stop = true;
                    }
                    if (!$stop) {
                        $from = $from_email;
                        if (strlen(trim($from_name)) > 0) {
                            $from = '"' . trim($from_name) . '" <' . $from_email . '>';
                        }
                        $to = $to_email;
                        if (strlen(trim($to_name)) > 0) {
                            $to = '"' . trim($to_name) . '" <' . $to_email . '>';
                        }
                        $this->Put('Message-Id: <' . date('YmdHis') . '.' . md5(microtime()) . '.' . strtoupper($from_email) . '>');
                        $this->Put('Date: ' . date('D, d M Y H:i:s O'));
                        $this->Put('From: ' . $from);
                        $this->Put('Reply-To: ' . $from_email);
                        $this->Put('X-Mailer: P2B CRM');
                        $this->Put('X-MSMail-Priority: Low');
                        $this->Put('To: ' . $to);
                        $this->Put('Subject: ' . $subject);
                        $this->Put('MIME-Version: 1.0');
                        if ($file) {
                            $this->Put('Content-type: Multipart/Mixed; boundary="' . $boundary . '"');
                        }
                        if ($file) {
                            $this->Put("\r\n");
                            $this->Put('--' . $boundary);
                        }
                        $this->Put('Content-Type: ' . $type . '; charset=' . $this->charset);
                        $this->Put('Content-Transfer-Encoding: 8bit');
                        $this->Put("\r\n");
                        if (strlen($container) > 0) {
                            $this->Put($container);
                        }
                        if ($file) {
                            $this->Put("\r\n");
                        }
                        $this->Put("\r\n");
                        $files = explode(';', $file);
                        $file_exists = false;
                        if (count($files) > 0) {
                            for ($i = 0; $i < count($files); $i++) {
                                $file = $files[$i];
                                if (file_exists($file)) {
                                    $info = pathinfo($file);
                                    $filename = $info['basename'];
                                    $fileextensions = $info['extension'];

                                    $filetype = mime_content_type($file);
                                    if (preg_match("#unknown#", strtolower($filetype))) {
                                        $filetype = $this->toMimeType($fileextensions);
                                    }
                                    $this->Put('--' . $boundary);
                                    $this->Put('Content-Type: ' . $filetype . '; name="' . $filename . '"');
                                    $this->Put('Content-Transfer-Encoding: base64');
                                    if (strlen($description) > 0) {
                                        $this->Put('Content-Description: "' . $description . '"');
                                    }
                                    $this->Put('Content-Disposition: attachment; filename="' . $filename . '"');
                                    $this->Put("\r\n");
                                    $this->Put(chunk_split(base64_encode(file_get_contents($file))));
                                    $file_exists = true;
                                }
                            }
                            if ($file_exists) {
                                $this->Put('--' . $boundary . '--');
                                $this->Put("\r\n");
                            }
                        }
                    }
                    if (!$stop and $this->getCode($this->Put("\r\n\r\n" . '.', true)) == '250') {
                        $stop = false;
                    }
                    $this->getDebug('SendAttachment(): End Message');
                }
                if (!$stop) {
                    return true;
                }
            } else {
                $stop = true;
            }
        }
        if (!$stop) {
            return true;
        }
        return false;
    }

    /**
     * Abre Conexão
     * @return boolean
     */
    function Open() {
        if (is_resource($this->conn) and !$this->onconnected) {
            socket_set_timeout($this->conn, 0, 45000);
            //$this->getDebug('Open(): Resource Created');
            if ($this->getCode($this->Get()) == '220') {
                //$this->getDebug('Open(): Connection Created');
                $stop = false;
                if (($this->auth == "1")) {
                    if (($this->getCode($this->Put('EHLO ' . $this->host, true)) != '250')) {
                        $stop = true;
                    }
                    if (!$stop and $this->getCode($this->Put('RSET', true)) != '250') {
                        $stop = true;
                    }
                    if (!$stop and $this->getCode($this->Put('AUTH LOGIN', true)) != '334') {
                        $stop = true;
                    }
                    if (!$stop and $this->getCode($this->Put(base64_encode($this->user), true)) != '334') {
                        $stop = true;
                    }
                    if (!$stop and $this->getCode($this->Put(base64_encode($this->pass), true)) != '235') {
                        $stop = true;
                    }
                } else {
                    if ($this->getCode($this->Put('HELO ' . $this->host, true)) != '250') {
                        $stop = true;
                    }
                    if (!$stop and $this->getCode($this->Put('RSET', true)) != '250') {
                        $stop = true;
                    }
                }
                if (!$stop) {
                    $this->onconnected = true;
                }
                /* if (($this->auth == "1") and ($this->getCode($this->Put('EHLO '. $this->host, true)) != '250')) {
                  $stop = true;
                  } elseif ($this->getCode($this->Put('HELO '. $this->host, true)) != '250') {
                  $stop = true;
                  }
                  if (!$stop and $this->getCode($this->Put('RSET', true)) != '250') {
                  $stop = true;
                  }
                  if (!$stop) {
                  $code = $this->getCode($this->Put('AUTH LOGIN', true));
                  switch ($code) {
                  case "502":
                  $this->onconnected = true;
                  break;
                  case "334":
                  if ($this->getCode($this->Put(base64_encode($this->user), true)) != '334') {
                  $stop = true;
                  }
                  if (!$stop and $this->getCode($this->Put(base64_encode($this->pass), true)) == '235') {
                  $this->onconnected = true;
                  }
                  break;

                  default:
                  $stop = true;
                  break;
                  }
                  } */
            }
        }
        if ($this->onconnected) {
            return true;
        }
        $this->getDebug('Open(' . $this->host . '): Open Error');
        return false;
    }

    /**
     * Fecha a Conexão
     * @return boolean
     */
    function Close() {
        if ($this->on_send_email) {
            if (is_resource($this->conn)) {
                if ($this->getCode($this->Put('QUIT', true)) == '221' and fclose($this->conn)) {
                    if ($this->onconnected) {
                        $this->onconnected = false;
                    }
                    $this->getDebug('Close(): Connection and Resource Destroyed');
                    return true;
                }
            }
            $this->getDebug('Close(): Open Error');
            return false;
        }
        return true;
    }

    /**
     * Depura o SMTP
     * @param <type> $message
     */
    function getDebug($message = null) {
        if ($this->debug == "1") {
            print $message . "<br>\n";
        }
    }

    /**
     * Recupera o código da resposta do Servidor
     * @param string $value
     * @param string $length
     * @return mixed
     */
    function getCode($value = null, $length = 3) {
        if (strlen($value) > ($length - 1)) {
            return substr(trim($value), 0, $length);
        }
        return false;
    }

    /**
     * 	Enviar informações ao Servidor
     * @param string $string
     * @param string $print
     * @return mixed
     */
    function Put($string, $get = false, $print = true, $return = false) {
        $string .= "\r\n";
        $this->xResource++;
        if (is_resource($this->conn)) {
            if ($return = fputs($this->conn, $string, strlen($string) + 2)) {
                $resource = (strlen(trim($return)) > 0 ? '#' . trim($return) . '.' . $this->xResource : '#?.' . $this->xResource);
                $this->getDebug('Put(' . $resource . '): ' . $string);
                if ($get) {
                    $return = $this->Get($resource, $print);
                }
            }
        }
        return $return;
    }

    /**
     * Recupera Informações do Servidor
     * @param string $string
     * @param string $print
     * @param string $return
     * @param string $line
     * @return mixed
     */
    function Get($string = null, $print = true, $return = false, $line = null) {
        $continue = true;
        if (is_resource($this->conn)) {
            while ($continue) {
                $line = fgets($this->conn);
                $return .= $line;
                if (!(strpos($return, "\r\n") == false or substr($line, 3, 1) != ' ')) {
                    $continue = false;
                }
            }
        }
        if ($print and $return) {
            $this->getDebug('Get(' . $string . '): ' . $return);
        }
        return $return;
    }

}

?>