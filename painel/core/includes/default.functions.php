<?php
//funktions
function loadContent() {
    global $_MODULE;
    global $_MODULES;
    global $GLOBAL;
    global $MYSQL;
    global $pg;
    global $_THEME;
    global $SUPER;
    extract($_GET);

    if (!isset($module[0])) {
        include('modules/default/index.php');
    } else {
        $load = true;
        $permission_free = array('profile');
        if (isset($sub) and !in_array($sub,$permission_free)) {
            $modulos_permitidos = null;
            if (isset($_SESSION['login']['allowedmodules']) and strlen($_SESSION['login']['allowedmodules'])>0) {
                $modulos_permitidos = explode(":",$_SESSION['login']['allowedmodules']);
            }
            if (is_array($modulos_permitidos)) {
                if (!in_array($module, $modulos_permitidos)) {
                    echo '<div id="page-header"><h1>Acesso negado</h1></div>';
                    echo createMessage('Você não tem permissão para acessar este conteúdo', 'error');
                    $load = false;
                }
            }
        }

        if ($load == true) {
            
            echo '<div id="module_'.$module.'">';
            if (function_exists('int'.$module)) {
                eval('init'.$module);
            }
            if (isset($action) and isset($_MODULE[$module][$action]['name']) and !isset($_GET['ajax'])) {
                if (isset($_MODULE[$module][$action]['name'])) {
                    echo '<div id="page-header"><h1><a href="'. SYSTEM_PATH .'index.php?module='.$module.'&sub='.$sub.'&action='.$action.'">'.$_MODULE[$module]['name'].' &raquo; '.$_MODULE[$module][$action]['name'].'</a></h1></div>';
                }
            } else if(isset($sub) and isset($_MODULE[$module][$sub]['name']) and !isset($_GET['ajax'])) {
                echo '<div id="page-header"><h1><a href="'. SYSTEM_PATH .'index.php?module='.$module.'&sub='.$sub.'">'.$_MODULE[$module]['name'].' &raquo; '.$_MODULE[$module][$sub]['name'].'</a></h1></div>';
            } else  if (isset($_MODULE[$module]['name']) and !isset($_GET['ajax'])) {
                echo '<div id="page-header"><h1><a href="'. SYSTEM_PATH .'index.php?module='.$module.'">'.$_MODULE[$module]['name'].'</a></h1></div>';
            }
            if (!isset($action[0])) {
                $action = 'index';
            }
            $sub_link = false;
            if (isset($sub[0])) {
                $sub_link = $sub.'/';
            }
            include('modules/'.$module.'/'.$sub_link.$action.'.php');
            echo '</div>';
        }
    }
}

function loadSidebarMenu() {
    global $_MENU;
//include('trash/nav.php');
    $string = null;
    if (is_array($_MENU)) {
        $menuid = 1;
        foreach(array_keys($_MENU) as $tmp) {
            $this_argument = $tmp;
            $primary = $_MENU[$tmp];
            if (is_array($primary)) {
                foreach($primary as $item) {
                    if (isset($item['menu']) and is_array($item['menu'])) {

                        if (isset($item['module'],$_GET['module']) and  $_GET['module'] == $item['module']) {
                            $string[] = '<span class="ul-header"><a  href="#" toggle="menu_'.$menuid.'" class="toggle hidden menu_'.$item['module'].'" title="'.$item['name'].'">'.$item['name'].'</a></span>';
                        } else {
                            $string[] = '<span class="ul-header"><a  href="#" toggle="menu_'.$menuid.'" class="toggle visible menu_'.$item['module'].'" title="'.$item['name'].'">'.$item['name'].'</a></span>';
                        }

                        $string[] = '<ul id="menu_'.$menuid.'">';
                        foreach($item['menu'] as $secundary) {
                            $class = 'icn_manage_pages';
                            if (isset($secundary['class'])) {
                                $class = $secundary['class'];
                            }
                            $string[] = '<li><a class="'.$class.'" href="'. SYSTEM_PATH .''.$secundary['link'].'" title="'.$item['name'].' - '.$secundary['name'].'">'.$secundary['name'].'</a></li>';
                        }
                        $string[] = '</ul>';
                    } else {
                        $string[] = '<span class="ul-header"><a  href="'. SYSTEM_PATH .''.$item['link'].'">'.$item['name'].'</a></span>';
                    }
                }
            }
            $menuid++;
        }
    }
    if (is_array($string)) {
        $string = join("\n",$string);
    }
    echo $string;
}
function setMenuPermissions() {
    global $_MENU;
    global $_MODULE;
    global $_RELATORIOS_MENU;
    if (isset($_SESSION['login']['allowedmodules']) and strlen($_SESSION['login']['allowedmodules'])>0) {
        $modulos_permitidos = explode(":",$_SESSION['login']['allowedmodules']);
        if (!empty($modulos_permitidos) and is_array($_MENU)) {
            foreach($_MENU as $key =>$val) {
                if (in_array($key, $modulos_permitidos)) {
                    $novomenu[] = $_MENU[$key];
                } else {
                    unset($_MENU[$key]);
                    unset($_RELATORIOS_MENU[$key]);
                }
            }
            $_MENU = $novomenu;
        }

    }
    return $_MENU;
}
function setMenuOrder() {
    global $_MENU;
    $_MENUORDER = null;
    if (isset($_MENUORDER) and is_array($_MENUORDER)) {
        $tmp = $_MENU;
        $_MENU = null;
        foreach($_MENUORDER as $order) {
            if (isset($tmp[$order])) {
                $_MENU[$order] = $tmp[$order];
                unset($tmp[$order]);
            }
        }
        if (count($tmp)>0) {
            $_MENU = array_merge($_MENU, $tmp);
        }
    }
    return $_MENU;
}
function loadSidebarStatistics() {
    $functions = get_defined_functions();
    foreach($functions['user'] as $function_name) {
        if (preg_match("#stats#",strtolower($function_name))) {
            eval('$retorno[] = '.$function_name.'();');
        }
    }
    if (isset($retorno) and is_array($retorno)) {
        echo '<span class="ul-header">Estatísticas</span>';
        echo '<ul>';
        foreach($retorno as $stat) {
            echo ' <li>'.$stat['label'].': <span style="float:right;font-weight:bold;">'.$stat['value'].'</span></li>';
        }

        echo '</ul>';
    }
}
function breadcrumbs() {
    global $_MODULE;
    extract($_GET);
    $retorno[] = '<ul id="breadcrumbs" >';
    $retorno[] = '<li>Você está em: </li>';
    $retorno[] = '<li><a href="'. SYSTEM_PATH .'index.php">Início</a></li>';
    if (isset($module)) {
        if (isset($_MODULE[$module]['name'])) {
            $retorno[] = '<li> &raquo; <a href="'. SYSTEM_PATH .'index.php?module='.$module.'">'.$_MODULE[$module]['name'].'</a></li>';
        }
        if (isset($sub)) {
            if (isset($_MODULE[$module][$sub]['name'])) {
                $retorno[] = '<li> &raquo; <a href="'. SYSTEM_PATH .'index.php?module='.$module.'&sub='.$sub.'">'.$_MODULE[$module][$sub]['name'].'</a></li>';
            }
        }
        if (isset($action)) {

            if (isset($_MODULE[$module][$action]['name'])) {
                $action_name = $_MODULE[$module][$action]['name'];
            } else {
                switch($action) {
                    case 'new':
                        $action_name = 'Adicionar';
                        break;
                    case 'edit':
                        $action_name = 'Editar';
                        break;
                    case 'del':
                        $action_name = 'Excluir';
                        break;
                }
            }
            if (!isset($sub)) {
                $sub = '';
            }
//module='.$module.'&sub='.$sub.'&action='.$action.'
            $retorno[] = '<li> &raquo; <a href="'. SYSTEM_PATH .'index.php?'.$_SERVER['QUERY_STRING'].'">'.$action_name.'</a></li>';
        }
    } else {
        $retorno[] = '<li> &raquo; <a href="'. SYSTEM_PATH .'index.php">Dashboard</a></li>';
    }

    $retorno[] = '</ul>';
    echo join('',$retorno);
}
function createBox($title,$content,$size=12,$type='') {
// type: empty,table,stats
    if ($size>12) {
        $size = 12;
    }
    $print[] = '<div class="grid_'.$size.'">';
    if (isset($title[0])) {
        $print[] = '<div class="box-header"> '.$title.' </div>';
    }
    $print[] = '<div class="box '.$type.'">'.$content.'</div>';
    $print[] = '</div>';
    return join("",$print);
}
function createMessage($content,$type) {
    $retorno = false;
    switch($type) {
        case 'success':
            $retorno = '<div class="notification success"> <span class="strong">SUCESSO!</span> '.$content.' </div>';
            break;
        case 'error':
            $retorno =  '<div class="notification error"> <span class="strong">ERRO!</span> '.$content.' </div>';
            break;
        case 'warning':
            $retorno =  '<div class="notification warning"> <span class="strong">ATENÇÃO!</span> '.$content.' </div>';
            break;
        case 'tip':
            $retorno =  '<div class="notification tip"> <span class="strong">DICA:</span> '.$content.' </div>';
            break;
        case 'info':
            $retorno =  '<div class="notification info"> <span class="strong">INFORMAÇÃO!</span> '.$content.' </div>';
            break;
        case 'loading':
            $retorno =  '<div class="notification loading"> <span class="strong">AGUARDE!</span> '.$content.' </div>';
            break;
    }
    return $retorno;
}
function loginRequeired() {
    global $_THEME;
    if (!isset($_SESSION['login'])) {
        include('template/'. $_THEME .'/login.php');
        exit;
    }
}

function decode($str) {
    $charcode = mb_detect_encoding($str);
    if ($charcode != 'UTF-8') {
        $str = trim((string)$str);
        $str = stripslashes($str);
        $str = html_entity_decode($str);
    }
    return $str;
}

function translate() {
    $conteudo = ob_get_contents();
    ob_clean();
    $language = getTranslate();
    if (is_array($language) ) {
        foreach($language as $key => $value) {
            $conteudo = str_replace($key,$value,$conteudo);
        }
    }
    return $conteudo;
}
function getTranslate($domain = 'pt-br') {
    $language = false;

    include('core/languages/'.$domain.'.php');
    return $language;
}
function includeForm() {
    global $header_box;
    ob_flush();
    include('form.php');
    $_content = ob_get_contents();
    ob_clean();
    echo createBox($header_box, $_content);
}
function isRequeired($campos) {
    if (!is_array($campos)) {
        $campos = (array)$campos;
    }
    if ($_POST) {
        $emptys = null;
        foreach($campos as $key) {
            if (isset($_POST[$key])) {
                if (strlen(trim($_POST[$key]))==0) {
                    $emptys[] = '$("#'.$key.'").addClass("error");';
                }
            }
        }

        if (is_array($emptys)) {
            echo createMessage('Erro ao salvar, verifique abaixo os campos em vermelho!', 'error');
            echo '<script>';
            echo '$(document).ready(function() {';
            echo join("\n",$emptys);
            echo '});';
            echo '</script>';
            return true;
        }
    }
    return false;
}
function correctDate($date) {
    $char = '/';
    $new_char = '-';
    if (preg_match("#-#", $date)) {
        $char = '-';
        $new_char = '/';
    }
    $exploded_data =  explode("{$char}",$date);
    if (is_array($exploded_data)) {
        $exploded_data = array_reverse($exploded_data);
        return join($new_char,$exploded_data);
    }
    return false;
}

function debug($msg) {
    error_log($msg);
}
if(!function_exists('mime_content_type')) {

    function mime_content_type($filename) {

        $mime_types = array(

                'txt' => 'text/plain',
                'htm' => 'text/html',
                'html' => 'text/html',
                'php' => 'text/html',
                'css' => 'text/css',
                'js' => 'application/javascript',
                'json' => 'application/json',
                'xml' => 'application/xml',
                'swf' => 'application/x-shockwave-flash',
                'flv' => 'video/x-flv',

// images
                'png' => 'image/png',
                'jpe' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'jpg' => 'image/jpeg',
                'gif' => 'image/gif',
                'bmp' => 'image/bmp',
                'ico' => 'image/vnd.microsoft.icon',
                'tiff' => 'image/tiff',
                'tif' => 'image/tiff',
                'svg' => 'image/svg+xml',
                'svgz' => 'image/svg+xml',

// archives
                'zip' => 'application/zip',
                'rar' => 'application/x-rar-compressed',
                'exe' => 'application/x-msdownload',
                'msi' => 'application/x-msdownload',
                'cab' => 'application/vnd.ms-cab-compressed',

// audio/video
                'mp3' => 'audio/mpeg',
                'qt' => 'video/quicktime',
                'mov' => 'video/quicktime',

// adobe
                'pdf' => 'application/pdf',
                'psd' => 'image/vnd.adobe.photoshop',
                'ai' => 'application/postscript',
                'eps' => 'application/postscript',
                'ps' => 'application/postscript',

// ms office
                'doc' => 'application/msword',
                'rtf' => 'application/rtf',
                'xls' => 'application/vnd.ms-excel',
                'ppt' => 'application/vnd.ms-powerpoint',

// open office
                'odt' => 'application/vnd.oasis.opendocument.text',
                'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
}

function geraimg($img, $max_x, $max_y, $nome_foto) {

	list($width, $height) = getimagesize($img);

	$original_x = $width;
	$original_y = $height;

	// se a largura for maior que altura acho a porcentagem
	if($original_x > $original_y) {
	   $porcentagem = (100 * $max_x) / $original_x;
	}
	else {
	   $porcentagem = (100 * $max_y) / $original_y;
	}

	$tamanho_x = $original_x * ($porcentagem / 100);
	$tamanho_y = $original_y * ($porcentagem / 100);

	$image_p = imagecreatetruecolor($tamanho_x, $tamanho_y);
	$image   = imagecreatefromjpeg($img);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tamanho_x, $tamanho_y, $width, $height);

	/*
	$imagem1 = "marca.png";
	$imagem_marca = ImageCreateFromPng($imagem1);
	$pontoX1 = ImagesX($imagem_marca);
	$pontoY1 = ImagesY($imagem_marca);
	//cola marca dágua
	if ($max_x > 100) {
		ImageCopyMerge($image_p, $imagem_marca, 322, 363, 0, 0, $pontoX1, $pontoY1, 80);
		}
	*/
	//salva a imagem
	return imagejpeg($image_p, $nome_foto, 100);

}




?>