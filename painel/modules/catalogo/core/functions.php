<?php

function productView($loja, $product) {
    global $MYSQL;
    $array_find['FROM'] = 'catalog_products';
    $array_find['JOIN'] = 'LEFT JOIN catalog_manufacturers ON (catalog_manufacturers.idcatalog_manufacturers = catalog_products.idcatalog_manufacturers)';
    $array_find['FIELDS'] = 'catalog_products.*, catalog_manufacturers.name as manufacturer_name';
    $array_find['WHERE'] = 'idcatalog_products = "' . $product . '" AND catalog_products.idlogin = "' . $loja . '"';
    $find = $MYSQL->find($array_find);
    if (is_array($find) and isset($find[0])) {
        return $find[0];
    }
    return false;
}

function productList($loja, $page = 0, $category = null, $limit = 20) {
    global $MYSQL;
    $array_find['FROM'] = 'catalog_products';
    $array_find['JOIN'] = 'LEFT JOIN catalog_manufacturers ON (catalog_manufacturers.idcatalog_manufacturers = catalog_products.idcatalog_manufacturers)';
    $array_find['FIELDS'] = 'catalog_products.*, catalog_manufacturers.name as manufacturer_name';

    $WHERE[] = 'catalog_products.idlogin = "' . $loja . '"';
    if (strlen($category) > 0) {
        $WHERE[] = 'idcatalog_categories = "' . $category . '"';
    }

    if (isset($WHERE)) {
        $array_find['WHERE'] = join(" AND ", $WHERE);
    }

    $int = ($page > 0 ? ($page - 1 ) * 3 : 0);
    $array_find['OTHERS'] = 'LIMIT ' . $int . ',' . $limit;

    $find = $MYSQL->find($array_find);
    if (is_array($find)) {
        $return["products"] = $find;
        $return["rows"] = $MYSQL->total_rows;
        $return["pages"] = ceil($return["rows"] / 4);
        return $return;
    }
    return false;
}

function categoryName($loja, $category = false) {
    global $MYSQL;
    $find = $MYSQL->find(array('FROM' => 'catalog_categories', 'WHERE' => 'idlogin = "' . $loja . '" AND idcatalog_categories = "' . $category . '"'));
    if (is_array($find)) {
        return $find[0]['name'];
    }
    return false;
}

function categoryList($loja, $main = false) {
    global $MYSQL;
    $WHERE[] = 'idlogin = "' . $loja . '"';
    if (strlen($main) > 0) {
        $WHERE[] = 'idcatalog_categories_main = "' . $main . '"';
    } else {
        $WHERE[] = 'idcatalog_categories_main IS NULL';
    }
    $WHERE = join(" AND ", $WHERE);

    $find = $MYSQL->find(array('FROM' => 'catalog_categories', 'WHERE' => $WHERE));
    if (is_array($find)) {
        return $find;
    }
    return false;
}

function categoryBanner($loja, $position, $category = false) {
    global $MYSQL;
    $WHERE[] = 'idlogin = "' . $loja . '"';
    if (strlen($position) > 0) {
        $WHERE[] = 'position = "' . $position . '"';
    }
    if (strlen($category) > 0) {
        $WHERE[] = 'idcatalog_categories = "' . $category . '"';
    }
    $WHERE = join(" AND ", $WHERE);
    $find = $MYSQL->find(array('FROM' => 'banners', 'WHERE' => $WHERE));
    if (is_array($find)) {
        return $find;
    }
    return false;
}

function cartInsert($loja, $product) {
    global $MYSQL;
    $produto = $MYSQL->find(array('FROM' => 'catalog_products', 'WHERE' => 'idcatalog_products = "' . $product . '"'));
    if (is_array($produto)) {
        $_SESSION['webcart'][$loja]['products'][$product]['id'] = $product;
        $_SESSION['webcart'][$loja]['products'][$product]['thumbnail'] = $produto[0]['thumbnail'];
        $_SESSION['webcart'][$loja]['products'][$product]['qnt'] = 1;
        $_SESSION['webcart'][$loja]['products'][$product]['weight'] = $produto[0]['weight'];
        $_SESSION['webcart'][$loja]['products'][$product]['name'] = $produto[0]['name'];
        $_SESSION['webcart'][$loja]['products'][$product]['value'] = $produto[0]['value'];
        $cep = cepGet($loja);
        cepSet($loja, $cep["cep"], $cep["cepdigito"], $cep["tipo"]);
        return true;
    }
    return false;
}

function cartUpdate($loja, $product, $qnt = 1, $thumbnail = false, $weight = false, $name = false, $value = false) {
    if (isset($_SESSION['webcart'][$loja]['products'][$product])) {
        if ($qnt < 1) {
            $return = cartRemove($loja, $product);
            $cep = cepGet($loja);
            cepSet($loja, $cep["cep"], $cep["cepdigito"], $cep["tipo"]);
            return $return;
        } else {
            if ($qnt <= 10) {
                $_SESSION['webcart'][$loja]['products'][$product]['qnt'] = (int) $qnt;
                if ($thumbnail) {
                    $_SESSION['webcart'][$loja]['products'][$product]['thumbnail'] = $thumbnail;
                }
                if ($weight) {
                    $_SESSION['webcart'][$loja]['products'][$product]['weight'] = $weight;
                }
                if ($name) {
                    $_SESSION['webcart'][$loja]['products'][$product]['name'] = $name;
                }
                if ($value) {
                    $_SESSION['webcart'][$loja]['products'][$product]['value'] = $value;
                }
                $cep = cepGet($loja);
                cepSet($loja, $cep["cep"], $cep["cepdigito"], $cep["tipo"]);
                return true;
            }
        }
    }
    return false;
}

function cartIncrement($loja, $product) {
    if (isset($_SESSION['webcart'][$loja]['products'][$product])) {
        $return = cartUpdate($loja, $product, $_SESSION['webcart'][$loja]['products'][$product]['qnt'] + 1);
        return $return;
    }
    return false;
}

function cartDecrement($loja, $product) {
    if (isset($_SESSION['webcart'][$loja]['products'][$product])) {
        $return = cartUpdate($loja, $product, $_SESSION['webcart'][$loja]['products'][$product]['qnt'] - 1);
        return $return;
    }
    return false;
}

function cartRemove($loja, $product) {
    if (isset($_SESSION['webcart'][$loja]['products'][$product])) {
        unset($_SESSION['webcart'][$loja]['products'][$product]);
        if (count($_SESSION['webcart'][$loja]['products']) == 0) {
            unset($_SESSION['webcart'][$loja]['products']);
        }
        return true;
    }
    return false;
}

function cartList($loja) {
    if (isset($_SESSION['webcart'][$loja]['products'])) {
        return $_SESSION['webcart'][$loja]['products'];
    }
    return false;
}

function cepSet($loja, $cep, $cepdigito, $tipo) {
    $valor = $peso = 0;
    $envio = "pac";
    if ($tipo == "sedex") {
        $envio = "sedex";
    }
    $cepcompleto = $cep . $cepdigito;
    $_SESSION['webcart'][$loja]['cep']["cep"] = $cep;
    $_SESSION['webcart'][$loja]['cep']["cepdigito"] = $cepdigito;
    $_SESSION['webcart'][$loja]['cep']["localidade"] = null;
    $_SESSION['webcart'][$loja]['cep']["valor"] = 0;
    $_SESSION['webcart'][$loja]['cep']["tipo"] = $envio;
    if (isset($_SESSION['webcart'][$loja]['products'])) {
        foreach ($_SESSION['webcart'][$loja]['products'] as $product) {
            $peso += $product['weight'] * $product['qnt'];
            $valor += $product['value'] * $product['qnt'];
        }
    }
    if ($peso > 0) {
        $peso = str_replace('.', ',', $peso);
        $valor = str_replace('.', '', number_format((float) $valor, 2, ",", "."));
        //print "https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete_calcular.jhtml?postalCodeFrom=26256100&weight={$peso}&value={$valor}&postalCodeTo={$cepcompleto}";
        $return = @file_get_contents("https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete_calcular.jhtml?postalCodeFrom=26256100&weight={$peso}&value={$valor}&postalCodeTo={$cepcompleto}");
        if (strlen($return) > 0) {
            $result = explode('|', $return);
            if ($result[0] == 'ok') {
                $_SESSION['webcart'][$loja]['cep']["localidade"] = $result[1] . ($result[2] == "true" ? " (capital)" : " (interior)");
                $_SESSION['webcart'][$loja]['cep']["valor"] = ($envio == "sedex" ? $result[3] : $result[4]);
                return true;
            }
        }
    }
    return false;
}

function cepGet($loja) {
    if (isset($_SESSION['webcart'][$loja]['cep'])) {
        return $_SESSION['webcart'][$loja]['cep'];
    }
    return false;
}

function catalogCart() {
    global $MYSQL;
    global $GLOBAL;
    if ($_POST) {
        extract($_POST);
        if (isset($action)) {
            switch ($action) {
                case 'add':
                    if (isset($product) and strlen($product) > 0) {
                        $produto = $MYSQL->find(array('FROM' => 'catalog_products', 'WHERE' => 'idcatalog_products = "' . $product . '"'));
                        if (is_array($produto)) {
                            if (!isset($qnt) or strlen($qnt) == 0 or !is_numeric($qnt)) {
                                $qnt = 1;
                            } else {
                                if ($qnt < 1) {
                                    $qnt = 1;
                                }
                            }
                            $_SESSION['webcart'][$product]['id'] = $product;
                            $_SESSION['webcart'][$product]['thumbnail'] = $produto[0]['thumbnail'];
                            $_SESSION['webcart'][$product]['qnt'] = (int) $qnt;
                            $_SESSION['webcart'][$product]['weight'] = $produto[0]['weight'];
                            $_SESSION['webcart'][$product]['name'] = $produto[0]['name'];
                            $_SESSION['webcart'][$product]['value'] = $produto[0]['value'];
                        }
                    }
                    break;
                case 'update':
                    foreach ($_POST as $key => $value) {
                        $qnt_field = explode("_", $key);
                        if ($qnt_field[0] == 'qnt') {
                            $product = $qnt_field[1];
                            $_SESSION['webcart'][$product]['qnt'] = (int) $value;
                        }
                    }
                    break;
                case 'remove':

                    if (isset($product) and strlen($product) > 0) {
                        unset($_SESSION['webcart'][$product]);
                        if (count($_SESSION['webcart']) == 0) {
                            unset($_SESSION['webcart']);
                        }
                    }

                    break;
                case 'empty':
                    unset($_SESSION['webcart']);
                    break;
            }
        }
    }
    // monta carrinho
    if (isset($_SESSION['webcart']) and is_array($_SESSION['webcart'])) {
        if (isset($GLOBAL['PAGSEGURO']) and strlen($GLOBAL['PAGSEGURO']) > 0) {
            echo '<form name="formCart" method="post" action="https://pagseguro.uol.com.br/checkout/checkout.jhtml">';
            echo '<input type="hidden" name="email_cobranca" value="' . $GLOBAL['PAGSEGURO'] . '">';
            echo '<input type="hidden" name="tipo" value="CP">';
            echo '<input type="hidden" name="moeda" value="BRL">';
            echo '<input type="hidden" name="tipo_frete" value="EN">';
        } else {
            echo '<form action="" method="POST" name="formCart">';
        }
        echo '<input type="hidden" name="action" id="action" value=""/>';
        echo '<input type="hidden" name="product" id="product" value=""/>';
        echo '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
        echo '   <thead>';
        echo '       <tr>';
        echo '           <td>Produto</td>';
        echo '           <td width="40">Qnt.</td>';
        echo '           <td width="100">Vl. Unitário</td>';
        echo '           <td width="100">Vl. Total</td>';
        echo '           <td width="50">Excluir</td>';
        echo '       </tr>';
        echo '   </thead>';
        echo ' <tbody>';
        $j = 1;
        foreach ($_SESSION['webcart'] as $item) {
            echo '       <tr>';
            if (isset($GLOBAL['PAGSEGURO']) and strlen($GLOBAL['PAGSEGURO']) > 0) {
                echo '<input type="hidden" name="item_id_' . $j . '" value="' . $item['id'] . '">';
                echo '<input type="hidden" name="item_descr_' . $j . '" value="' . $item['name'] . '">';
                echo '<input type="hidden" name="item_quant_' . $j . '" value="' . $item['qnt'] . '">';
                echo '<input type="hidden" name="item_valor_' . $j . '" value="' . number_format($item['value'], 2, "", "") . '">';
                echo '<input type="hidden" name="item_frete_' . $j . '" value="0">';
                echo '<input type="hidden" name="item_peso_' . $j . '" value="' . $item['weight'] . '">';
                echo '';
            }
            echo '           <td><img src="' . SYSTEM_PATH . 'tmp/catalog/small/' . $item['thumbnail'] . '" align="left" class="cartImage"/>' . $item['name'] . '</td>';
            echo '           <td width="40"><input type="text" name="qnt_' . $item['id'] . '" value="' . $item['qnt'] . '" size="5"/></td>';
            echo '           <td width="100">R$ ' . number_format($item['value'], 2, ".", ",") . '</td>';
            echo '           <td width="100">R$ ' . number_format($item['value'] * $item['qnt'], 2, ".", ",") . '</td>';
            echo '           <td width="50"><a href="javascript:void(0);" onClick="deleteItemCart(\'' . $item['id'] . '\');">X</a> </td>';
            echo '       </tr>';
            $j++;
        }
        echo ' </tbody>';
        echo '</table>';

        echo '<a href="javascript:void(0);" onclick="updateCart();" class="updatecartLink">Atualizar</a> ';
        echo '<a href="javascript:void(0);" onclick="clearCart();" class="clearcartLink">Limpar Carrinho</a> ';
        if (isset($GLOBAL['PAGSEGURO']) and strlen($GLOBAL['PAGSEGURO']) > 0) {
            echo '<input type="image" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-pagar-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!">';
        } else {
            echo '<a href="checkout.php" class="checkoutLink">Fechar compra</a>';
        }
        echo '</form>';

        echo '<script>';
        echo 'function deleteItemCart(id) {';
        echo '  document.getElementById("product").value = id;';
        echo '  document.getElementById("action").value = "remove";';
        echo ' document.formCart.action = "";';
        echo ' document.formCart.submit();';
        echo '}';
        echo 'function clearCart() {';
        echo '  document.getElementById("action").value = "empty";';
        echo ' document.formCart.action = "";';
        echo ' document.formCart.submit();';
        echo '}';
        echo 'function updateCart() {';
        echo '  document.getElementById("action").value = "update";';
        echo ' document.formCart.action = "";';
        echo ' document.formCart.submit();';
        echo '}';

        echo '</script>';
    } else {
        echo 'Seu carrinho está vazio.';
    }
}

?>
