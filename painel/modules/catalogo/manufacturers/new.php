<?php

$header_box = 'Adicionar';
ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array('name');
    $error = isRequeired($campos);
    if (!$error) {
        $save_array = array();
        $save_array['TABLE'] = 'catalog_manufacturers';
        $save_array['FIELDS'][] = array(
                'name'=>"{$name}",
                'idlogin'=>"{$_SESSION['login']['idlogin']}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            $clear_form = true;
            echo "<script>window.location = '". SYSTEM_PATH ."index.php?module=". $_GET['module'] ."&sub=". $_GET['sub'] ."&success';</script>";
            echo createMessage('Fabricante adicionado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    } 
}

if ($clear_form) {
    $fields = $MYSQL->getColumns('catalog_manufacturers');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
}

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
