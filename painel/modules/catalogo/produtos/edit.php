<?php
$header_box = 'Editar';
ob_flush();
if ($_POST) {
    extract($_POST);
    $campos = array('idcatalog_categories','name','value','description');
    $error = isRequeired($campos);
    if (!$error and isset($id)) {
        $save_array = array();
        $save_array['TABLE'] = 'catalog_products';
        $save_array['FIELDS'][0] = array(
                'idcatalog_products'=>"{$id}",
                'idcatalog_categories'=>"{$idcatalog_categories}",
                'name'=>"{$name}",
                'guarantee' => "{$guarantee}",
                'value' => "{$value}",
                'last_value' => "{$last_value}",
                'weight' => "{$weight}",
                'description' => "{$description}",
                'thumbnail' => "{$thumbnail}"
        );

        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
        	if (isset($_FILES)) {
        		foreach(array_keys($_FILES) as $key) {
            		$foo = new Upload($_FILES[$key]);
           			if ($foo->uploaded) {
 	          			$foo->image_resize = true;
  						$foo->image_convert = 'jpg';
 						$foo->image_x = 150;
 						$foo->image_y = 150;
  						$foo->image_ratio_fill = true;
  						$foo->file_safe_name = true;
  						
  						$foo->Process('/home/usuariodigital/mercadaopopular.com.br/userfiles/'.$_SESSION['login']['alias'].'/small/');
  						
  						$novonome = $foo->file_dst_name;
  						
 						$foo->image_x = 650;
 						$foo->image_y = 650;
  						
  						$foo->Process('/home/usuariodigital/mercadaopopular.com.br/userfiles/'.$_SESSION['login']['alias'].'/big/');
  						  						
  						$foo->clean();
  						$save_array = array();
                		$save_array['TABLE'] = 'catalog_products_photos';
                		$save_array['FIELDS'][0] = array(
                        	'idcatalog_products'=>"{$id}",
                        	'photo'=>"{$novonome}"
                		);

                		$exephoto = $MYSQL->save($save_array);
           			}
            	}
            }
            echo createMessage('Produto alterado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    }
}
$find = $MYSQL->find(array('FROM'=>'catalog_products','WHERE'=>'idcatalog_products="'.$id.'"'));
extract($find[0]);
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
