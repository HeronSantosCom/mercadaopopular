<?php

$header_box = 'Adicionar';
ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array('idcatalog_categories','name','value','description');
    $error = isRequeired($campos);
    if (!$error) {
        $thumbnail = '';
        if (isset($_FILES['upload']['name'])) {
            $novonome = $thumbnail = date("dmYHis").md5($_FILES['upload']['name']).".jpg";
        }
        $save_array = array();
        $save_array['TABLE'] = 'catalog_products';
        $save_array['FIELDS'][0] = array(
                'idcatalog_categories'=>"{$idcatalog_categories}",
                'idlogin'=>"{$_SESSION['login']['idlogin']}",
                'guarantee' => "{$guarantee}",
                'name'=>"{$name}",
                'value' => "{$value}",
                'last_value' => "{$last_value}",
                'weight' => "{$weight}",
                'description' => "{$description}",
                'thumbnail' => "{$thumbnail}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            $id = $exe[0];
            if (isset($_FILES)) {
        		foreach(array_keys($_FILES) as $key) {
            		$foo = new Upload($_FILES[$key]);
           			if ($foo->uploaded) {
 	          			$foo->image_resize = true;
  						$foo->image_convert = 'jpg';
 						$foo->image_x = 150;
 						$foo->image_y = 150;
 						$foo->image_ratio_fill = true;

  						$foo->file_safe_name = true;
  						$foo->Process('/home/usuariodigital/mercadaopopular.com.br/userfiles/'.$_SESSION['login']['alias'].'/small/');
  						
  						$novonome = $foo->file_dst_name;
  						
 						$foo->image_x = 650;
 						$foo->image_y = 650;
  						$foo->Process('/home/usuariodigital/mercadaopopular.com.br/userfiles/'.$_SESSION['login']['alias'].'/big/');
  						
  						$save_array = array();
                		$save_array['TABLE'] = 'catalog_products_photos';
                		$save_array['FIELDS'][0] = array(
                        	'idcatalog_products'=>"{$id}",
                        	'photo'=>"{$novonome}"
                		);

                		$exephoto = $MYSQL->save($save_array);
           			}
            	}
            }
            $clear_form = true;
            echo "<script>window.location = '".SYSTEM_PATH."index.php?module=". $_GET['module'] ."&sub=". $_GET['sub'] ."&success';</script>";
            echo createMessage('Contato adicionado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    }
}

if ($clear_form) {
    $fields = $MYSQL->getColumns('catalog_products');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
}

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
