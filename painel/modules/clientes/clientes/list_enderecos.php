<?php

$sql = $MYSQL->find(
        array('FROM'=>'clientes_enderecos',
            'FIELDS'=>'clientes_enderecos.*,idclientes_enderecos as grid_id, concat_ws(" - ",fat_endereco,fat_numero,fat_complemento,fat_bairro,fat_cidade,fat_estado,fat_cep) as endereco',
            'WHERE'=>'`idclientes`="'.$id.'"'));
$array['header'] = array('nome'=>'Nome','endereco'=>'Cargo');
$array['body'] = $sql;
echo createGrid($array);

?>
<script>
    $('#contextbox div.fl').empty().append('<input type="button" onclick="addClientes();" class="button small strong" value="Adicionar">');
    function addClientes() {
        window.location = '<?php echo SYSTEM_PATH?>/index.php?module=clientes&sub=enderecos&action=new&idclientes=<?php echo $id ?>';
    }
    $('#contextbox table tbody tr').each(function() {
        var the_id = $(this).find("input[type=checkbox]").val();
        if (the_id!=undefined) {
            $(this).attr('ondblclick',null);
            $(this).bind('dblclick',function() {
                editContact(the_id);
            });
        }
    });
    function editContact(val) {
        window.location = '<?php echo SYSTEM_PATH?>/index.php?module=clientes&sub=enderecos&action=edit&idclientes=<?php echo $id ?>&id='+val;
    }
</script>
<style>
    #contactbox .box-header { display: none; }
    #contactbox div.tablefoot:last-child { display: none; }
    #contactbox table thead td { border-top:1px solid #999;}
    #contactbox .loading { display: none; }
</style>
