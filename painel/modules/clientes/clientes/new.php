<?php

$header_box = 'Adicionar';
ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array("nome","tipo","senha","documento1","telefone","email","fat_endereco","fat_numero","fat_bairro","fat_cidade","fat_estado","fat_pais","fat_cep");
    $error = isRequeired($campos);
    $find_email = $MYSQL->find(array("FROM"=>"clientes","WHERE"=>"email=$email"));
    if (!$error and is_array($find_email)) {
        $error = true;
        echo createMessage('Email ja cadastrado no sistema', 'error');
    }
    if (!$error) {
        $create_date = date("Y-m-d");
        $save_array = array();
        $nome = str_replace("'","´",$nome);
        $save_array['TABLE'] = 'clientes';

        $senha = md5($senha);
        $status = 1;
        $save_array['FIELDS'][] = array(
                'nome' => "{$nome}",
                'tipo' => "{$tipo}",
                'documento1' => "{$documento1}",
                'documento2' => "{$documento2}",
                'telefone' => "{$telefone}",
                'email' => "{$email}",
                'senha' => "{$senha}",
                'idlogin'=>"{$_SESSION['login']['idlogin']}",
                'fax' => "{$fax}",
                'fat_endereco' => "{$fat_endereco}",
                'fat_numero' => "{$fat_numero}",
                'fat_complemento' => "{$fat_complemento}",
                'fat_bairro' => "{$fat_bairro}",
                'fat_cidade' => "{$fat_cidade}",
                'fat_estado' => "{$fat_estado}",
                'fat_pais' => "{$fat_pais}",
                'fat_cep' => "{$fat_cep}",
                'observacoes' => "{$observacoes}",
                'create_date' => "{$create_date}",
                'status' =>"{$status}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo "<script>window.location = '".SYSTEM_PATH."index.php?module=". $_GET['module'] ."&sub=". $_GET['sub'] ."&success';</script>";
            echo createMessage('Cliente cadastrado com sucesso', 'success');

        } else {
            echo createMessage('Erro ao salvar cliente', 'error');
        }
    }
}

if ($clear_form) {
    $fields = $MYSQL->getColumns('clientes');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
    $fat_pais = 'Brasil';
}

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
