<form method="post" action="">
    <div class="row subheader">
        <strong>Endereço de entrega</strong>
    </div>
    <div class="row">
        <label>Nome completo: *</label>
        <input type="text" size="40" name="nome" id="nome" value="<?php echo $nome?>"/>
    </div>

    <div class="row">
        <label>Logradouro: *</label>
        <input type="text" size="40" name="fat_endereco" id="fat_endereco" value="<?php echo $fat_endereco?>"/>
    </div>
    <div class="row">
        <label>Número: *</label>
        <input type="text" size="40" name="fat_numero" id="fat_numero" alt="9999999999" value="<?php echo $fat_numero?>"/>
    </div>
    <div class="row">
        <label>Complemento: </label>
        <input type="text" size="40" name="fat_complemento" id="fat_complemento" value="<?php echo $fat_complemento?>"/>
    </div>
    <div class="row">
        <label>Bairro: *</label>
        <input type="text" size="40" name="fat_bairro" id="fat_bairro" value="<?php echo $fat_bairro?>"/>
    </div>
	<div class="row">
        <label>Cidade: </label>
		<input type="text" size="40" name="fat_cidade" id="fat_cidade" value="<?php echo $fat_cidade?>"/>
    </div>
    <div class="row">
        <label>Estado: *</label>
        <select name="fat_estado" id="fat_estado">
            <option value=""></option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espirito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MT">Mato Grosso</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
        </select>
    </div>
    <div class="row">
        <label>País: *</label>
        <input type="text" size="40" name="fat_pais" id="fat_pais" value="<?php echo $fat_pais?>"/>
    </div>
    <div class="row">
        <label>CEP: *</label>
        <input type="text" size="10" maxlength=9"" name="fat_cep" id="fat_cep" alt="99999-999" value="<?php echo $fat_cep?>"/>
    </div>
    <div class="row subheader">
        <strong>Outros</strong>
    </div>
    <div class="row">
        <label>Observações:</label>
        <textarea name="observacoes" id="observacoes" rows="7" cols="32"><?php echo $observacoes ?></textarea>
    </div>
    
    
    <br clear="all"/>
    <div id="products_list1"></div>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="<?php echo REFERRER ?>">Cancelar</a>
</form>
<script>
    $(document).ready(function() {
<?php
if (isset($fat_estado)) {
    echo '$("#fat_estado").val("'.$fat_estado.'");';
    
}
if (isset($idrevendas)) {
    echo '$("#idrevendas").val("'.$idrevendas.'").trigger("change");';
}
?>
    });
    
</script>
<div id="list_select1"></div>
<style>
    #contextbox .loading { display: none; }
    #list_select1 .loading { display: none; }
</style>