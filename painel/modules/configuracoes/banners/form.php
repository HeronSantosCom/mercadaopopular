<form method="post" action="" enctype="multipart/form-data">
    <div class="row">
        <label>Nome: *</label>
        <input type="text" size="40" name="name" id="name" value="<?php echo $name?>"/>
    </div>
    <div class="row">
        <label>URL: *</label>
        <input type="text" size="40" name="url" id="url" value="<?php echo $url?>"/>
    </div>

    <div class="row">
        <label>Categoria: *</label>
        <select name="idcatalog_categories" id="idcatalog_categories">
            <option></option>
            
            <?php
            $fields = $MYSQL->find(array('FROM'=>'catalog_categories','WHERE'=>'idlogin = "'.$_SESSION['login']['idlogin'].'"'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idcatalog_categories'].'">'.decode($row['name']).'</option>';
                }
            }
            ?>
        </select>
        <div class="row">
        <label>Posição: *</label>
        <select name="position" id="position">
            <option value="ESQUERDA">Esquerda</option>
            <option value="CENTRO">Centro</option>
            <option value="DIREITA">Direita</option>
        </select>
        </div>
        <div class="row">
        	<label>Foto: *</label>
        	<input type="file" name="upload" id="upload"/>
    	</div>
    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="<?php echo REFERRER ?>">Cancelar</a>
</form>