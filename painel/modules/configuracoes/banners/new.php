<?php

$header_box = 'Adicionar';

//idcatalog_categories_main

ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array('name','url');
    $error = isRequeired($campos);
    if (!$error) {
    	if (isset($_FILES)) {
        	foreach(array_keys($_FILES) as $key) {
            	$foo = new Upload($_FILES[$key]);
           		if ($foo->uploaded) {
    				$foo->image_resize = true;
  					$foo->image_convert = 'jpg';
 					$foo->image_x = 680;
 					$foo->image_y = 250;
 					$foo->image_ratio_fill = true;
					$foo->file_safe_name = true;
  					$foo->Process('/home/usuariodigital/mercadaopopular.com.br/userfiles/'.$_SESSION['login']['alias'].'/banners/');
  					$novonome = $foo->file_dst_name;
  					
  					$save_array = array();
        			$save_array['TABLE'] = 'banners';
			        $save_array['FIELDS'][] = array(
            		    'name'=>"{$name}",
                		'idlogin'=>"{$_SESSION['login']['idlogin']}",
                		'idcatalog_categories'=>"{$idcatalog_categories}",
                		'position'=>"{$position}",
		                'file'=>"{$novonome}",
       			        'url'=>"{$url}"
        			);
			        $exe = $MYSQL->save($save_array);
        			if (is_array($exe)) {
			            $clear_form = true;
            			echo "<script>window.location = '". SYSTEM_PATH ."index.php?module=". $_GET['module'] ."&sub=". $_GET['sub'] ."&success';</script>";
            			echo createMessage('Banners adicionada com sucesso', 'success');
        			} else {
            			echo createMessage('Erro ao salvar cadastro', 'error');
        			}
    			} else {
    				echo createMessage('Erro ao salvar cadastro', 'error');
    			}
    		}
    	}
    }
}
 $fields = $MYSQL->getColumns('banners');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
