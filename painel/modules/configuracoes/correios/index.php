<?php
	$CEP_ORIGEM = null;
	$configuracoes = null;
	if (isset($_SESSION['login']['configuracoes'])) {
		$configuracoes = unserialize($_SESSION['login']['configuracoes']);
		extract($configuracoes);
	}

	if ($_POST) {
		extract($_POST);
		//unserialize($_SESSION['login']['configuracoes']);
		
		$configuracoes['CEP_ORIGEM'] = $CEP_ORIGEM;
		$config_serial = serialize($configuracoes);
		
		$save_array =null;
		$save_array['TABLE'] = 'login';
        $save_array['FIELDS'][0] = array(
                'configuracoes'=>"{$config_serial}",
                'idlogin'=>"{$_SESSION['login']['idlogin']}");
        $exe = $MYSQL->save($save_array);
		if (is_array($exe)) {
			$_SESSION['login']['configuracoes'] = $config_serial;
            echo createMessage('Configurações alterada com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar configurações', 'error');
        }
	}
?><div class="grid_12">
    <div class="box-header"> Correios </div>
    <div class="box">
    	<form method="post" action="">
    		<div class="row cart">
				<label>Cep de Origem: *</label>
				<input type="text" size="30" name="CEP_ORIGEM" id="CEP_ORIGEM" value="<?php echo $CEP_ORIGEM?>"/>
			</div>
			<input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php">Cancelar</a>
    	</form>

    </div>
</div>