<?php
	$PAGSEGURO_EMAIL = null;
	$PAGSEGURO_TOKEN = null;
	$configuracoes = null;
	if (isset($_SESSION['login']['configuracoes'])) {
		$configuracoes = unserialize($_SESSION['login']['configuracoes']);
		extract($configuracoes);
	}

	if ($_POST) {
		extract($_POST);
		//unserialize($_SESSION['login']['configuracoes']);
		
		$configuracoes['PAGSEGURO_EMAIL'] = $PAGSEGURO_EMAIL;
		$configuracoes['PAGSEGURO_TOKEN'] = $PAGSEGURO_TOKEN;
		
		$config_serial = serialize($configuracoes);
		
		$save_array =null;
		$save_array['TABLE'] = 'login';
        $save_array['FIELDS'][0] = array(
                'configuracoes'=>"{$config_serial}",
                'idlogin'=>"{$_SESSION['login']['idlogin']}");
        $exe = $MYSQL->save($save_array);
		if (is_array($exe)) {
			$_SESSION['login']['configuracoes'] = $config_serial;
            echo createMessage('Configurações alterada com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar configurações', 'error');
        }
	}
?>
<div class="grid_12">
    <div class="box-header"> PagSeguro </div>
    <div class="box">
    	<form method="post" action="">
    		<div class="row cart">
				<label>E-mail: *</label>
				<input type="text" size="30" name="PAGSEGURO_EMAIL" id="PAGSEGURO_EMAIL" value="<?php echo $PAGSEGURO_EMAIL?>" />
			</div>
			<div class="row cart">
				<label>Token: *</label>
				<input type="text" size="30" name="PAGSEGURO_TOKEN" id="PAGSEGURO_TOKEN" value="<?php echo $PAGSEGURO_TOKEN?>"/> (Para retorno automático)
			</div>
			<input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php">Cancelar</a>
    	</form>
    </div>
</div>