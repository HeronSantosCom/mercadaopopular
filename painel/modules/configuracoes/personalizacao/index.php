<?php
	$LOJA_NOME = null;
	$LOJA_KEYWORDS = null;
	$LOJA_DESCRIPTION = null;
	$LOJA_SCHEMA = null;
	$LOJA_LOGO = null;
	$configuracoes = null;
	if (isset($_SESSION['login']['configuracoes'])) {
		$configuracoes = unserialize($_SESSION['login']['configuracoes']);
		extract($configuracoes);
	}

	if ($_POST) {
		extract($_POST);
		//unserialize($_SESSION['login']['configuracoes']);
		
		$configuracoes['LOJA_NOME'] = $LOJA_NOME;
		$configuracoes['LOJA_KEYWORDS'] = $LOJA_KEYWORDS;
		$configuracoes['LOJA_DESCRIPTION'] = $LOJA_DESCRIPTION;
		$configuracoes['LOJA_SCHEMA'] = $LOJA_SCHEMA;
		$foo = new Upload($_FILES['LOJA_LOGO']);
        if ($foo->uploaded) {
 	       	$foo->image_resize = true;
  			$foo->image_convert = 'jpg';
 			$foo->image_x = 360;
 			$foo->image_y = 90;
 			$foo->image_ratio_fill = "L";
  			$foo->file_safe_name = true;
  			$foo->Process('/home/usuariodigital/mercadaopopular.com.br/userfiles/'.$_SESSION['login']['alias'].'/');
  			$LOJA_LOGO = $foo->file_dst_name;
		
			$configuracoes['LOJA_LOGO'] = $LOJA_LOGO;
		}
		
		
		$config_serial = serialize($configuracoes);
		
		$save_array =null;
		$save_array['TABLE'] = 'login';
        $save_array['FIELDS'][0] = array(
                'configuracoes'=>"{$config_serial}",
                'idlogin'=>"{$_SESSION['login']['idlogin']}");
        $exe = $MYSQL->save($save_array);
		if (is_array($exe)) {
			$_SESSION['login']['configuracoes'] = $config_serial;
            echo createMessage('Configurações alterada com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar configurações', 'error');
        }
	}
?>
<div class="grid_12">
    <div class="box-header"> Personalização </div>
    <div class="box">
    	<form method="post" action="" enctype="multipart/form-data">
    		<div class="row cart">
				<label>Nome da loja: *</label>
				<input type="text" size="50" name="LOJA_NOME" id="LOJA_NOME" value="<?php echo $LOJA_NOME ?>"/>
			</div>
			<div class="row cart">
				<label>Palavras-chave: *</label>
				<input type="text" size="50" name="LOJA_KEYWORDS" id="LOJA_KEYWORDS" value="<?php echo $LOJA_KEYWORDS ?>" />
			</div>
			<div class="row cart">
				<label>Descrição: *</label>
				<input type="text" size="50" name="LOJA_DESCRIPTION" id="LOJA_DESCRIPTION" value="<?php echo $LOJA_DESCRIPTION ?>" />
			</div>
			<div class="row cart">
				<label>Esquema de cor: *</label>
				<select name="LOJA_SCHEMA" id="LOJA_SCHEMA">
					<option value="branco">Branco</option>
					<option value="preto">Preto</option>
					<option value="amarelo">Amarelo</option>
					<option value="verde">Verde</option>
					<option value="vermelho">Vermelho</option>
					<option value="azul">Azul</option>
					<option value="laranja">Laranja</option>
					<option value="cinza">Cinza</option>
					<option value="rosa">Rosa</option>
					<option value="roxo">Roxo</option>
				</select>
			</div>
			<div class="row cart">
				<label>Logo da loja: *</label>
				<input type="file" name="LOJA_LOGO" id="LOJA_LOGO" />
			</div>
			<input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php">Cancelar</a>
    	</form>
    </div>
</div>
<script>
	<?php if (isset($LOJA_SCHEMA) and strlen($LOJA_SCHEMA)>0) {
		echo "$('#LOJA_SCHEMA').val('".$LOJA_SCHEMA."');";
	}?>
</script>