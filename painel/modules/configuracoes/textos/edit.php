<?php
$header_box = 'Editar';
ob_flush();
if ($_POST) {
    extract($_POST);
    $campos = array('title','texto');
    $error = isRequeired($campos);
    if (!$error and isset($id)) {
        $save_array = array();
        $save_array['TABLE'] = 'textos';
        $save_array['FIELDS'][0] = array(
                'idtextos'=>"{$id}",
                'title'=>"{$title}",
                'texto'=>"{$texto}"
        );

        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
        	echo createMessage('Texto alterado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    }
}
$find = $MYSQL->find(array('FROM'=>'textos','WHERE'=>'idtextos="'.$id.'"'));
extract($find[0]);
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
