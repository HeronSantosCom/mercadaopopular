<?php
if ($_POST) {
    $save_array = array();
    $save_array['TABLE'] = 'globals';
    $global_fields = $MYSQL->find(array('FROM'=>'globals'));
    if (is_array($global_fields)) {
        foreach($global_fields as $key) {
            extract($key);
            if (isset($_POST[$var])) {
                $save_array['FIELDS'][] = array(
                        'var'=>"{$var}",
                        'value'=>$_POST[$var],
                        'WHERE' => '`var`="'.$var.'"'
                );
            }
        }

        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo createMessage('Configurações salvas com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar configurações', 'error');
        }
    } else {
        echo createMessage('Erro ao resgatar campos', 'error');
    }
}
?>
<div class="grid_12">
    <div class="box-header"> Sistema </div>
    <div class="box">
        <form method="post" action="">
            <div class="row">
                <label>Linhas da Grid:</label>
                <select name="MAX_LINES" id="MAX_LINES">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
            <div class="row">
                <label>Email PagSeguro:</label>
                <input name="PAGSEGURO" id="PAGSEGURO" size="40" value=""/>
            </div>
            <input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php">Cancelar</a>
        </form>

    </div>
</div>
<script>
<?php


$fields = $MYSQL->find(array('FROM'=>'globals'));
if (is_array($fields)) {
    foreach($fields as $row) {
        extract($row);
        echo '$("#'.$var.'").val("'.$value.'");';
    }
}
?>
</script>