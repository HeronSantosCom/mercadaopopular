<?php

$header_box = 'Adicionar';
ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array('name');
    $error = isRequeired($campos);
    if (!$error) {
        $save_array['TABLE'] = 'groups';
        $save_array['FIELDS'][] = array(
                'name'=>"{$name}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            $clear_form = true;
            echo createMessage('Grupo salvo com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar grupo', 'error');
        }
    }

}

if ($clear_form) {
    $fields = $MYSQL->getColumns('groups');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
}

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
