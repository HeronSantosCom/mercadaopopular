<?php
    if (isset($module)) {
        header("Location: /index.php");
    }
?>
<div id="page-header">
    <h1>Dashboard</h1>
</div>
<?php
$dashboard_painel = dashboardDefault();
if (is_array($dashboard_painel)) {
   echo join('',$dashboard_painel);
} else {
    echo 'Nenhum painel instalado';
}
?>
