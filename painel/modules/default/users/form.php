<form method="post" action="">
	<div class="row subheader">
        <strong>Informações do Cliente</strong>
    </div>
    <div class="row">
        <label>Documento: *</label>
        <?php if ($action=='edit') { ?>
        <strong><?php echo $documento?></strong>
            <?php } else { ?>
        <input type="text" name="documento" id="documento" size="40" value="<?php echo $documento?>"/>
            <?php } ?>
    </div>
    <div class="row">
        <label>Nome: *</label>
        <input type="text" name="nome" id="nome" size="40" value="<?php echo $nome?>"/>
    </div>
    <div class="row">
        <label>E-mail: *</label>
        <?php if ($action=='edit') { ?>
        <strong><?php echo $email?></strong>
            <?php } else { ?>
        <input type="text" name="email" id="email" size="40" value="<?php echo $email?>"/>
            <?php } ?>
    </div>
    <div class="row">
        <label>Endereço: *</label>
        <input type="text" name="endereco" id="endereco" size="40" value="<?php echo $endereco?>"/>
    </div>
	<div class="row">
        <label>Complemento: </label>
        <input type="text" name="complemento" id="complemento" size="40" value="<?php echo $complemento?>"/>
    </div>
    <div class="row">
        <label>Bairro: </label>
        <input type="text" name="bairro" id="bairro" size="40" value="<?php echo $bairro?>"/>
    </div>
    <div class="row">
        <label>Cidade/UF: </label>
        <input type="text" name="cidade" id="cidade" size="40" value="<?php echo $cidade?>"/> 
        <select>
        	<option>UF</option>
        </select>
    </div>
    <div class="row">
        <label>Telefone: </label>
        <input type="text" name="telefone" id="telefone" size="40" value="<?php echo $telefone?>"/>
    </div>
    <div class="row">
        <label>Celular: </label>
        <input type="text" name="celular" id="celular" size="40" value="<?php echo $celular?>"/>
    </div>
    <div class="row subheader">
        <strong>Informações da loja</strong>
    </div>
	
    <div class="row">
        <label>Apelido Loja: *</label>
        <?php if ($action=='edit') { ?>
        <strong>http://<?php echo $alias?>.mercadaopopular.com.br</strong>
            <?php } else { ?>
        http://<input type="text" name="alias" id="alias" size="40" value="<?php echo $alias?>"/>.mercadaopopular.com.br
            <?php } ?>
    </div>
    <div class="row">
        <label>Grupo: *</label>
        <select name="idgroup" id="idgroup">
            <?php
            $groups = $MYSQL->find(array('FROM'=>'groups'));
            if (is_array($groups)) {
                foreach($groups as $group) {
                    echo '<option value="'.$group['idgroups'].'">'.$group['name'].'</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="row subheader">
        <strong>Informações de permissões</strong>
    </div>
    <div class="row">
        <label style="height: 150px;">Módulos permitidos: *</label>
        <div>
            <?php
            ksort($_MODULE);
            if (isset($_MODULE)) {
                foreach($_MODULE as $key => $value) {
                    echo '<input type="checkbox" name="allowedmodules[]" id="allowedmodule_'.$key.'" value="'.$key.'">' . $_MODULE[$key]['name'] . '<br />';
                }
            }
            ?>
        </div>
    </div>
    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php?module=<?php echo $module ?>&sub=<?php echo $sub ?>">Voltar</a>
</form>
<script>
    $("#idgroup").val("<?php echo $idgroup ?>");
    <?php
        if (isset($allowedmodules)) {
            $allowedmodules = (array)explode(":",$allowedmodules);
            foreach($allowedmodules as $allowed) {
                echo "$('#allowedmodule_" . $allowed . "').attr('checked','checked');\n";
            }
        }
    ?>
</script>