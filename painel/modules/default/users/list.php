<?php
$sql = $MYSQL->find(
        array(
        'FROM'=>'login',
        'JOIN'=>'LEFT JOIN groups ON (login.idgroups = groups.idgroups)',
        'FIELDS'=>'login.*,groups.name as grupo')
        ,'grid');
$array['header'] = array('nome'=>'Nome','grupo'=>'Grupo','email'=>'E-mail','idcontatos'=>'');
$array['body'] = $sql;

echo createGrid($array);

?>
<script>
    $(".td_head:last-child").html('');
    $("td.idcontatos").each(function() {
        if ($(this).html() != '') {
            var idcontatos = $(this).html();
            $(this).html('<a href="javascript:void(0);" onclick="goTo(\''+idcontatos+'\');" class="noprint toright" alt="Alterar Senha"> <img src="img/icons/others/key.png" border="0" alt="Imprimir chamado" align="absmiddle"/></a>');
        }
    });
    function goTo(idcontatos) {
        window.location = '/index.php?module=default&sub=users&action=password&id='+idcontatos;
    }
</script>
<style>
    .idcontatos { text-align: center; }
</style>