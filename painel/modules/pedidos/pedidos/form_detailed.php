<?php
$payment_methods = $SUPER['payment_methods'];
$link_products = SYSTEM_PATH.'index.php?ajax&module='.$_GET['module'].'&sub='.$_GET['sub'].'&action=products_list';
$list_products = SYSTEM_PATH.'index.php?ajax&module='.$_GET['module'].'&sub='.$_GET['sub'].'&action=list_products';

//unset($_SESSION['proposal']);
?>
<form method="post" action="">

    <div class="row">
        <label>Cliente: </label>
        <select name="idclientes" id="idclientes">
            <option value=""></option>
            <?php
            $fields = $MYSQL->find(array('FROM'=>'clientes','ORDER'=>'nome'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idclientes'].'">'.decode($row['nome']).'</option>';
                }
            }
            ?>
        </select>
    </div>


    <div class="row">
        <label>Status: *</label>
        <select name="idpedidos_status" id="idpedidos_status">
            <option value=""></option>
            <?php
            $fields = $MYSQL->find(array('FROM'=>'pedidos_status'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idpedidos_status'].'">'.decode($row['name']).'</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="row subheader">
        <strong>Produtos / Serviços * </strong> <?php if ($action=='new') {
            echo '<input type="button" onclick="showAddCart();" class="button small strong" value="Adicionar"> <input type="button" onclick="delCart();" class="button small strong" value="Excluir">';
        } ?>

    </div>
    
    <div id="products_list">

    </div>
    <div id="products_list1">
    </div>
    <div class="row">
        <label>Formas de pagto: *</label>
        <select name="forma_pagamento" id="forma_pagamento" class="formas_pagto">
            <option value="0"></option>
            <?php
            if (isset($payment_methods) and is_array($payment_methods)) {
                foreach(array_keys($payment_methods) as $pm) {
                    echo '<option value="'.$payment_methods.'">'.$payment_methods[$pm].'</option>';
                }
            }

            ?>
        </select>

    </div>
    <div class="row">
        <label>Observações: </label>
        <textarea name="observacoes" id="observacoes" cols="50" rows="10"><?php echo $observacoes?></textarea>
    </div>

    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="<?php echo REFERRER ?>">Cancelar</a>
</form>
<script>
    $(document).ready(function() {

        openAjax('<?php echo $link_products ?>','products_list');


        $("#products_list").ajaxComplete(function() {
            $(".order_arrow").hide();
        });

    });
    
<?php
if (isset($idclientes) and strlen($idclientes)>0) {
    echo "$('#idclientes').val('".$idclientes."').attr('disabled','disabled');";
}
if (isset($idpedidos_status) and strlen($idpedidos_status)>0) {
    echo "$('#idpedidos_status').val('".$idpedidos_status."');";
}
if (isset($forma_pagamento) and strlen($forma_pagamento)>0) {
    echo "$('#forma_pagamento').val('".$forma_pagamento."').attr('disabled','disabled');";
}
?>
    // only numbers

</script>
<style>
    #addcart { position: absolute; z-index: 1000; margin: 0 auto; width: 400px;}
    #products_list2 .loading {display: none; }
    .loading { display:none;}
</style>
