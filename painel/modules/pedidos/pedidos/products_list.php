<?php

$array = null;
$array['header'] = array('produto_nome'=>'Produto','produto_qnt'=> 'Qnt.','produto_valor'=>'Valor');
if (isset($idproduct)) {
    $_SESSION['webcart'][$idproduct] = array(
            'grid_id'=>"{$idproduct}",
            'produto_id'=>"{$idproduct}",
            'produto_nome'=>"{$nome}",
            'produto_valor'=>"{$valor}",
            'produto_qnt'=>"{$qnt}");
}
if (isset($delete)) {
    $delete = explode(":",$delete);
    foreach($delete as $ids) {
        unset($_SESSION['webcart'][$ids]);
    }
    if (count($_SESSION['webcart'])==0) {
        unset($_SESSION['webcart']);
    }
}
$body = array();
if (isset($_SESSION['webcart'])) {
    $body = $_SESSION['webcart'];
}
$array['body'] = $body;

echo createGrid($array);

?>
<style>
    #products_list .box-header { display: none; }
    #products_list .tablefoot { display:none ;}
    #products_list table thead td { border-top:1px solid #999;}
    #products_list .qnt {
        width: 50px;
    }
    #products_list .tipo, #products_list .valor {
        width: 100px;
    }

    #products_list .order_arrow { display:none;}
</style>
<script>
    $(".order_arrow").hide();
    $("#categoria").val('');
    $("#idproducts").empty();
    $("#qnt").val('1');
    $("#valor").val('');
    $("#addcart").hide();
    $(document).ready(function() {
        $("#products_list table tbody tr").each(function () {
            $(this).removeAttr('ondblclick');
            var tipo = $(this).find('.tipo').text();
            switch(tipo) {
                case '1':
                    $(this).find('.tipo').text('Solução');
                    break;
                case '2':
                    $(this).find('.tipo').text('Opcional');
                    break;
                case '3':
                    $(this).find('.tipo').text('Serviços');
                    break;
            }
            var tipo = $(this).find('.valor').text('R$ ' + $.mask.string( $(this).find('.valor').text().replace('.',''), 'decimal' ) );
        });
    });
</script>
