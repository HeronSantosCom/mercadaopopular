function openAjax(new_url,container,effect) {
    if (effect==undefined) {
        effect='';
    }
    //$('html').scrollTop('0px');
    //$('body').scrollTop('0px');
    $("#"+container).html('<div class="notification loading"> <span class="strong">Aguarde!</span> Carregando informações... </div>');
    $.ajax({
        url: new_url,
        cache: false,
        success: function(data){
            if (effect.length==0) {
                effect='yes';
            }
            if (effect=='yes') {
                $("#"+container).hide();
                $("#"+container).html(data);
                $("#"+container).slideDown('slow');
            } else {
                $("#"+container).html(data);
            }
        },
        error: function(error){
            alert('Ocorreu um erro ao tentar carregar a página solicitada:'+error);
        }
    });
}
$(document).ready(function() {
    $(".checkall").live('click',function() {
        if ($(this).attr('checked') == false) {
            $(this).parents("table").find('input[type=checkbox]').not(this).removeAttr('checked');
        } else {
            $(this).parents("table").find('input[type=checkbox]').not(this).attr('checked','checked');
        }
    });
    $("#grid_filter").live('keydown',function(event) {
        if (event.keyCode == '13') {
            $("#grid_button").trigger('click');
        }
    });
    $(".order_arrow").fadeTo('fast',0.33);

    $(".order_arrow").live('mouseover',function() {
        $(this).fadeTo('fast',1);
    }).live('mouseout',function(){
        $(this).fadeTo('fast',0.33);
    });
    
});
function add() {
    window.location = '/painel/index.php?module='+module+'&sub='+msub+'&action=new';
}
function edit(id) {
    if (id.length>0) {
        window.location = '/painel/index.php?module='+module+'&sub='+msub+'&action=edit&id='+id;
    }
}
function del() {
    var agree = confirm('Deseja excluir o(s) registro(s) selecionado(s)?');
    if (agree) {
        var ids = new Array()
        $(".grid_checkbox:checked").each(function() {
            ids.push($(this).val());
        });
        if (ids.length>0) {
            window.location = '/painel/index.php?module='+module+'&sub='+msub+'&ids='+ids;
        } else {
            alert('É necessário selecionar um ítem para executar esta ação!');
        }
    }
}

function master_filter(url,container,effect) {
    if (effect == undefined || effect == null || effect == '') {
        effect = '';
    }
    var grid_filter = $("#grid_filter").val();
    var grid_order = $("#grid_order").val();
    if (grid_filter==undefined) {
        grid_filter = '';
    }
    if (grid_order==undefined) {
        grid_order = '';
    }
    openAjax(url+'&grid_filter='+grid_filter+'&grid_order='+grid_order,container,effect);
    
}
function setGridOrder(field,order) {
    $("#grid_order").val(field + ' '+ order);
    filter();
}




var dateDif = {
 dateDiff: function(strDate1,strDate2){
 return (((Date.parse(strDate2))-(Date.parse(strDate1)))/(24*60*60*1000)).toFixed(0);
 }
}

function diasEntreDatas(dataInicial, dataFinal)
{
 var mes, dataAtual, dataInicial, arrDataInicial, novaDataInicial, diasEntreDatas;
 mes = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

 arrDataFinal = dataFinal.split('/');
 arrDataInicial = dataInicial.split('/');
 novaDataInicial = mes[(arrDataInicial[1] - 1)] + ' ' + arrDataInicial[0] + ' ' + arrDataInicial[2];
 novaDataFinal = mes[(arrDataFinal[1] - 1)] + ' ' + arrDataFinal[0] + ' ' + arrDataFinal[2];
 diasEntreDatas = dateDif.dateDiff(novaDataInicial, novaDataFinal);
 return diasEntreDatas;
}
