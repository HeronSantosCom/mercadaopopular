<?php

// Murilho;

if ($_POST) {
    $message = ServiceLogin();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <base href="http://testecrm.phone2b.com.br/template/<?php echo $_THEME ?>/" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
        <script type="text/javascript" src="js/jquery-1.4.all.js"></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
        <link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
        <title>CRM Phone2b - Login</title>
    </head>
    <body>
        <div id="ieblock">

        </div>
        <div id="iemessage">
            <div id="page-header"><h1 >Aviso importante</h1></div>
            <p>Foi detectado que você está utilizando o navegador Internet Explorer para acessar esta página. Este navegador não obedece os padrões WEB e pode apresentar problemas em algumas páginas de nosso sistema. </p>
            <p>Por favor, tente acessar o CRM através de outro navegador.</p>
            <p>Tente utilizar os seguintes navegadores: <a href="http://br.mozdev.org/download/" target="_blank"><img src="img/firefox.png" border="0" alt="Firefox" title="Firefox"/></a>, <a href="http://www.google.com.br/chrome" target="_blank"><img src="img/chrome.png" border="0" alt="Google Chrome" title="Google Chrome"/></a>, <a href="http://www.apple.com/br/safari/download/" target="_blank"><img src="img/safari.png" border="0" alt="Apple Safari" title="Apple Safari"/></a>.</p>

        </div>
        <div id="login-wrapper">
            <div class="box-header login">
                <span class="fr"><a href="http://www.mercadaopopular.com.br">Voltar para o site</a></span>
                Loja virtual
            </div>
            <div class="box">
                <?php
                if (isset($message)) {
                    echo '<div class="notification error"> <span class="strong">ERRO!</span> '.$message.' </div>';
                }
                ?>
                <form method="post" action="" class="login">
                    <div class="row">
                        <label>E-mail:</label>
                        <input type="text" value="<?php echo (isset($_POST['username']) ? $_POST['username'] : null) ?>" name="username" id="username" />
                    </div>

                    <div class="row">
                        <label>Senha:</label>
                        <input type="password" value="" name="password" id="password"/>
                    </div>
                    <div class="row tr">
                    <!--<input type="checkbox" id="rememberme" checked="checked" class="checkbox" /> <label class="checkbox tl strong" for="rememberme" style="width:105px">Lembrar-me</label>-->
                        Não possui uma loja? <a href="http://www.mercadaopopular.com.br">Clique aqui</a> <input type="submit" value="Entrar" class="button" style="width:90px!important;" />
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $("#ieblock").fadeTo("fast",0.8);
<?php
if (preg_match("#MSIE#",$_SERVER['HTTP_USER_AGENT'])) {
    echo '$("#iemessage, #ieblock").show();';
} else {
    echo '$("#iemessage, #ieblock").hide();';
}
?>
    });


        </script>
        <style>
            body, html { overflow: hidden;}
        </style>
    </body>
</html>
<?php
echo translate();
?>