<?php
error_reporting(E_ALL);
$phpversion = explode(".",phpversion());
if ($phpversion[0] <5) {
    die(utf8_decode('É necessário ter instalado no servidor uma versão mais nova do PHP'));
}
session_start();
ob_start();
setlocale(LC_ALL, "pt");
//funcoes
require_once('includes/crm.functions.php');
require_once('includes/default.functions.php');
require_once('includes/default.services.php');
require_once('includes/grid.functions.php');
require_once('includes/pdf.functions.php');
require_once('includes/fckeditor/fckeditor.php');
//class
require_once('class/mysql.php');
require_once('class/smtp.php');
require_once('class/class.image/class.upload.php');
require_once('class/html2pdf/html2pdf.class.php');
ServiceLogoff();

// do

$config_file = @file_get_contents('config.inc');
if (strlen($config_file)>0) {
    $config_array = unserialize(base64_decode($config_file));
   
    $MYSQL = new Sql($config_array['mysql_host'], $config_array['mysql_user'], $config_array['mysql_pass'], $config_array['mysql_db']);

    if (isset($_SESSION['login']['theme']) and strlen($_SESSION['login']['theme'])>0) {
        $_THEME = $_SESSION['login']['theme'];
    }
    $GLOBAL = array();
    if ($MYSQL->tableExists('globals')) {
        $global_fields = $MYSQL->find(array('FROM'=>'globals'));

        if (is_array($global_fields)) {
            foreach($global_fields as $row) {
                extract($row);
                
                $GLOBAL[$var] = $value;
            }
        }
    }
    $GLOBAL['SYSTEM_PATH'] = $config_array['url'];

    if (!isset($pg)) {
        $pg = 1;
    }
    $_MENU = array();
    $_RELATORIOS_MENU = null;
    // load modules config files
    $_MODULES = scandir('modules');

    $_NOTALLOWED = array('.','..');
    if (is_array($_MODULES)) {
        $modulos_permitidos = null;
        if (isset($_SESSION['login']['allowedmodules']) and strlen($_SESSION['login']['allowedmodules'])>0) {
            $modulos_permitidos = explode(":",$_SESSION['login']['allowedmodules']);
        }

        foreach($_MODULES as $_m) {
            if (!in_array($_m, $_NOTALLOWED)) {
                @include_once('modules/'.$_m.'/core/config.php');
                $loadfunctions = true;
                if (is_array($modulos_permitidos)) {
                    if (!in_array($_m, $modulos_permitidos)) {
                        $loadfunctions = false;
                    }
                }
                if ($loadfunctions == true or ($_m == 'default') ) {
                    @include_once('modules/'.$_m.'/core/functions.php');
                    
                }
                $check_install = true;

                @include_once('modules/'.$_m.'/core/install.php');

            }
        }
    }

    $_MENU = setMenuPermissions();
    $_MENU = setMenuOrder();

    if (isset($_RELATORIOS_MENU) and is_array($_RELATORIOS_MENU)) {
        $_RELATORIOS = array();
        foreach($_RELATORIOS_MENU as $key => $val) {
            $_RELATORIOS = array_merge($_RELATORIOS,$_RELATORIOS_MENU[$key]);
        }
        $_MENU['relatorios'][] = array('module'=>'relatorios','name'=>'Relatórios','menu'=>$_RELATORIOS);
    }
} else {
    if (!preg_match("#install.php#",$_SERVER['SCRIPT_NAME'])) {
        header("Location: install.php");
    } else {

    }
    @include_once('modules/default/core/install.php');
}
define('SYSTEM_PATH',$GLOBAL['SYSTEM_PATH']);
?>