<?php
function createGrid($array) {
    extract($_GET);
    $print[] = '<div class="grid_12">';
    $print[] = '<div class="box-header">';
    $print[] = '    <div class="row">';
    $print[] = '        Filtro: <input type="text" name="grid_filter" id="grid_filter" value="'.(isset($_GET['grid_filter'])?$_GET['grid_filter']:'').'"/> <input type="button" name="grid_button" id="grid_button" value="Ok" class="button medium strong" onClick="filter();" />';
    $print[] = '        <input type="hidden" name="grid_order" id="grid_order" value="'.(isset($_GET['grid_order'])?$_GET['grid_order']:'').'"/>';
    $print[] = '    </div>';
    $print[] = '</div>';
    $print[] = '<div class="box table">';
    $print[] = '<form><table cellspacing="0">';
    if (isset($array['header']) and is_array($array['header'])) {
        $print[] = '<thead><tr>';
        $print[] = '<td width="20" class="checkbox"><input type="checkbox" class="checkall" /></td>';
        //make header
        foreach($array['header'] as $key => $head) {
            $print[] = '<td class="td_head '.$key.'" nowrap>'.$head.' <a href="javascript:void(0);" class="order_arrow" onclick="setGridOrder(\''.$key.'\',\'ASC\');"><img src="./img/icons/small/bullet_arrow_up.png" align="absmiddle" border="0" /></a><a href="javascript:void(0);" class="order_arrow" onclick="setGridOrder(\''.$key.'\',\'DESC\');"><img src="./img/icons/small/bullet_arrow_down.png" align="absmiddle" border="0" /></a></td>';
        }
        $print[] = '</tr></thead>';
    }
    // body
    $colspan = count($array['header']) + 1;
    $print[] = '<tbody>';
    $count_rows = 0;
    if (isset($array['body']) and is_array($array['body'])) {
        //make header
        foreach($array['body'] as $line) {
            $count_rows++;
            if (isset($line['grid_id'])) {
                $print[] = '<tr ondblclick="edit(\''.$line['grid_id'].'\');" title="Dê um duplo clique para editar">';
                $print[] = '<td class="td_body checkbox"><input type="checkbox" class="grid_checkbox" value="'.$line['grid_id'].'"/></td>';
            } else {
                $print[] = '<tr>';
                $print[] = '<td class="td_body checkbox"><input type="checkbox" class="grid_checkbox" value="'.uniqid().'"/></td>';

            }
            if (is_array($line)) {
                foreach($array['header'] as $key => $head) {
                    if (!isset($line[$key])) {
                        $print[] = '<td class="'.$key.'"></td>';
                    } else {
                        $print[] = '<td class="'.$key.'">'.decode(nl2br($line[$key])).'</td>';
                    }

                }
            }
            $print[] = '</tr>';
        }
    } else {
        $print[] = '<tr>';
        $print[] = '<td colspan="'.$colspan.'" class="tc">';
        $print[] = '<strong>Nenhum registro encontrado</strong>';
        $print[] = '</td>';
        $print[] = '</tr>';
    }
    // footer
    $print[] = '<tr>';
    $print[] = '<td colspan="'.$colspan.'" >';
    $print[] = '<div class="tablefoot fl">';
    $print[] = '    <input type="button" value="Adicionar" class="button small strong" onclick="add();"/>';
    // $print[] = '    <input type="button" value="Editar" class="button small strong" onclick="edit();" />';
    $print[] = '    <input type="button" value="Excluir" class="button small strong" onclick="del();"/>';
    $print[] = '</div>';
    $print[] = '<div class="tablefoot fr tr">'.paginateGrid().'</div>';
    $print[] = '</td>';
    $print[] = '</tr>';

    $print[] = '</tbody>';
    $print[] = '</table></form>';
    $print[] = '</div>';
    $print[] = '</div>';
    $print[] = '<script>';
    $print[] = '    $(".order_arrow").fadeTo("fast",0.33);';
    $print[] = '    var grid_order = $("#grid_order").val();';
    $print[] = '    if (grid_order!="") {';
    $print[] = '        grid_order = grid_order.split(" ");';
    $print[] = '        if (grid_order[1] == "DESC") {';
    $print[] = '            $("td.td_head." + grid_order[0] + " .order_arrow").eq(1).fadeTo("fast",1).removeClass("order_arrow");';
    $print[] = '        } else if (grid_order[1] == "ASC") {';
    $print[] = '            $("td.td_head." + grid_order[0] + " .order_arrow").eq(0).fadeTo("fast",1).removeClass("order_arrow");';
    $print[] = '        }';
    $print[] = '    }';
    $print[] = '</script>';
    $print = join('',$print);
    return $print;
}

function paginateGrid() {
    global $MYSQL;
    global $GLOBAL;
    $total = ceil($MYSQL->total_rows / $GLOBAL['MAX_LINES']);
    if ($total<1) {
        $total = 1;
    }

    extract($_GET);
    if (!isset($pg)) {
        $pg = '1';
    }
    $link = null;
    foreach($_GET as $key => $value) {
        if ($key != 'pg') {
            $link[] = $key.'=' .$value;
        }
    }
    if (is_array($link)) {
        $link = join('&',$link );
    }
    $class= 'disable_this';
    $anterior = $pg - 1;
    $proxima = $pg + 1;
    $ultima = $total;

    $link = SYSTEM_PATH . '/index.php?ajax&'.$link;

//echo $link;

    $print[] = 'Página <strong>'.$pg.'</strong> de <strong>'.$total.'</strong>';
    $print[] = '&nbsp;&nbsp;';
    if ($pg <= 1) {
        $print[] = '<a href="javascript:void(0);"><img src="./img/icons/small/pagination_first.png" align="absmiddle" class="disable_this" border="0"/> </a>';
        $print[] = '<a href="javascript:void(0);" ><img src="./img/icons/small/pagination_previous.png" align="absmiddle" class="disable_this" border="0"/> </a>';
    } if




    ($pg > 1) {
        $print[] = '<a href="javascript:void(0);" onclick="filter(\''.$link.'&pg=1\');"><img src="./img/icons/small/pagination_first.png" align="absmiddle" border="0"/> </a>';
        $print[] = '<a href="javascript:void(0);" onclick="filter(\''.$link.'&pg='.$anterior.'\');"><img src="./img/icons/small/pagination_previous.png" align="absmiddle" border="0"/> </a>';
    }
    if ($pg >= $total) {
        $print[] = '<a href="javascript:void(0);"><img src="./img/icons/small/pagination_next.png" align="absmiddle" class="disable_this" border="0"/> </a>';
        $print[] = '<a href="javascript:void(0);"><img src="./img/icons/small/pagination_last.png" align="absmiddle" class="disable_this" border="0"/></a>';
    } if




    ($pg < $total) {
        $print[] = '<a href="javascript:void(0);" onclick="filter(\''.$link.'&pg='.$proxima.'\');"><img src="./img/icons/small/pagination_next.png" align="absmiddle" border="0"/> </a>';
        $print[] = '<a href="javascript:void(0);" onclick="filter(\''.$link.'&pg='.$ultima.'\');"><img src="./img/icons/small/pagination_last.png" align="absmiddle" border="0"/></a>';
    }

    $print[] = '<script>$(".disable_this").fadeTo("fast",0.33);</script>';
    return join('',$print);
}
?>
