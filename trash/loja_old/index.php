<?php
$_THEME = 'default';
include("core/configurations.php");
loginRequeired();
if (isset($_GET['ajax'])) {
    sleep(1);
    loadContent();
    echo translate();
    exit;
}

$FIRST_PAGE = "/index.php";
if ($_SERVER["REQUEST_URI"] == $FIRST_PAGE or $_SERVER["REQUEST_URI"] == "/") {
    unset($_SESSION["REFERRER_HISTORY"]);
    $FIRST_PAGE = $_SERVER["REQUEST_URI"];
}
$_SESSION["REFERRER_HISTORY"][] = $_SERVER["REQUEST_URI"];

$NEW_REFERRER = $FIRST_PAGE;
if (isset($_SESSION["REFERRER_HISTORY"])) {
    $aux = $_SESSION["REFERRER_HISTORY"];
    unset($_SESSION["REFERRER_HISTORY"]);
    foreach ($aux as $key => $value) {
        $_SESSION["REFERRER_HISTORY"][] = $value;
        if ($value == $_SERVER["REQUEST_URI"]) {
            break;
        }
        $NEW_REFERRER = $value;
    }
}
define("REFERRER", $NEW_REFERRER);
// inclui o layout
require_once("template/". $_THEME ."/index.php");

echo translate();

?>