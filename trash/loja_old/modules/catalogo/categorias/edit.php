<?php
$header_box = 'Editar';
ob_flush();
if ($_POST) {
    extract($_POST);
    $campos = array('name');
    $error = isRequeired($campos);
    if (!$error and isset($id)) {
        $save_array = array();
        $save_array['TABLE'] = 'catalog_categories';
        $save_array['FIELDS'][] = array(
                'idcatalog_categories'=>"{$id}",
                'name'=>"{$name}",
                'idcatalog_categories_main'=>"{$idcatalog_categories_main}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo createMessage('Categoria alterada com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    }
}
$find = $MYSQL->find(array('FROM'=>'catalog_categories','WHERE'=>'idcatalog_categories="'.$id.'"'));
extract($find[0]);
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
