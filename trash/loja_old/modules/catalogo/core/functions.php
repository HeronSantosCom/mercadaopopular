<?php
function catalogCategories($url = 'catalogo.php') {
    global $MYSQL;
    $find = $MYSQL->find(array('FROM'=>'catalog_categories'));
    if (is_array($find)) {
        echo '<ul>';
        $link = null;
        if (is_array($_GET)) {
            foreach($_GET as $key => $value) {
                if ($key!='product' and $key!='category') {
                    $link[] = $key.'='.$value;
                }
            }
            if (is_array($link)) {
                $link = join("",$link).'&';
            }
        }
        foreach($find as $cat) {
            echo '<li><a href="'.$url.'?'.$link.'category='.$cat['idcatalog_categories'].'">'.$cat['name'].'</a></li>';
        }
        echo '</ul>';
    }
}

function catalogProducts($cart_file = 'cart.php') {
    global $MYSQL;
    $category_title = '';
    $WHERE = null;
    $array_find = null;
    $array_find['FROM'] = 'catalog_products';
    $array_find['JOIN'] = 'LEFT JOIN catalog_manufacturers ON (catalog_manufacturers.idcatalog_manufacturers = catalog_products.idcatalog_manufacturers)';
    $array_find['FIELDS'] = 'catalog_products.*, catalog_manufacturers.name as manufacturer_name';
    /* condiÃ§Ãµes */

    if (isset($_GET['category'])) {
        $WHERE[] = 'idcatalog_categories = "'.$_GET['category'].'"';
        $category_title = 'Categoria '.$_GET['category'];
    }
    if (isset($_GET['product'])) {
        $WHERE = array('idcatalog_products = "'.$_GET['product'].'"');
    }
    if (is_array($WHERE)) {
        $array_find['WHERE'] = join(" AND ",$WHERE);
    }
    $int = 0;
    if (isset($_GET['pg']) and strlen($_GET['pg'])>0) {
        $int = ($_GET['pg'] -1 )* 9;
    }

    $array_find['OTHERS'] = 'LIMIT '.$int.',9';
    $find = $MYSQL->find($array_find);
    $registros = $MYSQL->total_rows;


    if (is_array($find)) {
        if (strlen($category_title)>0) {
            echo '<p class="category_title">'.$category_title.'</p>';
        }
        $link = '?';
        $link_add = null;
        if (is_array($_GET)) {
            foreach($_GET as $key => $value) {
                if ($key!='product' and $key!='category' and $key!='pg') {
                    $link_add[] = $key.'='.$value;
                }
            }
        }
        if (is_array($link_add)) {
            $link .= join("",$link_add).'&';
        }
        if (!isset($_GET['product']) or strlen($_GET['product'])==0) {
            echo '<ul>';
        }
        $i = 0;

        foreach($find as $produto) {
            if (isset($_GET['product']) and strlen($_GET['product'])>0) {
                echo '<div class="product">';
                echo '<p class="product_title">'.$produto['name'].'</p>';

                $produto['manufacturer_name'] = 'Nome do fabricante';
                $produto['guarantee'] = '1 Ano';

                /**/
                echo '<div id="product_photo_box">';
                echo '  <img src="'.SYSTEM_PATH.'tmp/catalog/big/'.$produto['thumbnail'].'" width="250" />';
                echo '</div>';
                $array_pictures = null;
                $array_pictures['FROM'] = 'catalog_products_photos';
                $array_pictures['WHERE'] = 'idcatalog_products = "' . $produto['idcatalog_products'] . '"';
                $find_pictures = $MYSQL->find($array_pictures);
                if (is_array($find_pictures)) {
                    echo '<div id="product_photo_list">';
                    foreach($find_pictures as $pic) {
                        echo '  <a href="'.SYSTEM_PATH.'tmp/catalog/big/'.$pic['photo'].'"><img src="'.SYSTEM_PATH.'tmp/catalog/small/'.$pic['photo'].'" /></a>';
                    }
                    echo '</div>';
                }
                echo '<div id="product_info_box">';
                echo '	<p class="product_info"><strong>Código</strong><br>'.str_pad($produto['idcatalog_products'],10,'0',STR_PAD_LEFT).'</p>';
                echo '	<p class="product_info"><strong>Fabricante</strong><br>'.$produto['manufacturer_name'].'</p>';
                echo '<p class="product_info product_value">R$ '.number_format($produto['value'],2,",",".").'</p>';
                echo '	<p class="product_info"><strong>Garantia</strong><br>'.$produto['guarantee'].'</p>';
                echo '</div>';
                echo '<form method="post" action="'.$cart_file.'">';
                echo '<input type="hidden" name="product" value="'.$produto['idcatalog_products'].'" />';
                echo '<input type="hidden" name="action" value="add" />';
                echo '<input type="submit" value="Adicionar" class="cart_button"/>';
                echo '</form>';
                echo '<div class="toggle">';
                echo '  <p class="product_header">Caracteristicas</p>';
                echo '  <p class="product_description">'.$produto['description'].'</p>';
                echo '  <p>&nbsp;</p>';
                echo '</div>';

                // hack


                $paginate = false;
            } else {
                if ($i==3) {
                    echo '</ul><ul>';
                    $i=0;
                }
                echo '<li class="product">';
                echo '<a href="'.$link.'product='.$produto['idcatalog_products'].'">';
                echo '<img src="'.SYSTEM_PATH.'tmp/catalog/small/'.$produto['thumbnail'].'" />';
                echo '<p class="product_title">'.$produto['name'].'</p>';
                if (strlen($produto['description'])>0) {
                    $small_description = join(" ",array_slice(explode(" ",$produto['description']), 0, 10)).' ...';
                    echo '<p class="product_description">'.nl2br($small_description).'</p>';
                }
                echo '<p class="product_value">R$ '.number_format($produto['value'],2,",",".").'</p>';
                echo '<p class="product_details">+ detalhes</p>';
                echo '</a>';
                echo '</li>';

                $i++;
                $paginate = true;
            }
        }
        if (!isset($_GET['product']) or strlen($_GET['product'])==0) {
            echo '</ul>';
        }
        if ($paginate == true) {
            $paginas = ceil($registros / 10);
            echo '<br clear="all"/>';
            echo '<br clear="all"/>';
            echo '<br clear="all"/>';
            if ($paginas>1) {

                echo '<div id="paginate">Navegação: ';
                $pag = null;
                for ($i=1;$i<=$paginas;$i++) {
                    $pag[] = '<a href="'.$link.'pg='.$i.'">'.$i.'</a>';
                }
                if (is_array($pag)) {
                    echo join(", ",$pag);
                }
                echo '</div>';
            }
        }
    } else {
        echo '<p>Nenhum produto cadastrado nesta categoria</p>';
    }
}
function catalogCart() {
    global $MYSQL;
    global $GLOBAL;
    if ($_POST) {
        extract($_POST);
        if (isset($action)) {
            switch($action) {
                case 'add':
                    if (isset($product) and strlen($product)>0) {
                        $produto = $MYSQL->find(array('FROM'=>'catalog_products','WHERE'=>'idcatalog_products = "'.$product.'"'));
                        if (is_array($produto)) {
                            if (!isset($qnt) or strlen($qnt)==0 or !is_numeric($qnt)) {
                                $qnt = 1;
                            } else {
                                if ($qnt<1) {
                                    $qnt = 1;
                                }
                            }
                            $_SESSION['webcart'][$product]['id'] = $product;
                            $_SESSION['webcart'][$product]['thumbnail'] = $produto[0]['thumbnail'];
                            $_SESSION['webcart'][$product]['qnt'] = (int)$qnt;
                            $_SESSION['webcart'][$product]['weight'] = $produto[0]['weight'];
                            $_SESSION['webcart'][$product]['name'] = $produto[0]['name'];
                            $_SESSION['webcart'][$product]['value'] = $produto[0]['value'];
                        }
                    }
                    break;
                case 'update':
                    foreach($_POST as $key => $value) {
                        $qnt_field = explode("_",$key);
                        if ($qnt_field[0]=='qnt') {
                            $product = $qnt_field[1];
                            $_SESSION['webcart'][$product]['qnt'] = (int)$value;
                        }

                    }
                    break;
                case 'remove':

                    if (isset($product) and strlen($product)>0) {
                        unset($_SESSION['webcart'][$product]);
                        if (count($_SESSION['webcart'])==0) {
                            unset($_SESSION['webcart']);
                        }
                    }

                    break;
                case 'empty':
                    unset($_SESSION['webcart']);
                    break;
            }
        }
    }
    // monta carrinho
    if (isset($_SESSION['webcart']) and is_array($_SESSION['webcart'])) {
        if (isset($GLOBAL['PAGSEGURO']) and strlen($GLOBAL['PAGSEGURO'])>0) {
            echo '<form name="formCart" method="post" action="https://pagseguro.uol.com.br/checkout/checkout.jhtml">';
            echo  '<input type="hidden" name="email_cobranca" value="'.$GLOBAL['PAGSEGURO'].'">';
            echo  '<input type="hidden" name="tipo" value="CP">';
            echo  '<input type="hidden" name="moeda" value="BRL">';
            echo '<input type="hidden" name="tipo_frete" value="EN">';
        } else {
            echo '<form action="" method="POST" name="formCart">';
        }
        echo '<input type="hidden" name="action" id="action" value=""/>';
        echo '<input type="hidden" name="product" id="product" value=""/>';
        echo '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
        echo '   <thead>';
        echo '       <tr>';
        echo '           <td>Produto</td>';
        echo '           <td width="40">Qnt.</td>';
        echo '           <td width="100">Vl. Unitário</td>';
        echo '           <td width="100">Vl. Total</td>';
        echo '           <td width="50">Excluir</td>';
        echo '       </tr>';
        echo '   </thead>';
        echo ' <tbody>';
        $j=1;
        foreach($_SESSION['webcart'] as $item) {
            echo '       <tr>';
            if (isset($GLOBAL['PAGSEGURO']) and strlen($GLOBAL['PAGSEGURO'])>0) {
                echo '<input type="hidden" name="item_id_'.$j.'" value="'.$item['id'].'">';
                echo '<input type="hidden" name="item_descr_'.$j.'" value="'.$item['name'].'">';
                echo '<input type="hidden" name="item_quant_'.$j.'" value="'.$item['qnt'].'">';
                echo '<input type="hidden" name="item_valor_'.$j.'" value="'.number_format($item['value'],2,"","").'">';
                echo '<input type="hidden" name="item_frete_'.$j.'" value="0">';
                echo '<input type="hidden" name="item_peso_'.$j.'" value="'.$item['weight'].'">';
                echo '';

            }
            echo '           <td><img src="'.SYSTEM_PATH.'tmp/catalog/small/'.$item['thumbnail'].'" align="left" class="cartImage"/>'.$item['name'].'</td>';
            echo '           <td width="40"><input type="text" name="qnt_'.$item['id'].'" value="'.$item['qnt'].'" size="5"/></td>';
            echo '           <td width="100">R$ '.number_format($item['value'],2,".",",").'</td>';
            echo '           <td width="100">R$ '.number_format($item['value'] * $item['qnt'],2,".",",").'</td>';
            echo '           <td width="50"><a href="javascript:void(0);" onClick="deleteItemCart(\''.$item['id'].'\');">X</a> </td>';
            echo '       </tr>';
            $j++;
        }
        echo ' </tbody>';
        echo '</table>';

        echo '<a href="javascript:void(0);" onclick="updateCart();" class="updatecartLink">Atualizar</a> ';
        echo '<a href="javascript:void(0);" onclick="clearCart();" class="clearcartLink">Limpar Carrinho</a> ';
        if (isset($GLOBAL['PAGSEGURO']) and strlen($GLOBAL['PAGSEGURO'])>0) {
            echo '<input type="image" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-pagar-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!">';
        } else {
            echo '<a href="checkout.php" class="checkoutLink">Fechar compra</a>';
        }
        echo '</form>';

        echo '<script>';
        echo 'function deleteItemCart(id) {';
        echo '  document.getElementById("product").value = id;';
        echo '  document.getElementById("action").value = "remove";';
        echo ' document.formCart.action = "";';
        echo ' document.formCart.submit();';
        echo '}';
        echo 'function clearCart() {';
        echo '  document.getElementById("action").value = "empty";';
        echo ' document.formCart.action = "";';
        echo ' document.formCart.submit();';
        echo '}';
        echo 'function updateCart() {';
        echo '  document.getElementById("action").value = "update";';
        echo ' document.formCart.action = "";';
        echo ' document.formCart.submit();';
        echo '}';

        echo '</script>';
    } else {
        echo 'Seu carrinho está vazio.';
    }
}
?>
