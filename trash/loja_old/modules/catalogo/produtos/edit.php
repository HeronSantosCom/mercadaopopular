<?php
$header_box = 'Editar';
ob_flush();
if ($_POST) {
    extract($_POST);
    $campos = array('idcatalog_categories','name','value','description');
    $error = isRequeired($campos);
    if (!$error and isset($id)) {
        $save_array = array();
        $save_array['TABLE'] = 'catalog_products';
        $save_array['FIELDS'][0] = array(
                'idcatalog_products'=>"{$id}",
                'idcatalog_categories'=>"{$idcatalog_categories}",
                'idcatalog_manufacturers'=>"{$idcatalog_manufacturers}",
                'name'=>"{$name}",
                'guarantee' => "{$guarantee}",
                'value' => "{$value}",
                'last_value' => "{$last_value}",
                'weight' => "{$weight}",
                'description' => "{$description}",
                'thumbnail' => "{$thumbnail}"
        );

        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            if (isset($_FILES['upload']['name']) and strlen($_FILES['upload']['name'])>0) {
                $uploaddir = 'tmp/catalog/';
                $novonome = date("dmYHis").md5($_FILES['upload']['name']).".jpg";

                geraimg($_FILES['upload']['tmp_name'],$GLOBAL['BIG_WIDTH_SIZE'],$GLOBAL['BIG_HEIGHT_SIZE'],$uploaddir . "big/" .$novonome);
                geraimg($_FILES['upload']['tmp_name'],$GLOBAL['SMALL_WIDTH_SIZE'],$GLOBAL['SMALL_HEIGHT_SIZE'],$uploaddir . "small/" .$novonome);


                $save_array = array();
                $save_array['TABLE'] = 'catalog_products_photos';
                $save_array['FIELDS'][0] = array(
                        'idcatalog_products'=>"{$id}",
                        'photo'=>"{$novonome}"
                );

                $exephoto = $MYSQL->save($save_array);
            }
            echo createMessage('Produto alterado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    }
}
$find = $MYSQL->find(array('FROM'=>'catalog_products','WHERE'=>'idcatalog_products="'.$id.'"'));
extract($find[0]);
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
