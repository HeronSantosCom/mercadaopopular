<form method="post" action="" enctype="multipart/form-data">

    <div class="row">
        <label>Nome: *</label>
        <input type="text" size="40" name="name" id="name" value="<?php echo $name?>"/>
    </div>
    <div class="row">
        <label>Categoria: *</label>
        <select name="idcatalog_categories" id="idcatalog_categories">
            <option></option>
            <?php
            $fields = $MYSQL->find(array('FROM'=>'catalog_categories'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idcatalog_categories'].'">'.decode($row['name']).'</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="row">
        <label>Fabricante: *</label>
        <select name="idcatalog_manufacturers" id="idcatalog_manufacturers">
            <option></option>
            <?php
            $fields = $MYSQL->find(array('FROM'=>'catalog_manufacturers'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idcatalog_manufacturers'].'">'.decode($row['name']).'</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="row">
        <label>Valor: *</label>
        <input type="text" size="10" name="value" class="currency" alt="decimal" id="value" value="<?php echo $value?>"/>
    </div>
    <div class="row">
        <label>Valor antigo: *</label>
        <input type="text" size="10" name="last_value" class="currency" alt="decimal" id="last_value" value="<?php echo $last_value?>"/>
    </div>
    <div class="row">
        <label>Peso: *</label>
        <input type="text" size="10" name="weight" id="weight" value="<?php echo $weight?>"/> (Gramas)
    </div>

    <div class="row">
        <label>Garantia: *</label>
        <input type="text" size="40" name="guarantee" id="guarantee" value="<?php echo $guarantee?>"/>
    </div>
    <div class="row">
        <label>Descrição: </label>
        <div style="width:550px;float:left;">
            <?php
            $oFCKeditor = new FCKeditor('description') ;
            $oFCKeditor->BasePath = '../../core/includes/fckeditor/' ;
            $oFCKeditor->Config['EnterMode'] = 'br';
            $oFCKeditor->ToolbarSet = 'Basic';
            $oFCKeditor->Value = $description;
            $oFCKeditor->Create() ;
            ?>
        </div>
    </div>
    <div class="row">
        <label>Foto: *</label>
        <input type="file" name="upload" id="upload"/>
    </div>
    <div>
        <?php
        if (isset($id)) {
            $find = $MYSQL->find(array('FROM'=>'catalog_products_photos','WHERE'=>'idcatalog_products="'.$id.'"'));
            if (is_array($find)) {
                $largura = $GLOBAL['SMALL_WIDTH_SIZE']+10;
                foreach($find as $image) {
                    echo '<div style="width: ' . $largura . 'px; height:100px; float:left">';
                    echo '<input type="radio" name="thumbnail" value="'.$image['photo'].'" '.((isset($thumbnail) and strlen($thumbnail)>0 and $thumbnail == $image['photo'])?'checked':'').'/>&nbsp;Principal';
                    echo '<br clear="all"/>';
                    echo '<div class="deleteImage" style="width: ' . $largura . 'px; height:100px;;background:url('.SYSTEM_PATH.'tmp/catalog/small/'.$image['photo'].') no-repeat">';
                    echo '<a href="javascript:void(0);" style="float:right;" rel="'.$image['idcatalog_products_photos'].'" title="Excluir esta imagem"><img src="img/icons/others/delete.png" border="0"/></a>';
                    echo '</div></div>';
                }
            }
        }
        ?>
        <br clear="all"/>
    </div>
    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="<?php echo REFERRER ?>">Cancelar</a>
</form>
<script>
    $("#idcatalog_categories").val("<?php echo $idcatalog_categories ?>");
    $(document).ready(function() {
        $('#weight').setMask({mask:'999.999',type:'reverse'});
        $(".deleteImage").fadeTo('fast',0.33);
    });

    $(".deleteImage").mouseover(function() {
        $(this).fadeTo('fast',1);
    });
    $(".deleteImage").mouseout(function() {
        $(this).fadeTo('fast',0.33);
    });
    $(".deleteImage a").click(function() {
        var id = $(this).attr('rel');
        var url = '<?php echo SYSTEM_PATH?>index.php?ajax&module=<? echo $_GET['module']?>&sub=<? echo $_GET['sub']?>&action=delete_photo';
        if (id.length>0) {
            $.get(url,
            {id:id},
            function(data) {
                alert(data);
                window.location = '<?php echo SYSTEM_PATH?>index.php?module=<? echo $_GET['module']?>&sub=<? echo $_GET['sub']?>&action=edit&id=<?php echo $id?>';
            });
        }
        
    });
</script>