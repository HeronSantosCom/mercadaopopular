<?php

if ($_POST) {
    extract($_POST);
    $campos = array("nome","tipo","documento1","telefone","email","fat_endereco","fat_numero","fat_bairro","fat_cidade","fat_estado","fat_pais","fat_cep");
    $error = isRequeired($campos);
    if (isset($encaminhar)) {
        $status = '4';
    }
    if (!$error and isset($id)) {
        $save_array = array();

        $nome = str_replace("'","´",$nome);
        $status = 1;
        $save_array['TABLE'] = 'clientes';
        $save_array['FIELDS'][0] = array(
                'idclientes'=>"{$id}",
                'nome' => "{$nome}",
                'tipo' => "{$tipo}",
                'documento1' => "{$documento1}",
                'documento2' => "{$documento2}",
                'telefone' => "{$telefone}",
                'fax' => "{$fax}",
                'fat_endereco' => "{$fat_endereco}",
                'fat_numero' => "{$fat_numero}",
                'fat_complemento' => "{$fat_complemento}",
                'fat_bairro' => "{$fat_bairro}",
                'fat_cidade' => "{$fat_cidade}",
                'fat_estado' => "{$fat_estado}",
                'fat_pais' => "{$fat_pais}",
                'fat_cep' => "{$fat_cep}",
                'observacoes' => "{$observacoes}",
                'status' =>"{$status}"
        );
        if (isset($senha) and strlen($senha)>0) {
            $senha = md5($senha);
            $save_array['FIELDS'][0]['senha'] = "{$senha}";
        }
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            $clear_form = true;

            echo createMessage('Cliente alterado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cliente', 'error');
        }

    } else {
        echo createMessage('Erro ao salvar cliente', 'error');
    }
}
$find = $MYSQL->find(array('FROM'=>'clientes','WHERE'=>'idclientes="'.$id.'"'));
extract($find[0]);


$header_box = 'Editar';
ob_flush();
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
