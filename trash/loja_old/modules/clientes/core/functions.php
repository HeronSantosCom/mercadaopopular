<?php

function controleCliente($id = null) {
    global $MYSQL;
    if ($_POST) {
        extract($_POST);
        $campos = array("nome","documento1","telefone","email","fat_endereco","fat_numero","fat_bairro","fat_cidade","fat_estado","fat_pais","fat_cep");
        $error = isRequeired($campos);

        if (!$error and isset($id)) {
            $save_array = array();

            $nome = str_replace("'","´",$nome);
            $save_array['TABLE'] = 'clientes';
            $save_array['FIELDS'][0] = array(
                    'nome' => "{$nome}",
                    'documento1' => "{$documento1}",
                    'documento2' => "{$documento2}",
                    'telefone' => "{$telefone}",
                    'fax' => "{$fax}",
                    'fat_endereco' => "{$fat_endereco}",
                    'fat_numero' => "{$fat_numero}",
                    'fat_complemento' => "{$fat_complemento}",
                    'fat_bairro' => "{$fat_bairro}",
                    'fat_cidade' => "{$fat_cidade}",
                    'fat_estado' => "{$fat_estado}",
                    'fat_pais' => "{$fat_pais}",
                    'fat_cep' => "{$fat_cep}",
                    'observacoes' => "{$observacoes}",
                    'create_date' => "{$create_date}"
            );
            if (isset($id)) {
                $save_array['FIELDS'][0]['idclientes'] = $id;
            }
            if (isset($senha) and strlen($senha)>0) {
                $senha = md5($senha);
                $save_array['FIELDS'][0]['senha'] = "{$senha}";
            }
            $exe = $MYSQL->save($save_array);
            if (is_array($exe)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }
    return false;
}
?>
