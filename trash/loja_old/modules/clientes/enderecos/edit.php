<?php

if ($_POST) {
    extract($_POST);
    $campos = array("nome","fat_endereco","fat_numero","fat_bairro","fat_cidade","fat_estado","fat_pais","fat_cep");
    $error = isRequeired($campos);

    if (!$error and isset($id) and isset($idclientes)) {
        $save_array = array();
        $nome = str_replace("'","´",$nome);
        $save_array['TABLE'] = 'clientes_enderecos';
        $save_array['FIELDS'][] = array(

                'idclientes' => "{$idclientes}",
                'nome' => "{$nome}",
                'fat_endereco' => "{$fat_endereco}",
                'fat_numero' => "{$fat_numero}",
                'fat_complemento' => "{$fat_complemento}",
                'fat_bairro' => "{$fat_bairro}",
                'fat_cidade' => "{$fat_cidade}",
                'fat_estado' => "{$fat_estado}",
                'fat_pais' => "{$fat_pais}",
                'fat_cep' => "{$fat_cep}",
                'create_date' => "{$create_date}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            $clear_form = true;

            echo createMessage('Cliente alterado com sucesso', 'success');
            echo "<script>window.location = '".SYSTEM_PATH."/index.php?module=". $_GET['module'] ."&sub=clientes&action=edit&id=".$idclientes."&success';</script>";
        } else {
            echo createMessage('Erro ao salvar cliente', 'error');
        }

    } else {
        echo createMessage('Erro ao salvar cliente', 'error');
    }
}
$find = $MYSQL->find(array('FROM'=>'clientes_enderecos','WHERE'=>'idclientes_enderecos="'.$id.'"'));
extract($find[0]);


$header_box = 'Editar';
ob_flush();
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
