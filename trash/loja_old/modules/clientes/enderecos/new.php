<?php

$header_box = 'Adicionar';
ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array("nome","fat_endereco","fat_numero","fat_bairro","fat_cidade","fat_estado","fat_pais","fat_cep");
    if (isset($encaminhar)) {
        $status = '4';
    }
    $error = isRequeired($campos);
    if (!$error and isset($idclientes)) {
        $create_date = date("Y-m-d");
        $save_array = array();
        $nome = str_replace("'","´",$nome);
        $save_array['TABLE'] = 'clientes_enderecos';
        $save_array['FIELDS'][] = array(

                'idclientes' => "{$idclientes}",

                'nome' => "{$nome}",
                'fat_endereco' => "{$fat_endereco}",
                'fat_numero' => "{$fat_numero}",
                'fat_complemento' => "{$fat_complemento}",
                'fat_bairro' => "{$fat_bairro}",
                'fat_cidade' => "{$fat_cidade}",
                'fat_estado' => "{$fat_estado}",
                'fat_pais' => "{$fat_pais}",
                'fat_cep' => "{$fat_cep}",
                'create_date' => "{$create_date}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo "<script>window.location = '".SYSTEM_PATH."/index.php?module=". $_GET['module'] ."&sub=clientes&action=edit&id=".$idclientes."&success';</script>";
            echo createMessage('Cliente cadastrado com sucesso', 'success');

        } else {
            echo createMessage('Erro ao salvar cliente', 'error');
        }
    }
}

if ($clear_form) {
    $fields = $MYSQL->getColumns('clientes');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
    $fat_pais = 'Brasil';
}

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
