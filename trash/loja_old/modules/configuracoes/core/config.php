﻿<?php

$_MODULE['configuracoes']['name'] = 'Configurações';
$_MODULE['configuracoes']['meios_pagamento']['name'] = 'Meios de Pagamento';
$_MODULE['configuracoes']['frete_envio']['name'] = 'Frete / Envio';
$_MODULE['configuracoes']['mensagens']['name'] = 'Mensagens';
$_MODULE['configuracoes']['conteudo']['name'] = 'Conteúdo';
 
 /* inclui todos os meios de pagametos disponíveis */
$payment_methods = scandir('modules/configuracoes/meios_pagamento');
foreach($payment_methods as $payment_method) {
	@include_once("modules/configuracoes/meios_pagamento/".$payment_method."/core/config.php");
	@include_once("modules/configuracoes/meios_pagamento/".$payment_method."/core/functions.php");
	@include_once("modules/configuracoes/meios_pagamento/".$payment_method."/core/install.php");
}
/* inclui todos os métodos disponíveis */
$shippings = scandir('modules/configuracoes/frete_envio');
foreach($shippings as $shipping) {
	include_once("modules/configuracoes/frete_envio/".$shipping."/core/config.php");
	include_once("modules/configuracoes/frete_envio/".$shipping."/core/functions.php");
	include_once("modules/configuracoes/frete_envio/".$shipping."/core/install.php");
}
unset($submenu);
$submenu[] = array('link'=>'index.php?module=configuracoes&sub=meios_pagamento','name'=>'Meios de Pagamento');
$submenu[] = array('link'=>'index.php?module=configuracoes&sub=frete_envio','name'=>'Frete / Envio');
$submenu[] = array('link'=>'index.php?module=configuracoes&sub=mensagens','name'=>'Mensagens');
$submenu[] = array('link'=>'index.php?module=configuracoes&sub=conteudo','name'=>'Conteúdo');

$_MENU['configuracoes'][] = array('module'=>'configuracoes','link'=>'index.php','name'=>'Configurações','menu'=>$submenu);
?>
