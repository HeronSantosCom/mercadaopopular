<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
$header_box = 'Editar';
ob_flush();
if ($_POST) {
    extract($_POST);
    if (md5($atual)!=$_SESSION['login']['senha']) {
        // senha atual nao confere
        echo createMessage('Senha atual não confere', 'error');
    } else if ($nova!=$confirmacao) {
        echo createMessage('Senha nova não é igual a confirmação', 'error');
    } else {
        $nova = md5($nova);
        $save_array['TABLE'] = 'login';

        $save_array['FIELDS'][] = array(
                'idlogin'=>"{$_SESSION['login']['idlogin']}",
                'senha'=>"{$nova}"
        );

        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo createMessage('Senha atualizada com sucesso', 'success');
            $_SESSION['login']['senha'] = md5($nova);
        } else {
            echo createMessage('Erro ao atualizar senha', 'error');
        }

    }
}
?>
<form method="post" action="">
    <div class="row">
        <label>Senha atual: *</label>
        <input type="password" name="atual" id="atual" />
    </div>
    <div class="row">
        <label>Nova senha: *</label>
        <input type="password" name="nova" id="nova"/>
    </div>
    <div class="row">
        <label>Confirmação: *</label>
        <input type="password" name="confirmacao" id="confirmacao"/>
    </div>
    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php">Voltar</a>
</form>
<?php
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
