<form method="post" action="">

    <div class="row">
        <label>Nome: *</label>
        <input type="text" name="nome" id="nome" size="40" value="<?php echo $nome?>"/>
    </div>
    <div class="row">
        <label>E-mail: *</label>
        <?php if ($action=='edit') { ?>
        <strong><?php echo $email?></strong>
            <?php } else { ?>
        <input type="text" name="email" id="email" size="40" value="<?php echo $email?>"/>
            <?php } ?>
    </div>
    
    <div class="row">
        <label>Grupo: *</label>
        <select name="idgroup" id="idgroup">
            <?php
            $groups = $MYSQL->find(array('FROM'=>'groups'));
            if (is_array($groups)) {
                foreach($groups as $group) {
                    echo '<option value="'.$group['idgroups'].'">'.$group['name'].'</option>';
                }
            }
            ?>
        </select>
    </div>
    
    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="<?php echo SYSTEM_PATH?>index.php?module=<?php echo $module ?>&sub=<?php echo $sub ?>">Voltar</a>
</form>
<script>
    $("#idgroup").val("<?php echo $idgroups ?>");
</script>