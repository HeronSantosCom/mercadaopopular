<?php
if ($_POST) {
    extract($_POST);
    $campos = array('nome','idgroup');
    $error = isRequeired($campos);
    if (!$error and isset($id)) {
        $save_array['TABLE'] = 'login';
        $data_nasc = correctDate($data_nasc);
        $allowedmodules = null;
        if (isset($_POST['allowedmodules']) and is_array($_POST['allowedmodules'])) {
            $allowedmodules = join(":",$_POST['allowedmodules']);
            if ($_SESSION['login']['idlogin'] == $id) {
                $_SESSION['login']['allowedmodules'] = $allowedmodules;
            }
            
        }
        $save_array['FIELDS'][] = array(
                'idlogin'=>"{$id}",
                'nome'=>"{$nome}",
                'idgroups'=>"{$idgroup}",
                'allowedmodules'=>"{$allowedmodules}"
        );

        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo createMessage('Usuário atualizado com sucesso', 'success');

        } else {
            echo createMessage('Erro ao atualizar usuário', 'error');
        }
    }

}
$find = $MYSQL->find(array('FROM'=>'login','WHERE'=>'idlogin="'.$id.'"'));
extract($find[0]);
$header_box = 'Editar';
ob_flush();
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
