<?php

$header_box = 'Adicionar';
ob_flush();

$clear_form = true;
if ($_POST) {
    $clear_form = false;
    extract($_POST);
    $campos = array('nome','idgroup');
    $error = isRequeired($campos);
    if (!$error) {
        $save_array['TABLE'] = 'login';
        
        $allowedmodules = null;
        if (isset($_POST['allowedmodules']) and is_array($_POST['allowedmodules'])) {
            $allowedmodules = join(":",$_POST['allowedmodules']);
        }
        $save_array['FIELDS'][0] = array(
                'nome'=>"{$nome}",
                'email'=>"{$email}",
                'idgroups'=>"{$idgroup}",
                'allowedmodules'=>"{$allowedmodules}"
        );
        if ($action=='add') {
            $save_array['FIELDS'][0]['senha'] = md5($senha);

        }
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            $clear_form = true;
            echo createMessage('Usuário salvo com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar usuario', 'error');
        }
    }
}

if ($clear_form) {
    $fields = $MYSQL->getColumns('login');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
}

include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);

?>
