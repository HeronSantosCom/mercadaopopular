<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

$header_box = 'Editar';
$sql = $MYSQL->find(array('FROM'=>'login','WHERE'=>'idlogin="'.$id.'"'));
if (is_array($sql[0])) {
    $senha = $sql[0]['senha'];
    $nome = $sql[0]['nome'];
}
ob_flush();
if ($_POST) {
    extract($_POST);
    if (isset($id) and strlen($id)>0) {

        if ($nova!=$confirmacao) {
            echo createMessage('Senha nova não é igual a confirmação', 'error');
        } else {
            $nova = md5($nova);
            $save_array['TABLE'] = 'contatos';

            $save_array['FIELDS'][] = array(
                    'idlogin'=>"{$id}",
                    'senha'=>"{$nova}"
            );

            $exe = $MYSQL->save($save_array);
            if (is_array($exe)) {
                echo createMessage('Senha atualizada com sucesso', 'success');

            } else {
                echo createMessage('Erro ao atualizar senha', 'error');
            }
        }
        
    }
}
?>
<form method="post" action="">
    <div class="row">
        <label>Usuário: *</label>
        <span class="field"><?php echo $nome ?></span>
    </div>
    <div class="row">
        <label>Nova senha: *</label>
        <input type="password" name="nova" id="nova"/>
    </div>
    <div class="row">
        <label>Confirmação: *</label>
        <input type="password" name="confirmacao" id="confirmacao"/>
    </div>
    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="/index.php">Voltar</a>
</form>
<?php
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
