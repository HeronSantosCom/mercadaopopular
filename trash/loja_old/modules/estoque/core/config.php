<?php
$_MODULE['estoque']['name'] = 'Estoque';
$_MODULE['estoque']['depositos']['name'] = 'CDD / Depósitos';
$_MODULE['estoque']['inventario']['name'] = 'Inventário';

unset($submenu);
$submenu[] = array('link'=>'index.php?module=estoque&sub=depositos','name'=>'CDD / Depósitos');
$submenu[] = array('link'=>'index.php?module=estoque&sub=inventario','name'=>'Inventário');

$_RELATORIOS_MENU['estoque'][] = array('link'=>'index.php?module=estoque&sub=estoque','name'=>'Inventário');

$_MENU['estoque'][] = array('module'=>'estoque','link'=>'index.php','name'=>'Estoque','menu'=>$submenu);
?>
