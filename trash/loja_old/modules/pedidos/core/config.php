<?php
$_MODULE['pedidos']['name'] = 'Pedidos';
$_MODULE['pedidos']['pedidos']['name'] = 'Pedidos';
$_MODULE['pedidos']['status']['name'] = 'Status';

unset($submenu);

$submenu[] = array('link'=>'index.php?module=pedidos&sub=pedidos&action=new','name'=>'Criar Pedido');
$submenu[] = array('link'=>'index.php?module=pedidos&sub=pedidos','name'=>'Todos os Pedidos');
$submenu[] = array('link'=>'index.php?module=pedidos&sub=status','name'=>'Status');

$_RELATORIOS_MENU['pedidos'][] = array('link'=>'index.php?module=pedidos&sub=pedidos','name'=>'Pedidos');

$_MENU['pedidos'][] = array('module'=>'pedidos','link'=>'index.php','name'=>'Pedidos','menu'=>$submenu);
?>
