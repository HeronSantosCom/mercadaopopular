<?php

if (isset($check_install)) {
    //$MYSQL->execSQL("DROP TABLE pedidos");
    if (!$MYSQL->tableExists('pedidos')) {
        $MYSQL->execSql('CREATE TABLE `pedidos` (
  `idpedidos` INT(11) NOT NULL AUTO_INCREMENT,
	`idclientes` INT(11) NOT NULL,
        `idpedidos_status` INT(11) NOT NULL,
        `create_date` date DEFAULT NULL,
	`forma_pagamento` VARCHAR(250) NULL DEFAULT NULL,
        `forma_pagamento_infos` TEXT NULL DEFAULT NULL,
	`observacoes` TEXT NULL,
	PRIMARY KEY (`idpedidos`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');
    }
    //$MYSQL->execSQL("DROP TABLE itens_pedidos");
    if (!$MYSQL->tableExists('itens_pedidos')) {
        $MYSQL->execSql('CREATE TABLE `itens_pedidos` (
  `iditens_pedidos` INT(11) NOT NULL AUTO_INCREMENT,
  `idpedidos` INT(11) NOT NULL,
  `produto_id` INT(11) NOT NULL,
	`produto_nome` VARCHAR(250) NULL DEFAULT NULL,
        `produto_valor` VARCHAR(250) NULL DEFAULT NULL,
        `produto_qnt` VARCHAR(250) NULL DEFAULT NULL,
	PRIMARY KEY (`iditens_pedidos`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');
    }
    //$MYSQL->execSQL("DROP TABLE pedidos_status");
    if (!$MYSQL->tableExists('pedidos_status')) {
        $MYSQL->execSql('CREATE TABLE `pedidos_status` (
  `idpedidos_status` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(250) NULL DEFAULT NULL,
	PRIMARY KEY (`idpedidos_status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');
    }

}
?>
