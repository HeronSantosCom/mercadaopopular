<?php
$header_box = 'Editar';
ob_flush();
$dosearchid = true;
if ($_POST) {
    extract($_POST);
    $campos = array('idclientes','idproposta_status',);
    $error = isRequeired($campos);
    if (!$error) {
        $save_array = array();
        $save_array['TABLE'] = 'pedidos';
        $save_array['FIELDS'][] = array(
                'idpedidos'=>"{$id}",
                'idpedidos_status' => "{$idpedidos_status}",
                'observacoes'=>"{$observacoes}"
        );
        $exe = $MYSQL->save($save_array);
        
        echo createMessage('Pedido alterado com sucesso', 'success');
    }
}
/*
         *  remove tudo de `trunks_route_prefix` que o `idtrunks_providers`= $id
*/
$find = $MYSQL->find(array('FROM'=>'pedidos','FIELDS'=>'pedidos.*, date_format(create_date,"%d/%m/%Y") as create_date','WHERE'=>'idpedidos="'.$id.'"'));
extract($find[0]);

$find_cart = $MYSQL->find(array('FROM'=>'itens_pedidos','WHERE'=>'idpedidos="'.$id.'"'));

if (isset($find_cart[0])) {
    foreach($find_cart as $cart) {
        extract($cart);
        $_SESSION['webcart'][$produto_id] = array(
                'grid_id'=>"{$produto_id}",
                'produto_id'=>"{$produto_id}",
                'produto_nome'=>"{$produto_nome}",
                'produto_valor'=>"{$produto_valor}",
                'produto_qnt'=>"{$produto_qnt}");
    }
}

include('form_detailed.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
