<?php
unset($_SESSION['webcart']);
$payment_methods = $SUPER['payment_methods'];
$link_products = SYSTEM_PATH.'index.php?ajax&module='.$_GET['module'].'&sub='.$_GET['sub'].'&action=products_list';
$list_products = SYSTEM_PATH.'index.php?ajax&module='.$_GET['module'].'&sub='.$_GET['sub'].'&action=list_products';

//unset($_SESSION['proposal']);
?>
<form method="post" action="">

    <div class="row">
        <label>Cliente: </label>
        <select name="idclientes" id="idclientes">
            <option value=""></option>
            <?php
            $fields = $MYSQL->find(array('FROM'=>'clientes','ORDER'=>'nome'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idclientes'].'">'.decode($row['nome']).'</option>';
                }
            }
            ?>
        </select>
    </div>


    <div class="row">
        <label>Status: *</label>
        <select name="idpedidos_status" id="idpedidos_status">
            <option value=""></option>
            <?php
            $fields = $MYSQL->find(array('FROM'=>'pedidos_status'));
            if (is_array($fields)) {
                foreach($fields as $row) {
                    echo '<option value="'.$row['idpedidos_status'].'">'.decode($row['name']).'</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="row subheader">
        <strong>Produtos / Serviços * </strong> <?php if ($action=='new') {
            echo '<input type="button" onclick="showAddCart();" class="button small strong" value="Adicionar"> <input type="button" onclick="delCart();" class="button small strong" value="Excluir">';
        } ?>

    </div>
    <div id="addcart" class="defaultbox" style="display:none">
        <div class="row cart">
            <label>Categoria: *</label>
            <select name="categoria" id="categoria">
                <option value=""></option>
                <?php
                $fields = $MYSQL->find(array('FROM'=>'catalog_categories','WHERE'=>'idcatalog_categories_main="" or idcatalog_categories_main is null'));

                if (is_array($fields)) {
                    foreach($fields as $row) {
                        echo '<option value="'.$row['idcatalog_categories'].'">'.$row['name'].'</option>';
                        $fields1 = $MYSQL->find(array('FROM'=>'catalog_categories','WHERE'=>'idcatalog_categories_main="'.$row['idcatalog_categories'].'"'));
                        if (is_array($fields1)) {
                            foreach($fields1 as $row1) {
                                echo '<option value="'.$row1['idcatalog_categories'].'"> - '.$row1['name'].'</option>';
                            }
                        }
                    }
                }
                ?>
            </select>
        </div>
        <div class="row cart">
            <label>Produto: *</label>
            <select name="idproducts" id="idproducts" style="width:200px;">
            </select>
        </div>
        <div class="row cart">
            <label>Quantidade: *</label>
            <input type="text" size="10" name="qnt" id="qnt" value="1" alt="999" />
        </div>
        <div class="row cart">
            <label>Valor: *</label>
            <input type="text" size="10" name="valor" alt="decimal" id="valor" value="" />
        </div>
        <input type="button" style="float:right;" onclick="addCart();" class="button small strong" value="Salvar">
    </div>
    <div id="products_list">

    </div>
    <div id="products_list1">
    </div>
    <div class="row">
        <label>Formas de pagto: *</label>
        <select name="forma_pagamento" id="forma_pagamento" class="formas_pagto">
            <option value="0"></option>
            <?php
            if (isset($payment_methods) and is_array($payment_methods)) {
                foreach(array_keys($payment_methods) as $pm) {
                    echo '<option value="'.$payment_methods.'">'.$payment_methods[$pm].'</option>';
                }
            }

            ?>
        </select>

    </div>
    <div class="row">
        <label>Observações: </label>
        <textarea name="observacoes" id="observacoes" cols="50" rows="10"><?php echo $observacoes?></textarea>
    </div>

    <br clear="all"/>
    <input type="submit" value="Salvar" class="button medium" /> ou <a href="<?php echo REFERRER ?>">Cancelar</a>
</form>
<script>
    $(document).ready(function() {

        openAjax('<?php echo $link_products ?>','products_list');


        $("#products_list").ajaxComplete(function() {
            $(".order_arrow").hide();
        });

    });
    function delCart() {
        agree = confirm("Deseja excluir os ítens selecionados?");
        if (agree) {
            var deleteids = new Array();
            $("#products_list").find('input.grid_checkbox:checked').each(function() {
                var this_val = $(this).val();
                deleteids.push(this_val);
            });
            deleteids = deleteids.join(":");

            openAjax('<?php echo $link_products ?>&delete='+deleteids,'products_list');
            $("#products_list").ajaxComplete(function() {
                $(".order_arrow").hide();
            });
        }

    }


    function showAddCart() {
        $("#addcart").toggle();
    }
    function addCart() {
        var idproduct = $("#idproducts").val();

        var nome = $("#idproducts option:selected").text();
        var qnt = $("#qnt").val();
        var valor = $("#valor").val();
        var tipo = $("#tipo").val();

        openAjax('<?php echo $link_products ?>&idproduct='+idproduct+'&nome='+nome+'&qnt='+qnt+'&valor='+valor+'&tipo='+tipo,'products_list');

    }
    $("#categoria").change(function() {
        var cat = $(this).val();
        $("#idproducts").empty().append('<option>carregando...</option>');
        openAjax('<?php echo $list_products ?>&id='+cat,'products_list1');
    });

    $("#idproducts").change(function() {
        var valor = $("#idproducts").find("option:selected").attr('price');
        $("#valor").val(valor);
    });

    // only numbers

</script>
<style>
    #addcart { position: absolute; z-index: 1000; margin: 0 auto; width: 400px;}
    #products_list2 .loading {display: none; }
    .loading { display:none;}
</style>
