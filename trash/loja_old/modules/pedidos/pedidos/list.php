<?php
unset( $_SESSION['webcart']);

$find_array = array(
        'FROM'=>'pedidos',
        'JOIN'=>'LEFT JOIN clientes ON (pedidos.idclientes = clientes.idclientes) LEFT JOIN pedidos_status ON (pedidos.idpedidos_status=pedidos_status.idpedidos_status)',
        'FIELDS'=>'pedidos.*, date_format(pedidos.create_date,"%d/%m/%Y") as create_date, pedidos_status.name as nome_status,clientes.nome as cliente'

);

$sql = $MYSQL->find($find_array,'grid');
$array['header'] = array('idpedidos'=>'Nº','cliente'=>'Cliente','create_date'=> 'Data Inclusão','nome_status'=>'Status');
$array['body'] = $sql;
echo createGrid($array);

?>
<style>
    .number {
        width: 80px;
    }
    .idpedidos {
        width: 50px;
    }
    .create_date {
        width: 120px;
    }
</style>

<script>

    $("input[value='Excluir']").remove();

</script>