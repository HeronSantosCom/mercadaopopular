<?php
$header_box = 'Adicionar';
ob_flush();

if ($_POST) {
    extract($_POST);
    $campos = array('idclientes','idproposta_status',);
    $error = isRequeired($campos);
    if (!$error) {
        if (isset($_SESSION['webcart']) && is_array($_SESSION['webcart'])) {

            $create_date = date("Y-m-d") ;
            $forma_pagamento_infos = getPagamento($forma_pagamento);
            $save_array = array();
            $save_array['TABLE'] = 'pedidos';
            $save_array['FIELDS'][] = array(
                    'idclientes'=>"{$idclientes}",
                    'idpedidos_status' => "{$idpedidos_status}",
                    'create_date' => "{$create_date}",
                    'forma_pagamento' => "{$forma_pagamento}",
                    'forma_pagamento_infos' => "{$forma_pagamento_infos}",
                    'observacoes'=>"{$observacoes}"
            );
            $exe = $MYSQL->save($save_array);
            $idpedido = $exe[0];
            if ($idpedido>0) {


                //cadastra os produtos
                if (is_array($_SESSION['webcart'])) {
                    foreach($_SESSION['webcart'] as $product) {
                        $save_array = null;
                        $save_array['TABLE'] = 'itens_pedidos';
                        extract($product);

                        $save_array['FIELDS'][] = array(
                                'idpedidos'=>"{$idpedido}",
                                'produto_id' => "{$produto_id}",
                                'produto_nome' => "{$produto_nome}",
                                'produto_qnt' => "{$produto_qnt}",
                                'produto_valor' => "{$produto_valor}"
                        );

                        $exe = $MYSQL->save($save_array);
                    }
                }

                //echo "<script>window.location = '/index.php?module=". $_GET['module'] ."&sub=". $_GET['sub'] ."&action=edit&id=". $_GET['id'] ."&success';</script>";


                echo createMessage('Pedido adicionado com sucesso', 'success');

            } else {
                echo createMessage('Erro ao salvar cadastro', 'error');
            }
        } else {
            echo createMessage('Pedido precisa ter ao menos um produto / serviço', 'error');
        }
    }
}
if ($_POST) {
    extract($_POST);
} else {
    $fields = $MYSQL->getColumns('pedidos');
    if (is_array($fields)) {
        foreach($fields as $field) {
            $$field = null;
        }
    }
}


include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
