<?php
$header_box = 'Editar';
ob_flush();
if ($_POST) {
    extract($_POST);
    $campos = array('name');
    $error = isRequeired($campos);
    if (!$error and isset($id)) {
        $save_array = array();
        $save_array['TABLE'] = 'pedidos_status';
        $save_array['FIELDS'][] = array(
                'idpedidos_status'=>"{$id}",
                'name'=>"{$name}"
        );
        $exe = $MYSQL->save($save_array);
        if (is_array($exe)) {
            echo createMessage('Status alterado com sucesso', 'success');
        } else {
            echo createMessage('Erro ao salvar cadastro', 'error');
        }
    }
}
$find = $MYSQL->find(array('FROM'=>'pedidos_status','WHERE'=>'idpedidos_status="'.$id.'"'));
extract($find[0]);
include('form.php');
$_content = ob_get_contents();
ob_clean();
echo createBox($header_box, $_content);
?>
