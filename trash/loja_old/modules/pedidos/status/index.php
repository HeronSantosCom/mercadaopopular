<?php
if (isset($ids)) {
    $ids = explode(',',$ids);
    $field = 'idpedidos_status';
    $data = null;
    if (is_array($ids)) {
        foreach($ids as $id) {
            $where[] = '`'.$field.'`="'.$id.'"';
        }
        if (isset($where) and is_array($where)) {
            $data['WHERE'] = join(" or ",$where);
        }
    } else {
        $data['WHERE'] = '`'.$field.'`="'.$ids.'"';
    }

    
    $data['TABLE'] = 'pedidos_status';
    $delete = $MYSQL->remove($data);
    if ($delete > 0) {
        echo createMessage($delete.' Status(s) excluído(s) com sucesso', 'success');
    } else {
        echo createMessage('Erro ao excluir cadastro', 'error');
    }
    ////do exclude
    // do message

}
if (isset($success)) {
     echo createMessage('Status adicionado com sucesso', 'success');
}
?>
<div id="ajaxbox"></div>
<script>
    function filter(url) {
        if (url == undefined || url == '' || url == false) {
            url = '<?php echo SYSTEM_PATH ?>index.php?ajax&module=<?=$_GET['module']?>&sub=<?=$_GET['sub']?>&action=list';
        }
        master_filter(url,'ajaxbox');
    }
    filter('');
</script>