<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo SYSTEM_PATH ?>/template/<?php echo $_THEME ?>/" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Loja Virtual - Small Business -<?php echo $_SESSION['login']['nome'] ?><<?php echo $_SESSION['login']['email'] ?>></title>
<link type="text/css" rel="stylesheet" href="css/base.css" />
<link type="text/css" rel="stylesheet" href="css/grid.css" />
<link type="text/css" rel="stylesheet" href="css/visualize.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.7.custom.css">
<script type="text/javascript" src="js/jquery-1.4.all.js"></script>
<script type="text/javascript" src="js/jquery.meiomask.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="js/ui.datepicker-pt-BR.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<script type="text/javascript" src="js/visualize.jQuery.js"></script>
<script type="text/javascript" src="js/jquery.uploadify-v2.1.0/example/scripts/swfobject.js"></script>
<script type="text/javascript" src="js/jquery.uploadify-v2.1.0/example/scripts/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/default.js"></script>
<script>
            var module = "<?php  if(isset($_GET['module'])) {
    echo $_GET['module'];
} ?>";
    var msub = "<?php  if(isset($_GET['sub'])) {
    echo $_GET['sub'];
} ?>";
    $(document).ready(function() {
        $.datepicker.setDefaults($.datepicker.regional['']);
        $(".datepicker").datepicker($.datepicker.regional['pt-BR']);
        $('.datepicker').datepicker();
        $('.datepicker_img').live('click',function() {
            $(this).parent().find('input.datepicker').focus();
        });
        $('.datepicker').parent().append('<img src="img/icons/others/date.png" class="datepicker_img" align="absmiddle" border="0"/>');
        $('.toggle').trigger('click');
        $(".removeHeader .box-header").remove();
        $(".removeCheckbox .checkbox").remove();
        $(".removeFooter .tablefoot").remove();
        $(".removeOrderning .order_arrow").remove();
		$( "#optionstabs" ).tabs();
		
    });
    (function($){
        // call setMask function on the document.ready event
        $(function(){
            $('input:text').setMask();
        }
    );
    })(jQuery);
        </script>
</head>
<body>
<div id="header">
  <div class="header-top tr">
    <p>Bem vindo <strong><?php echo $_SESSION['login']['nome'] ?></strong>&nbsp;
      <!--seu último login foi <?php echo $_SESSION['login']['last_login'] ?> | Você tem <a href="/index.php" id="opener" class="strong">1 nova mensagem</a> |-->
      <a href="<?php echo SYSTEM_PATH ?>index.php?module=default&sub=profile">Meu Perfil</a> &nbsp;| &nbsp;<span class="nopermission"><a href="<?php echo SYSTEM_PATH ?>index.php?module=default&action=configurations" >Configurações</a> </span>
      <?php if ($_SESSION['login']['nivel']=='1') { ?>
      &nbsp;|&nbsp; <a href="<?php echo SYSTEM_PATH ?>index.php?module=default&sub=users">Usuários</a> &nbsp;|&nbsp; <a href="<?php echo SYSTEM_PATH ?>index.php?module=default&sub=groups">Grupos</a>
      <?php } ?>
      &nbsp;|&nbsp; <a href="<?php echo SYSTEM_PATH ?>?logoff">Logoff</a></p>
  </div>
  <div class="header-middle">
    <!-- Start Nav 
                <ul id="nav" class="fr ">
                    <li class="help" style="display:none;"><a class="help" href="javascript:void(0);">Ajuda</a>
                        <ul>
                            <li><a href="/index.php?module=default&action=procedures">Procedimentos</a></li>
                            <li><a href="/index.php?module=default&action=docs">Documentação</a></li>
                            <li><a href="/index.php?module=default&action=faq">FAQ</a></li>
                        </ul>
                    </li>
                    <li class="settings"><a class="settings" href="/index.php?module=default&action=configurations">Configurações</a>
                    </li>
                    <?php if ($_SESSION['login']['nivel']=='1') { ?>
                    <li class="users"><a class="users" href="javascript:void(0);">Usuários</a>
                        <ul>
                            <li><a href="/index.php?module=default&sub=users">Usuários</a></li>
                            <li><a href="/index.php?module=default&sub=groups">Grupos</a></li>
                        </ul>
                    </li>
                        <?php } ?>
                    <li class="content" style="display:none;"><a class="content" href="/index.php">Content</a>
                        <ul>
                            <li><a href="/index.php">Manage Pages  &raquo;</a>
                                <ul>
                                    <li><a href="/index.php">Add Page  &raquo;</a>
                                        <ul>
                                            <li><a href="/index.php">HTML Page</a></li>
                                            <li><a href="/index.php">Widget Page</a></li>
                                            <li><a href="/index.php">Custom Page</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/index.php">Edit Page</a></li>
                                    <li><a href="/index.php">Delete Page</a> </li>
                                </ul>
                            </li>
                            <li><a href="/index.php">Page Statstics</a></li>
                            <li><a href="/index.php">Page Categories</a></li>
                            <li><a href="/index.php">Page Tags</a></li>
                        </ul>
                    </li>
                    <li class="conversation" style="display:none"><a class="conversation" href="/index.php">Conversas</a></li>
                    <li class="dashboard"><a class="dashboard" href="/index.php">Dashboard</a></li>
                </ul>
                 Start Logo -->
    <a href="<?php echo SYSTEM_PATH ?>index.php"><img id="logo" src="img/logo.png" alt="Loja Virtual" height="35"/> </a>
    <!-- End Logo -->
    <br class="cl" />
  </div>
  <div class="header-bottom">
    <!-- Start Breadcrumbs -->
    <?php
                breadcrumbs();
                ?>
    <!-- End Breadcrumbs -->
  </div>
</div>
<div id="page-wrapper">
  <div class="page">
    <!-- Start Sidebar -->
    <div id="sidebar">
      <!-- Start Live Search  -->
      <form class="searchform" action="#" style="display:none;">
        <input id="livesearch" type="text" onblur="if (this.value == '') {this.value = 'Live Search...';}" onfocus="if (this.value == 'Live Search...') {this.value = '';}" value="Live Search..." class="searchfield" />
        <input type="button" value="Go" class="searchbutton" />
      </form>
      <!-- End Live Search  -->
      <?php
                    loadSidebarMenu();
                    ?>
      <!-- Start Statistics Area  -->
      <span class="ul-header">Estatísticas</span>
      <?php
                    loadSidebarStatistics();
                    ?>
      <!-- End Statistics Area  -->
    </div>
    <!-- End Sidebar  -->
    <!-- Star Page Content  -->
    <div id="page-content">
      <div class="container_12">
        <?php
                        loadContent();
                        ?>
      </div>
      <br class="cl" />
    </div>
    <!-- End Page Wrapper -->
  </div>
  <!-- End Page Content  -->
</div>
</div>
<!-- Start Footer -->
<div class="footer"> Copyright ©2010, a <a href="http://www.softpixel.com.br" target="_blank">SoftPixel</a>. </div>
<!-- End Footer -->
</body>
<?php
    if (isset($_SESSION['login']['allowedmodules']) and strlen($_SESSION['login']['allowedmodules'])>0 and !preg_match("#default#",$_SESSION['login']['allowedmodules'])) {
        echo '<script>';
        echo '$("#nav").remove();';
        echo '$(".nopermission").remove();';
        echo '</script>';
    }
    ?>
	<script>
	$( "#optionstabs ul" ).removeClass("*");
	</script>
</html>
