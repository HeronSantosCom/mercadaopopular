<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <base href="<?php echo SYSTEM_PATH ?>template/<?php echo $_THEME ?>/" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
        <link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
        <link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
        <title>Script de Instalação</title>
        <script type="text/javascript" src="js/jquery-1.4.all.js"></script>
        <script type="text/javascript" src="js/jquery.meiomask.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.2.custom.min.js"></script>
        <script type="text/javascript" src="js/ui.datepicker-pt-BR.js"></script>

        <script type="text/javascript" src="js/excanvas.js"></script>
        <script type="text/javascript" src="js/visualize.jQuery.js"></script>

        <script type="text/javascript" src="js/jquery.uploadify-v2.1.0/example/scripts/swfobject.js"></script>
        <script type="text/javascript" src="js/jquery.uploadify-v2.1.0/example/scripts/jquery.uploadify.v2.1.0.min.js"></script>

        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="js/default.js"></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/custom.css" />

        <script>
            var module = "<?php  if(isset($_GET['module'])) {
    echo $_GET['module'];
} ?>";
            var msub = "<?php  if(isset($_GET['sub'])) {
    echo $_GET['sub'];
} ?>";
            $(document).ready(function() {
                $.datepicker.setDefaults($.datepicker.regional['']);
                $(".datepicker").datepicker($.datepicker.regional['pt-BR']);
                $('.datepicker').datepicker();
                $('.datepicker_img').live('click',function() {
                    $(this).parent().find('input.datepicker').focus();
                });
                $('.datepicker').parent().append('<img src="img/icons/others/date.png" class="datepicker_img" align="absmiddle" border="0"/>');
                $('.toggle').trigger('click');
            });
            (function($){
                // call setMask function on the document.ready event
                $(function(){
                    $('input:text').setMask();
                }
            );
            })(jQuery);
        
            var module = "<?php  if(isset($_GET['module'])) { echo $_GET['module']; } ?>";
            var msub = "<?php  if(isset($_GET['sub'])) { echo $_GET['sub']; } ?>";
            var SYSTEM_PATH = "<?php echo SYSTEM_PATH ?>";
        </script>
    </head>
    <body>
	    <div id="page-content" style="width:790px;margin:0 auto;">
                    <div class="container_12">

<div class="grid_12">
<div class="box-header">Script de Instalação</div>
<div class="box">
	<p>Olá, seja bem vindo ao instalador. Insira a seguir as informações solicitadas para prosseguir:</p>
	<form method="post" action="">
		<input type="hidden" name="install" id="install" value="1" />
		<div style="border-bottom:1px dotted #ddd;padding:5px;"><strong>Informações básicas</strong></div>
          <div class="row">
            <label>URL Completa:</label>
            <input name="url" id="url" type="text" size="40" value="http://<?php echo $_SERVER['SERVER_NAME']?>/"/>
          </div>
          <div style="border-bottom:1px dotted #ddd;padding:5px;"><strong>Informações do banco de dados</strong></div>
          <div class="row">
            <label>Host:</label>
            <input id="mysql_host" name="mysql_host" type="text" size="40" />
          </div>
          <div class="row">
            <label>Usuário:</label>
            <input id="mysql_user" name="mysql_user" type="text" size="40"/>
          </div>
          <div class="row">
            <label>Senha:</label>
            <input id="mysql_pass" name="mysql_pass" type="text" size="40"/>
          </div>
          <div class="row">
            <label>Banco de dados:</label>
            <input id="mysql_db" name="mysql_db" type="text" size="40"/>
          </div>
          <input type="submit" value="Salvar" class="button small" />
          </form>
</div>
</div>
                    </div>
                    <br class="cl" />
                </div>

</body>
</html>