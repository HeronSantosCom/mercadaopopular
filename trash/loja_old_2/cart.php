<?php
include("config.php");
include("header.php");
$erro = false;
if (isset($action)) {
    $erro = "Produto não especificado!";
    if (isset($product)) {
        $erro = false;
        switch ($action) {
            case "add":
                $product = (isset($product) ? $product : false);
                if (!cartInsert($loja, $product)) {
                    $erro = "Não foi possível inserir o produto, tente novamente mas tarde!";
                }
                break;
            case "increment":
                $product = (isset($product) ? $product : false);
                if (!cartIncrement($loja, $product)) {
                    $erro = "Não foi possível incrementar o produto, tente novamente mas tarde!";
                }
                break;
            case "decrement":
                $product = (isset($product) ? $product : false);
                if (!cartDecrement($loja, $product)) {
                    $erro = "Não foi possível remover o produto, tente novamente mas tarde!";
                }
                break;
            case "remove":
                $product = (isset($product) ? $product : false);
                if (!cartDecrement($loja, $product)) {
                    $erro = "Não foi possível remover o produto, tente novamente mas tarde!";
                }
                break;
        }
    } else {
        $erro = false;
        switch ($action) {
            case "cep":
                $cep = (isset($cep) ? $cep : false);
                $cepdigito = (isset($cepdigito) ? $cepdigito : false);
                $ceptipo = (isset($ceptipo) ? $ceptipo : false);
                if (!cepSet($loja, $cep, $cepdigito, $ceptipo)) {
                    $erro = "Não foi possível calcular o valor do frete, tente novamente mas tarde!";
                }
                break;
        }
    }
}
?>
<div id="content">
    <h2>Minha sacola de compras</h2>
    <br clear="all"/>
    <?php
    $list = cartList($loja);
    $cep = cepGet($loja);
    $print[] = '<table  id="cart" cellpadding="0" cellpadding="0" border="0" width="100%">';
    $print[] = '<thead>';
    $print[] = '<tr>';
    $print[] = '<td width="60%">PRODUTO</td>';
    $print[] = '<td width="10%">QNT</td>';
    $print[] = '<td width="15%">VALOR UNITÁRIO</td>';
    $print[] = '<td width="15%">VALOR TOTAL</td>';
    $print[] = '</tr>';
    $print[] = '</thead>';
    $print[] = '<tbody>';
    if ($erro) {
        $print[] = '<tr><td colspan="4" height="100" align="center">' . $erro . '</td></tr>';
    }
    $total = (float) $cep["valor"];
    $qnt = 0;
    if ($list) {
        foreach ($list as $product => $info) {
            $subtotal = (float) ($info["value"] * $info["qnt"]);
            $print[] = '<tr>';
            $print[] = '<td>' . $info["name"] . '</td>';
            $print[] = '<td><a href="cart.php?loja=' . $loja . '&product=' . $product . '&action=increment">+</a> ' . $info["qnt"] . ' <a href="cart.php?loja=' . $loja . '&product=' . $product . '&action=decrement">-</a></td>';
            $print[] = '<td>R$ ' . number_format((float) $info["value"], 2, ",", ".") . '</td>';
            $print[] = '<td>R$ ' . number_format($subtotal, 2, ",", ".") . '</td>';
            $print[] = '</tr>';
            $total += $subtotal;
            $qnt += $info["qnt"];
        }
    } else {
        $print[] = '<tr><td colspan="4" height="100" align="center">Seu carrinho está vazio</td></tr>';
    }
    $print[] = '</tbody>';
    $print[] = '<tfoot>';
    $print[] = '<tr>';
    $print[] = '<td colspan="2">';
    $print[] = 'Calcule o frete <input type="text" size="10" maxlength="5" name="cep" value="' . (isset($cep["cep"]) ? $cep["cep"] : null) . '" /> - <input type="text" size="3" maxlength="3" name="cepdigito" value="' . (isset($cep["cepdigito"]) ? $cep["cepdigito"] : null) . '" /> ';
    $print[] = '<select name="ceptipo">';
    $print[] = '<option value="pac" ' . ($cep["tipo"] != "sedex" ? 'selected' : null) . '>Encomenda Normal (PAC)</option>';
    $print[] = '<option value="sedex" ' . ($cep["tipo"] == "sedex" ? 'selected' : null) . '>Sedex</option>';
    $print[] = '</select>';
    $print[] = ' <a href="javascript:void(0);" onclick="GetCEP()">buscar</a>';
    $print[] = '<br />';
    if (strlen($cep["localidade"]) > 0) {
        $print[] = 'Cidade de entrega: <b>' . $cep["localidade"] . '</b>.';
    }
    $print[] = '</td>';
    //$print[] = '<td>&nbsp;</td>';
    $print[] = '<td><strong>Frete</strong></td>';
    $print[] = '<td>';
    $print[] = 'R$ ' . number_format((float) $cep["valor"], 2, ",", ".");
    $print[] = '</td>';
    $print[] = '</tr>';
    $print[] = '<tr>';
    $print[] = '<td>&nbsp;</td>';
    $print[] = '<td>&nbsp;</td>';
    $print[] = '<td class="backred"><strong>Total</strong></td>';
    $print[] = '<td class="backred">R$ ' . number_format((float) $total, 2, ",", ".") . '</td>';
    $print[] = '</tr>';
    $print[] = '</tfoot>';
    $print[] = '</table>';
    print join("\n", $print);
    unset($print);
    ?>
    <br clear="all"/>
    <div class="box50">
        <p><a href="index.php?loja=<?php echo $loja ?>" class="button">Escolher mais produtos</a></p>
    </div>
    <div class="box50 textright">
        <p><a href="checkout.php?loja=<?php echo $loja ?>" class="button font18">Fechar pedido</a></p>
    </div>
    <br clear="all"/>
</div>
<script type="text/javascript">

    function GetCEP() {
        var cep = $("input[name=cep]").val();
        var cepdigito = $("input[name=cepdigito]").val();
        var ceptipo = $("select[name=ceptipo]").val();
        var cepcompleto = cep + cepdigito;
        if (cepcompleto.length == 8) {
            return window.open("cart.php?loja=<?php echo $loja; ?>&action=cep&cep=" + cep + "&cepdigito=" + cepdigito + "&ceptipo=" + ceptipo, "_parent");;
        }
        alert("CEP informado incorreto: " + cepcompleto);
        return false;
    }

</script>
<?php
    include_once("footer.php");
?>