<?php
include("config.php");
include("header.php");
$erro = false;
$logado = $endereco = true;
if (isset($action)) {
    switch ($action) {
        
    }
}
?>
<div id="content">
    <h2>Meu pedido</h2>
    <br clear="all"/>
    <?php
    $list = cartList($loja);
    $cep = cepGet($loja);
    $print[] = '<table  id="cart" cellpadding="0" cellpadding="0" border="0" width="100%">';
    $print[] = '<thead>';
    $print[] = '<tr>';
    $print[] = '<td width="60%">PRODUTO</td>';
    $print[] = '<td width="15%">VALOR UNITÁRIO</td>';
    $print[] = '<td width="15%">VALOR TOTAL</td>';
    $print[] = '</tr>';
    $print[] = '</thead>';
    $print[] = '<tbody>';
    if ($erro) {
        $print[] = '<tr><td colspan="4" height="100" align="center">' . $erro . '</td></tr>';
    }
    $total = (float) $cep["valor"];
    $qnt = 0;
    if ($list) {
        foreach ($list as $product => $info) {
            $subtotal = (float) ($info["value"] * $info["qnt"]);
            $print[] = '<tr>';
            $print[] = '<td>' . $info["name"] . '</td>';
            $print[] = '<td>R$ ' . number_format((float) $info["value"], 2, ",", ".") . '</td>';
            $print[] = '<td>R$ ' . number_format($subtotal, 2, ",", ".") . '</td>';
            $print[] = '</tr>';
            $total += $subtotal;
            $qnt += $info["qnt"];
        }
    } else {
        $print[] = '<tr><td colspan="4" height="100" align="center">Seu carrinho está vazio</td></tr>';
    }
    $print[] = '</tbody>';
    $print[] = '<tfoot>';
    $print[] = '<tr>';
    $print[] = '<td>';
    if (strlen($cep["localidade"]) > 0) {
        $print[] = 'Cidade de entrega: <b>' . $cep["localidade"] . '</b>.';
    }
    $print[] = '</td>';
    $print[] = '<td><strong>Frete</strong></td>';
    $print[] = '<td>';
    $print[] = 'R$ ' . number_format((float) $cep["valor"], 2, ",", ".");
    $print[] = '</td>';
    $print[] = '</tr>';
    $print[] = '<tr>';
    $print[] = '<td>&nbsp;</td>';
    $print[] = '<td class="backred"><strong>Total</strong></td>';
    $print[] = '<td class="backred">R$ ' . number_format((float) $total, 2, ",", ".") . '</td>';
    $print[] = '</tr>';
    $print[] = '</tfoot>';
    $print[] = '</table>';
    print join("\n", $print);
    unset($print);

    $print[] = '<br clear="all"/>';
    $print[] = '<table  id="cart" cellpadding="0" cellpadding="0" border="0" width="100%">';
    $print[] = '<thead>';
    $print[] = '<tr>';
    $print[] = '<td width="33%">Já sou cliente</td>';
    $print[] = '<td width="33%">Endereço de entrega e cobrança</td>';
    $print[] = '<td width="33%">Pagamento</td>';
    $print[] = '</tr>';
    $print[] = '</thead>';

    $body_cliente = "&nbsp;";
    $body_endereco = "&nbsp;";
    $body_pagamento = "&nbsp;";

    $foot_cliente = "Ainda não sou cadastrado...";
    $foot_endereco = "&nbsp;";
    $foot_pagamento = "&nbsp;";

    if ($logado) {

        unset($body_cliente);
        $body_cliente[] = 'Bem-vindo(a), <b>Cliente</b>!<br>';
        $body_cliente[] = 'Não é você? Sair.'; //customer.php?loja=

        unset($body_endereco);
        $body_endereco[] = '<div><b>Endereço de cobrança</b></div>';
        $body_endereco[] = '<div>';

        $body_endereco[] = '<select name="endereco_cobranca">';
        $body_endereco[] = '<option value="1">Casa</option>';
        $body_endereco[] = '</select>';

        $body_endereco[] = '</div>';
        $body_endereco[] = '<div><b>Endereço de entrega</b></div>';
        $body_endereco[] = '<div>';

        $body_endereco[] = '<select name="endereco_entrega">';
        $body_endereco[] = '<option value="1">Casa</option>';
        $body_endereco[] = '</select>';


        $body_endereco[] = '</div>';
        $body_endereco[] = '<div align="right">Efetuar pagamento...</div>';

        unset($foot_cliente, $foot_endereco);
        $foot_cliente[] = 'Voltar para o carrinho...';
        $foot_endereco[] = 'Cadastrar novo endereço...';
    }


    $print[] = '<tbody>';
    $print[] = '<tr>';
    $print[] = '<td valign="top">' . (is_array($body_cliente) ? join("\n", $body_cliente) : $body_cliente) . '</td>';
    $print[] = '<td valign="top">' . (is_array($body_endereco) ? join("\n", $body_endereco) : $body_endereco) . '</td>';
    $print[] = '<td valign="top">' . (is_array($body_pagamento) ? join("\n", $body_pagamento) : $body_pagamento) . '</td>';
    $print[] = '</tr>';
    $print[] = '</tbody>';

    $print[] = '<tfoot>';
    $print[] = '<tr>';
    $print[] = '<td align="right">' . (is_array($foot_cliente) ? join("\n", $foot_cliente) : $foot_cliente) . '</td>';
    $print[] = '<td align="right">' . (is_array($foot_endereco) ? join("\n", $foot_endereco) : $foot_endereco) . '</td>';
    $print[] = '<td align="right">' . (is_array($foot_pagamento) ? join("\n", $foot_pagamento) : $foot_pagamento) . '</td>';
    $print[] = '</tr>';
    $print[] = '</tfoot>';
    $print[] = '</table>';
    print join("\n", $print);
    ?>
    <br clear="all"/>
</div>
<script type="text/javascript">

    function GetCEP() {
        var cep = $("input[name=cep]").val();
        var cepdigito = $("input[name=cepdigito]").val();
        var ceptipo = $("select[name=ceptipo]").val();
        var cepcompleto = cep + cepdigito;
        if (cepcompleto.length == 8) {
            return window.open("cart.php?loja=<?php echo $loja; ?>&action=cep&cep=" + cep + "&cepdigito=" + cepdigito + "&ceptipo=" + ceptipo, "_parent");;
        }
        alert("CEP informado incorreto: " + cepcompleto);
        return false;
    }

</script>
<?php
    include_once("footer.php");
?>