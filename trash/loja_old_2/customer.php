<?php
	include("config.php");
	include("header.php");
?>
		<div id="content">
			
			<div class="divcenter box80">
				<h2>Identificação</h2>
				<div class="box45">
					<h4 class="padding5px">Já sou cliente</h4>
					<p>E-Mail:<br/><input size="40"/></p>
					<p>Senha:<br/><input size="40"/></p>
					<p><a href="">Esqueci a senha</a> | <a href="" class="button font18">Continuar</a></p>
					
				</div>
				<div class="box10">&nbsp;</div>
				<div class="box45">
					<h4 class="padding5px">Ainda não sou cliente</h4>
					<p>Informe seu cep:<br/><input size="10"/> - <input size="3"/> <a href="" class="button">Novo cliente</a></p>
					<p><a href="">Não sei meu cep</a></p>
				</div>
			
			</div>
			<br clear="all"/>
			
		</div>
		<div id="footer">
			<div class="box25">
				<h3>Institucional</h3>
				<ul>
					<li><a href="#">Quem Somos</a></li>
					<li><a href="#">Políticas de Privacidade</a></li>
					<li><a href="#">Quer ter uma loja igual a esta?</a></li>
				</ul>
			</div>
			<div class="box25">
				<h3>Dúvidas</h3>
				<ul>
					<li><a href="#">Troca e Devolução</a></li>
					<li><a href="#">Garantia e Assistência Técnica</a></li>
					<li><a href="#">Prazos de Entrega</a></li>
					<li><a href="#">Acompanhe seu pedido</a></li>
					<li><a href="#">Reimpressão de boletos</a></li>
				</ul>
			</div>
			<div class="box50">
				<p>Site seguro | Buscapé | Selos</p>
			</div>
			<br clear="all"/>
			<p><strong>Formas de pagamento:</strong> Cartão de Crédito e Boleto e Transferência Bancária</p>
			<p><a href="http://www.pagseguro.com.br" target="_blank"><img src="imagens/formas_pagamento.jpg"></a></p>
		</div>
	</div>
</body>
</html>