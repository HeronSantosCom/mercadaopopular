<div id="footer">
			<div class="box25">
				<h3>Institucional</h3>
				<ul>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=quem_somos">Quem Somos</a></li>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=politicas_privacidade">Políticas de Privacidade</a></li>
					<li><a href="http://www.mercadaopopular.com.br" target="_blank">Quer ter uma loja igual a esta?</a></li>
				</ul>
			</div>
			<div class="box25">
				<h3>Dúvidas</h3>
				<ul>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=troca_devolucao">Troca e Devolução</a></li>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=garantia">Garantia e Assistência Técnica</a></li>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=entrega">Prazos de Entrega</a></li>
					<li><a href="customer.php?loja=<?php echo $loja ?>">Acompanhe seu pedido</a></li>
				</ul>
			</div>
			<div class="box50">
				<p>Site seguro | Buscapé | Selos</p>
			</div>
			<br clear="all"/>
			<p><strong>Formas de pagamento:</strong> Cartão de Crédito e Boleto e Transferência Bancária</p>
			<p><a href="http://www.pagseguro.com.br" target="_blank"><img src="imagens/formas_pagamento.jpg"></a></p>
		</div>
	</div>
	<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
</body>
</html>