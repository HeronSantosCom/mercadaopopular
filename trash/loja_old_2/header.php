<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-language" content="cs" />
        <meta name="robots" content="all,follow" />

        <meta name="author" content="MercadãoPopular" />
        <meta name="copyright" content="www.mercadaopopular.com.br" />

        <title><?php echo $loja_config['LOJA_NOME'] ?></title>
        <meta name="description" content="<?php echo $loja_config['LOJA_DESCRIPTION'] ?>" />
        <meta name="keywords" content="<?php echo $loja_config['LOJA_KEYWORDS'] ?>" />

        <link rel="stylesheet" type="text/css" href="css/basic.css" />

        <link rel="stylesheet" type="text/css" href="css/<?php echo $loja_config['LOJA_SCHEMA'] ?>.css" />

        <link rel="stylesheet" href="js/nivo-slider.css" type="text/css" media="screen" />

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
        <script>
            $(document).ready(function() {
                $("#alldepartaments").hide();
                $("body,#shop,#menu,#header").mouseenter(function(e){
                    $("#alldepartaments").hide();
                    $(".alldepartaments").css("background-color","#ffffff");
                    e.stopPropagation();
                });
                $(".alldepartaments").mouseenter(function(e){
                    $("#alldepartaments").show();
                    $(".alldepartaments").css("background-color","#f5f5f5");
                    e.stopPropagation();
                });
                $("#alldepartaments").mouseleave(function(e){
                    $("#alldepartaments").hide();
                    $(".alldepartaments").css("background-color","#ffffff");
                    e.stopPropagation();
                });
            });
        </script>
    </head>

    <body>
        <div id="banner-alert">Esta loja faz parte do sistema <a href="http://www.mercadaopopular.com.br">Mercadão Popular</a>!
            <span>&nbsp;</span>Quer uma loja como essa? <a href="http://www.mercadaopopular.com.br">Clique aqui</a></div>
        <div id="shop">
            <div id="header">
                <div id="header-logo" class="box25">
                    <p><a href="index.php?loja=<?php echo $loja ?>" title="<?php echo $loja_config['LOJA_NOME'] ?>"><img src="http://www.mercadaopopular.com.br/userfiles/<?php echo $loja_info['alias'] ?>/<?php echo $loja_config['LOJA_LOGO'] ?>"/></a></p>
                </div>
                <div id="sub-header" class="box75">
                    <p>Boa Noite! Faça seu <a href="customer.php?loja=<?php echo $loja ?>">login</a> ou <a href="customer.php?loja=<?php echo $loja ?>">cadastre-se</a>. <a href="customer.php?loja=<?php echo $loja ?>">Veja seu cadastro</a> |  <a href="customer.php?loja=<?php echo $loja ?>">Veja seus pedidos</a></p>
                    <p>
                        <form method="post" action="search.php?loja=<?php echo $loja ?>">
					Busca: <input name="keyword-search" id="keyword-search" size="60" />
                        </form>
                    </p>
                    <?
                    $list = cartList($loja);
                    ?>
                    <p><a class="button textright mycart" href="cart.php?loja=<?php echo $loja ?>"><img src="imagens/bag.png" align="absmiddle"> Minha sacola: <?= ($list ? count($list) : '0') ?> Item</a></p>
                </div>
                <br clear="all"/>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="#" title="Todos os departamentos" class="alldepartaments"><img src="imagens/home.png" align="absmiddle" /></a></li>
                    <?php
                    $find = $MYSQL->find(array('FROM' => 'catalog_categories', 'WHERE' => 'idlogin = "' . $loja . '" AND front = "1" AND idcatalog_categories_main IS NULL'));
                    if (is_array($find)) {
                        foreach ($find as $catalog) {
                            echo '<li><a href="catalog.php?loja=' . $loja . '&id=' . $catalog['idcatalog_categories'] . '" title="' . $catalog['name'] . '">' . $catalog['name'] . '</a></li>';
                        }
                    }
                    ?>
                </ul>
                <div id="alldepartaments">
                    <h2>Todos os departamentos</h2>
                    <?php
                    $find = $MYSQL->find(array('FROM' => 'catalog_categories', 'WHERE' => 'idlogin = "' . $loja . '" AND idcatalog_categories_main IS NULL'));
                    if (is_array($find)) {
                        foreach ($find as $catalog) {
                            echo '<div class="box25"><a href="catalog.php?loja=' . $loja . '&id=' . $catalog['idcatalog_categories'] . '" title="' . $catalog['name'] . '">' . $catalog['name'] . '</a></div>';
                        }
                    }
                    ?>
                </div>
            </div>
