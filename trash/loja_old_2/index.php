<?php
include("config.php");
include("header.php");
?>
<div id="content">
    <div class="box15">
        <div class="headerbox">
            <?php
            $banner_esquerda = categoryBanner($loja, "ESQUERDA");
            if (is_array($banner_esquerda)) {
                echo '<div id="slider-wrapper">';
                echo '<div id="slider" class="nivoSlider">';
                foreach ($banner_esquerda as $banner) {
                    if (strlen($banner['url']) > 0) {
                        echo '<a href="' . $banner['url'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/></a><br/>';
                    } else {
                        echo '<img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/><br/>';
                    }
                }
                echo '</div>';
                echo '</div>';
            }
            ?>
            &nbsp;
        </div>
    </div>
    <div class="box70">
        <?php
            $banner_centro = categoryBanner($loja, "CENTRO");
            if (is_array($banner_centro)) {
                echo '<div id="slider-wrapper">';
                echo '<div id="slider" class="nivoSlider">';
                foreach ($banner_centro as $banner) {
                    if (strlen($banner['url']) > 0) {
                        echo '<a href="' . $banner['url'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/></a>';
                    } else {
                        echo '<img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/>';
                    }
                }
                echo '</div>';
                echo '</div>';
            }
        ?>
            <br clear="all"/>
        <?php
            $products = productList($loja, $page);
            if (isset($products["products"])) {
                foreach ($products["products"] as $key => $product) {
                    $print[] = "<div class='box33'>";
                    $print[] = "<div class='product'>";
                    $print[] = "<a href='product.php?loja={$loja}&id={$product["idcatalog_products"]}'>";
                    if (strlen($product['thumbnail']) > 0) {
                        $print[] = "<div><img src='/userfiles/{$loja_info['alias']}/small/{$product['thumbnail']}' /></div>";
                    } else {
                        $print[] = "<div class='image'></div>";
                    }
                    $print[] = $product["name"];
                    $print[] = "<br/>";
                    if ($product['last_value'] > 0 and $product['last_value'] != "0,00") {
                        $print[] = "<span class='price_for'>De: R$ " . number_format($product['last_value'], 2, ",", ".") . "</span><br/>";
                    }
                    $print[] = "<span class='price_to'>Por: R$ " . number_format($product['value'], 2, ",", ".") . "</span>";
                    $print[] = "</a>";
                    $print[] = "</div>";
                    $print[] = "</div>";
                }
                print join("\n", $print);
            } else {
                print "<p>Nenhum produto disponível!</p>";
            }
        ?>
            <br clear="all"/>
        </div>
        <div class="box15">
        <?php
            $banner_direita = categoryBanner($loja, "DIREITA");
            if (is_array($banner_direita)) {
                echo '<div id="slider-wrapper">';
                echo '<div id="slider" class="nivoSlider">';
                foreach ($find as $banner) {
                    if (strlen($banner['url']) > 0) {
                        echo '<a href="' . $banner['url'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/></a><br/>';
                    } else {
                        echo '<img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/><br/>';
                    }
                }
                echo '</div>';
                echo '</div>';
            }
        ?>
        </div>
        <br clear="all"/>
    </div>
<?php
            include_once("footer.php");
?>