<?php
	include("config.php");
	include("header.php");
?>
		<div id="content">
			<h2>Tópicos de Ajuda</h2>
			<br clear="all"/>
			<div class="box20">
				<h3>Institucional</h3>
				<ul>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=quem_somos">Quem Somos</a></li>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=politicas_privacidade">Políticas de Privacidade</a></li>
					<li><a href="http://www.mercadaopopular.com.br" target="_blank">Quer ter uma loja igual a esta?</a></li>

				</ul>
				<h3>Dúvidas</h3>
				<ul>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=troca_devolucao">Troca e Devolução</a></li>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=garantia">Garantia e Assistência Técnica</a></li>
					<li><a href="info.php?loja=<?php echo $loja ?>&page=entrega">Prazos de Entrega</a></li>
				</ul>
			</div>
			
			<div class="box80">
				<?php 
				
				if (isset($page) and strlen($page)>0) {
					$find = $MYSQL->find(array('FROM'=>'textos','WHERE'=>'idlogin = "'.$loja.'" AND alias = "'.$page.'"'));
					if (is_array($find)) {
						echo "<h3>".$find[0]['title']."</h3>";
						echo $find[0]['texto'];	
					} else {
						echo "<h3>Erro</h3>";
						echo "<p>Tópico não encontrado</p>";
					}
				} else {
					echo "<h3>Selecione um tópico ao lado</h3>";
					echo "<p></p>";
				}
				
				?>			
				<br clear="all"/>
			</div>
						<br clear="all"/>
		</div>
		<?php
			include_once("footer.php");
		?>