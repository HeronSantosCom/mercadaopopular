<?php
include("config.php");
include("header.php");
?>
<div id="content">
    <div class="box15">
        <div class="headerbox">
            <?php
            $banner_esquerda = categoryBanner($loja, "ESQUERDA");
            if (is_array($banner_esquerda)) {
                echo '<div id="slider-wrapper">';
                echo '<div id="slider" class="nivoSlider">';
                foreach ($banner_esquerda as $banner) {
                    if (strlen($banner['url']) > 0) {
                        echo '<a href="' . $banner['url'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/></a><br/>';
                    } else {
                        echo '<img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/><br/>';
                    }
                }
                echo '</div>';
                echo '</div>';
            }
            ?>
            &nbsp;
        </div>
    </div>
    <div class="box70">
        <?php
            $banner_centro = categoryBanner($loja, "CENTRO");
            if (is_array($banner_centro)) {
                echo '<div id="slider-wrapper">';
                echo '<div id="slider" class="nivoSlider">';
                foreach ($banner_centro as $banner) {
                    if (strlen($banner['url']) > 0) {
                        echo '<a href="' . $banner['url'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/></a>';
                    } else {
                        echo '<img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/>';
                    }
                }
                echo '</div>';
                echo '</div>';
            }
        ?>
            <br clear="all"/>
        <?php
            $product = productView($loja, $id);
            if (is_array($product)) {
                $print[] = '<div class="product">';
                $print[] = '<p class="product_title">' . $product['name'] . '</p>';
                $print[] = '<div id="product_photo_box">';
                $print[] = '  <img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/big/' . $product['thumbnail'] . '" width="250" />';
                $print[] = '</div>';
                $array_pictures = null;
                $array_pictures['FROM'] = 'catalog_products_photos';
                $array_pictures['WHERE'] = 'idcatalog_products = "' . $product['idcatalog_products'] . '"';
                $find_pictures = $MYSQL->find($array_pictures);
                if (is_array($find_pictures)) {
                    $print[] = '<div id="product_photo_list">';
                    foreach ($find_pictures as $pic) {
                        $print[] = '  <a href="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/big/' . $pic['photo'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/small/' . $pic['photo'] . '" /></a>';
                    }
                    $print[] = '</div>';
                }
                $print[] = '<div id="product_info_box">';
                $print[] = '	<p class="product_info"><strong>Código</strong><br>' . str_pad($product['idcatalog_products'], 10, '0', STR_PAD_LEFT) . '</p>';
                $print[] = '	<p class="product_info"><strong>Fabricante</strong><br>' . $product['manufacturer_name'] . '</p>';
                $print[] = '<p class="product_info product_value">R$ ' . number_format($product['value'], 2, ",", ".") . '</p>';
                $print[] = '	<p class="product_info"><strong>Garantia</strong><br>' . $product['guarantee'] . '</p>';
                $print[] = '</div>';
                $print[] = '<form method="post" action="cart.php?loja=' . $loja . '">';
                $print[] = '<input type="hidden" name="product" value="' . $product['idcatalog_products'] . '" />';
                $print[] = '<input type="hidden" name="action" value="add" />';
                $print[] = '<input type="submit" value="Adicionar" class="cart_button"/>';
                $print[] = '</form>';
                $print[] = '<div class="toggle">';
                $print[] = '  <p class="product_header">Caracteristicas</p>';
                $print[] = '  <p class="product_description">' . $product['description'] . '</p>';
                $print[] = '  <p>&nbsp;</p>';
                $print[] = '</div>';
                print join("\n", $print);
            } else {
                print "<p>Produto não encontrado!</p>";
            }
        ?>
            <br clear="all"/>
        </div>
        <div class="box15">
        <?php
            $banner_direita = categoryBanner($loja, "DIREITA");
            if (is_array($banner_direita)) {
                echo '<div id="slider-wrapper">';
                echo '<div id="slider" class="nivoSlider">';
                foreach ($find as $banner) {
                    if (strlen($banner['url']) > 0) {
                        echo '<a href="' . $banner['url'] . '"><img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/></a><br/>';
                    } else {
                        echo '<img src="http://www.mercadaopopular.com.br/userfiles/' . $loja_info['alias'] . '/banners/' . $banner['file'] . '" alt=""/><br/>';
                    }
                }
                echo '</div>';
                echo '</div>';
            }
        ?>
        </div>
        <br clear="all"/>
    </div>
<?php
            include_once("footer.php");
?>