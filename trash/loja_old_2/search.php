<?php
	include("config.php");
	include("header.php");
?>
		<div id="content">
			<div class="box15">
				{%SUBMENU_LEFT%}
				{%BANNER_LEFT%}
			</div>
			
			<div class="box70">
				<div class="main-banner">
					&nbsp;
				</div>
				<br clear="all"/>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Notebook SIM 2047 c/ Intel® Pentium Dual Core T4500 2.2GHz 2GB 320GB DVD-RW Office 2010 Starter (Word e Excel) Webcam 1.3MP 14" Windows 7 Starter - SIM<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p>
						</a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Smartphone Nokia C3 Grafite - GSM Desbloqueado Claro c/ Sistema Operacional Symbian S40 6.0, Wi-Fi, Teclado Qwerty, Câmera 2.0MP, Filmadora, MP3 Player, Rádio FM, Bluetooth, Fone, Cabo de Dados e Cartão de 2GB<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Condicionador de Ar Split 12.000 BTUs Frio - Branco - 220V - Comfee<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<br clear="all"/>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Notebook SIM 2047 c/ Intel® Pentium Dual Core T4500 2.2GHz 2GB 320GB DVD-RW Office 2010 Starter (Word e Excel) Webcam 1.3MP 14" Windows 7 Starter - SIM<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p>
						</a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Smartphone Nokia C3 Grafite - GSM Desbloqueado Claro c/ Sistema Operacional Symbian S40 6.0, Wi-Fi, Teclado Qwerty, Câmera 2.0MP, Filmadora, MP3 Player, Rádio FM, Bluetooth, Fone, Cabo de Dados e Cartão de 2GB<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Condicionador de Ar Split 12.000 BTUs Frio - Branco - 220V - Comfee<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<br clear="all"/>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Notebook SIM 2047 c/ Intel® Pentium Dual Core T4500 2.2GHz 2GB 320GB DVD-RW Office 2010 Starter (Word e Excel) Webcam 1.3MP 14" Windows 7 Starter - SIM<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p>
						</a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Smartphone Nokia C3 Grafite - GSM Desbloqueado Claro c/ Sistema Operacional Symbian S40 6.0, Wi-Fi, Teclado Qwerty, Câmera 2.0MP, Filmadora, MP3 Player, Rádio FM, Bluetooth, Fone, Cabo de Dados e Cartão de 2GB<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Condicionador de Ar Split 12.000 BTUs Frio - Branco - 220V - Comfee<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<br clear="all"/>
			</div>
			
			<div class="box15">
				{%BANNER_RIGHT%}
			</div>
			<br clear="all"/>
		</div>
		<div id="footer">
			<div class="box25">
				<h3>Institucional</h3>
				<ul>
					<li><a href="#">Quem Somos</a></li>
					<li><a href="#">Políticas de Privacidade</a></li>
					<li><a href="#">Quer ter uma loja igual a esta?</a></li>
				</ul>
			</div>
			<div class="box25">
				<h3>Dúvidas</h3>
				<ul>
					<li><a href="#">Troca e Devolução</a></li>
					<li><a href="#">Garantia e Assistência Técnica</a></li>
					<li><a href="#">Prazos de Entrega</a></li>
					<li><a href="#">Acompanhe seu pedido</a></li>
					<li><a href="#">Reimpressão de boletos</a></li>
				</ul>
			</div>
			<div class="box50">
				<p>Site seguro | Buscapé | Selos</p>
			</div>
			<br clear="all"/>
			<p><strong>Formas de pagamento:</strong> Cartão de Crédito e Boleto e Transferência Bancária</p>
			<p><a href="http://www.pagseguro.com.br" target="_blank"><img src="imagens/formas_pagamento.jpg"></a></p>
		</div>
	</div>
</body>
</html>