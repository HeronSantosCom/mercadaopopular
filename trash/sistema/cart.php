<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="cs" />
    <meta name="robots" content="all,follow" />

    <meta name="author" content="All: ... [Nazev webu - www.url.cz]; e-mail: info@url.cz" />
    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>Nome da Loja</title>
    <meta name="description" content="..." />
    <meta name="keywords" content="Loja, Loja virtual, Loja Grátis, Loja Virtual Grátis, E-commerce, E-commerce free" />
    
    <link rel="stylesheet" type="text/css" href="css/basic.css" />
    <link rel="stylesheet" type="text/css" href="css/personalizado.css" />
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script>
		$(document).ready(function() { 
			$("#alldepartaments").hide();
			$("body,#shop,#menu,#header").mouseenter(function(e){ 
				$("#alldepartaments").hide();
				$(".alldepartaments").css("background-color","#ffffff");
				e.stopPropagation();
			});
			$(".alldepartaments").mouseenter(function(e){ 
				$("#alldepartaments").show();
				$(".alldepartaments").css("background-color","#f5f5f5");
				e.stopPropagation();
			});
			$("#alldepartaments").mouseleave(function(e){ 
				$("#alldepartaments").hide();
				$(".alldepartaments").css("background-color","#ffffff");
				e.stopPropagation();
			});
		});
	</script>
</head>

<body>
	<div id="banner-alert">Esta loja faz parte do sistema <a href="http://www.mercadaopopular.com.br">Mercadão Popular</a>!
	<span>&nbsp;</span>Quer uma loja como essa? <a href="http://www.mercadaopopular.com.br">Clique aqui</a></div>
	<div id="shop">
		<div id="header">
			<div id="header-logo" class="box25">
				<p><a href="#"><img src="imagens/logo.jpg"/></a></p>
			</div>
			<div id="sub-header" class="box75">
				<p>Boa Noite! Faça seu <a href="#">login</a> ou <a href="#">cadastre-se</a>. <a href="#">Veja seu cadastro</a> |  <a href="#">Veja seus pedidos</a></p>
				<p>
					<form method="post">
					Busca: <input name="keyword-search" id="keyword-search" size="60" />
					</form>
				</p>
			</div>
			<br clear="all"/>
		</div>
		<div id="menu">
			<ul>
				<li><a href="#" title="Todos os departamentos" class="alldepartaments"><img src="imagens/home.png" align="absmiddle" /></a></li>
				<li><a href="#" title="Áudio e Vídeo" class="selected">Áudio e Vídeo</a></li>
				<li><a href="#" title="Câmeras e Filmadoras">Câmeras e Filmadoras</a></li>
				<li><a href="#" title="Hobby, Lazer e Carro">Hobby, Lazer e Carro</a></li>
				<li><a href="#" title="Utilizades Domésticas">Utilizades Domésticas</a></li>
				<li><a href="#" title="Segurança e Espionagem">Segurança e Espionagem</a></li>
				<li><a href="#" title="Informática">Informática</a></li>
			</ul>
			<div id="alldepartaments">
				<h2>Todos os departamentos</h2>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
			</div>
		</div>
		<div id="content">
			
				<h2>Minha sacola de compras</h2>
				<p><a href="#">Escolher mais produtos</a></p>
				<br clear="all"/>
				<table  id="cart" cellpadding="0" cellpadding="0" border="0" width="100%">
					<thead>
					<tr>
						<td width="60%">PRODUTO</td>
						<td width="10%">QNT</td>
						<td width="15%">VALOR UNITÁRIO</td>
						<td width="15%">VALOR TOTAL</td>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>Produto tal</td>
						<td>1</td>
						<td>R$ 20,00</td>
						<td>R$ 20,00</td>
					</tr>
					<tr>
						<td>Produto tal</td>
						<td>1</td>
						<td>R$ 20,00</td>
						<td>R$ 20,00</td>
					</tr>
					<tr>
						<td>Produto tal</td>
						<td>1</td>
						<td>R$ 20,00</td>
						<td>R$ 20,00</td>
					</tr>
					
					<tr>
						<td colspan="4" height="100" align="center">Seu carrinho está vazio</td>
					</tr>
					<tfoot>
					<tr>
						<td>Calcule o frete e saiba a previsão de entrega <input type="text" size="10" maxlength="5"/> - <input type="text" size="3" maxlength="3" /></td>
						<td>&nbsp;</td>
						<td><strong>Frete</strong></td>
						<td>R$ 5,00</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="backred"><strong>Total</strong></td>
						<td class="backred">R$ 60,00</td>
					</tr>
					</tfoot>
					</tbody>
				</table>
				<br clear="all"/>
				<div class="box50">
					<p><a href="#" class="button">Escolher mais produtos</a></p>
				</div>
				<div class="box50 textright">
					<p><a href="#" class="button font18">Fechar pedido</a></p>
				</div>
				<br clear="all"/>
		</div>
		<div id="footer">
			<div class="box25">
				<h3>Institucional</h3>
				<ul>
					<li><a href="#">Quem Somos</a></li>
					<li><a href="#">Políticas de Privacidade</a></li>
					<li><a href="#">Quer ter uma loja igual a esta?</a></li>
				</ul>
			</div>
			<div class="box25">
				<h3>Dúvidas</h3>
				<ul>
					<li><a href="#">Troca e Devolução</a></li>
					<li><a href="#">Garantia e Assistência Técnica</a></li>
					<li><a href="#">Prazos de Entrega</a></li>
					<li><a href="#">Acompanhe seu pedido</a></li>
					<li><a href="#">Reimpressão de boletos</a></li>
				</ul>
			</div>
			<div class="box50">
				<p>Site seguro | Buscapé | Selos</p>
			</div>
			<br clear="all"/>
			<p><strong>Formas de pagamento:</strong> Cartão de Crédito e Boleto e Transferência Bancária</p>
			<p><a href="http://www.pagseguro.com.br" target="_blank"><img src="imagens/formas_pagamento.jpg"></a></p>
		</div>
	</div>
</body>
</html>