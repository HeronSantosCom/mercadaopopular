<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="cs" />
    <meta name="robots" content="all,follow" />

    <meta name="author" content="All: ... [Nazev webu - www.url.cz]; e-mail: info@url.cz" />
    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>Nome da Loja</title>
    <meta name="description" content="..." />
    <meta name="keywords" content="Loja, Loja virtual, Loja Grátis, Loja Virtual Grátis, E-commerce, E-commerce free" />
    
    <link rel="stylesheet" type="text/css" href="css/basic.css" />
    <link rel="stylesheet" type="text/css" href="css/personalizado.css" />
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script>
		$(document).ready(function() { 
			$("#alldepartaments").hide();
			$("body,#shop,#menu,#header").mouseenter(function(e){ 
				$("#alldepartaments").hide();
				$(".alldepartaments").css("background-color","#ffffff");
				e.stopPropagation();
			});
			$(".alldepartaments").mouseenter(function(e){ 
				$("#alldepartaments").show();
				$(".alldepartaments").css("background-color","#f5f5f5");
				e.stopPropagation();
			});
			$("#alldepartaments").mouseleave(function(e){ 
				$("#alldepartaments").hide();
				$(".alldepartaments").css("background-color","#ffffff");
				e.stopPropagation();
			});
		});
	</script>
</head>

<body>
	<div id="banner-alert">Esta loja faz parte do sistema <a href="http://www.mercadaopopular.com.br">Mercadão Popular</a>!
	<span>&nbsp;</span>Quer uma loja como essa? <a href="http://www.mercadaopopular.com.br">Clique aqui</a></div>
	<div id="shop">
		<div id="header">
			<div id="header-logo" class="box25">
				<p><a href="#"><img src="imagens/logo.jpg"/></a></p>
			</div>
			<div id="sub-header" class="box75">
				<p>Boa Noite! Faça seu <a href="#">login</a> ou <a href="#">cadastre-se</a>. <a href="#">Veja seu cadastro</a> |  <a href="#">Veja seus pedidos</a></p>
				<p>
					<form method="post">
					Busca: <input name="keyword-search" id="keyword-search" size="60" />
					</form>
				</p>
			</div>
			<br clear="all"/>
		</div>
		<div id="menu">
			<ul>
				<li><a href="#" title="Todos os departamentos" class="alldepartaments"><img src="imagens/home.png" align="absmiddle" /></a></li>
				<li><a href="#" title="Áudio e Vídeo" class="selected">Áudio e Vídeo</a></li>
				<li><a href="#" title="Câmeras e Filmadoras">Câmeras e Filmadoras</a></li>
				<li><a href="#" title="Hobby, Lazer e Carro">Hobby, Lazer e Carro</a></li>
				<li><a href="#" title="Utilizades Domésticas">Utilizades Domésticas</a></li>
				<li><a href="#" title="Segurança e Espionagem">Segurança e Espionagem</a></li>
				<li><a href="#" title="Informática">Informática</a></li>
			</ul>
			<div id="alldepartaments">
				<h2>Todos os departamentos</h2>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
				<div class="box25"><a href="#" title="Áudio e Vídeo">Áudio e Vídeo</a></div>
			</div>
		</div>
		<div id="content">
			<div class="box15">
				{%SUBMENU_LEFT%}
				{%BANNER_LEFT%}
			</div>
			
			<div class="box70">
				<div class="main-banner">
					&nbsp;
				</div>
				<br clear="all"/>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Notebook SIM 2047 c/ Intel® Pentium Dual Core T4500 2.2GHz 2GB 320GB DVD-RW Office 2010 Starter (Word e Excel) Webcam 1.3MP 14" Windows 7 Starter - SIM<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p>
						</a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Smartphone Nokia C3 Grafite - GSM Desbloqueado Claro c/ Sistema Operacional Symbian S40 6.0, Wi-Fi, Teclado Qwerty, Câmera 2.0MP, Filmadora, MP3 Player, Rádio FM, Bluetooth, Fone, Cabo de Dados e Cartão de 2GB<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Condicionador de Ar Split 12.000 BTUs Frio - Branco - 220V - Comfee<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<br clear="all"/>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Notebook SIM 2047 c/ Intel® Pentium Dual Core T4500 2.2GHz 2GB 320GB DVD-RW Office 2010 Starter (Word e Excel) Webcam 1.3MP 14" Windows 7 Starter - SIM<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p>
						</a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Smartphone Nokia C3 Grafite - GSM Desbloqueado Claro c/ Sistema Operacional Symbian S40 6.0, Wi-Fi, Teclado Qwerty, Câmera 2.0MP, Filmadora, MP3 Player, Rádio FM, Bluetooth, Fone, Cabo de Dados e Cartão de 2GB<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Condicionador de Ar Split 12.000 BTUs Frio - Branco - 220V - Comfee<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<br clear="all"/>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Notebook SIM 2047 c/ Intel® Pentium Dual Core T4500 2.2GHz 2GB 320GB DVD-RW Office 2010 Starter (Word e Excel) Webcam 1.3MP 14" Windows 7 Starter - SIM<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p>
						</a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Smartphone Nokia C3 Grafite - GSM Desbloqueado Claro c/ Sistema Operacional Symbian S40 6.0, Wi-Fi, Teclado Qwerty, Câmera 2.0MP, Filmadora, MP3 Player, Rádio FM, Bluetooth, Fone, Cabo de Dados e Cartão de 2GB<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<div class="box33">
					<div class="product">
						<a href="#"><div class="image"></div>Condicionador de Ar Split 12.000 BTUs Frio - Branco - 220V - Comfee<br/>
						<p><span class="price_for">De: R$ 1.599,00</span><br/>
						<span class="price_to">Por: R$ 999,00</span></p></a>
					</div>
				</div>
				<br clear="all"/>
			</div>
			
			<div class="box15">
				{%BANNER_RIGHT%}
			</div>
			<br clear="all"/>
		</div>
		<div id="footer">
			<div class="box25">
				<h3>Institucional</h3>
				<ul>
					<li><a href="#">Quem Somos</a></li>
					<li><a href="#">Políticas de Privacidade</a></li>
					<li><a href="#">Quer ter uma loja igual a esta?</a></li>
				</ul>
			</div>
			<div class="box25">
				<h3>Dúvidas</h3>
				<ul>
					<li><a href="#">Troca e Devolução</a></li>
					<li><a href="#">Garantia e Assistência Técnica</a></li>
					<li><a href="#">Prazos de Entrega</a></li>
					<li><a href="#">Acompanhe seu pedido</a></li>
					<li><a href="#">Reimpressão de boletos</a></li>
				</ul>
			</div>
			<div class="box50">
				<p>Site seguro | Buscapé | Selos</p>
			</div>
			<br clear="all"/>
			<p><strong>Formas de pagamento:</strong> Cartão de Crédito e Boleto e Transferência Bancária</p>
			<p><a href="http://www.pagseguro.com.br" target="_blank"><img src="imagens/formas_pagamento.jpg"></a></p>
		</div>
	</div>
</body>
</html>